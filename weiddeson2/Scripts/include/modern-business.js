
var goods = 0;
var goodsPrice = 0;
var t;
var cp1 = document.getElementById('crushpay_timer');
var cp2 = document.getElementById('cardpay_timer');
var cp3 = document.getElementById('finish_timer');
var buycash = document.getElementById('buyCash');
var noncash = document.getElementById('nonCash');
var hascash = document.getElementById('hasCash');
var givecash = document.getElementById('giveCash');
var proid = document.getElementById('proid');
var cp1time = 60;
var cp2time = 30;
var cp3time = 30;

function showformbuy(num, price) {
    if (num <= 9) {
        num = "0" + num;
    }

    goods = num;
    goodsPrice = price;
    $('#buypic').attr("src", $("#productinfo" + num).data("img"));
    $('#buyname').text($("#productinfo" + num).data("pdname"));
    $('#discription').text($("#productinfo" + num).data("content"));
    $('#buyprice').val($("#productinfo" + num).data("unit"));
  //$('#formbuy').show();
    document.getElementById("formbuy").style.display = "block";
  /*document.getElementById("formpro1").style.display = "none";
  document.getElementById("formpro2").style.display = "none";
  document.getElementById("formpro3").style.display = "none";
  document.getElementById("formpro4").style.display = "none";
  document.getElementById("formpro5").style.display = "none";
  document.getElementById("formpro6").style.display = "none";
  document.getElementById("formpro7").style.display = "none";
  document.getElementById("formpro8").style.display = "none";
  document.getElementById("formpro9").style.display = "none";
  document.getElementById("formpro10").style.display = "none";*/
  //$('#formpro' + num).show();
  //$('#light').show();
    document.getElementById("formpro").style.display = "block";
    document.getElementById("light").style.display = "block";
    //$('#roadid').val(num);
    getRealPrice(num);
  // $('#buyCash').val(price);
}

function showformpay(type, type2) {
  document.getElementById("formpay").style.display = "block";
  document.getElementById("formpay1").style.display = "none";
  document.getElementById("formpay2").style.display = "none";
  document.getElementById("formpay3").style.display = "none";
  document.getElementById("formpay4").style.display = "none";
  document.getElementById("formpay5").style.display = "none";
  document.getElementById("formpay" + type).style.display = "block";
  document.getElementById("card1").style.display = "none";
  document.getElementById("card2").style.display = "none";
  document.getElementById("card3").style.display = "none";
  document.getElementById("card4").style.display = "none";
  document.getElementById("card5").style.display = "none";
  document.getElementById("card" + type).style.display = "block";
  document.getElementById("formpaycrush").style.display = "none";
  document.getElementById("formpaycard").style.display = "none";
  document.getElementById("formpay" + type2).style.display = "block";
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      s = setInterval(test, 700);
    }
  };
  xmlhttp.open("post", "/Trans/cashstampStart", true);
  xmlhttp.send();
}

function test() {
  checkData($('#roadid').val(), $('#buyCash').val());
  getCashin();
  //xmlhttpRequest();
}

function checkData(num, price) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      if (!(data.price == $('#buyCash').val())) {
          $('#buyCash').val(data.price);
      } else if (!(data.product == $('#roadid').val())) {
        $('#roadid').val(data.product);
      }
    }
  };
  xmlhttp.open("post", "/Trans/checkData", true);
  xmlhttp.send();
}

function getRealPrice(proid) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      $('#buyCash').val(this.responseText);
    }
  };
  xmlhttp.open("get", "/Trans/getRealProce?roadid=" + proid, true);
  xmlhttp.send();
}

function Cashout() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          if (this.responseText) {
              c = setTimeout(resets,3000);
          }
    }
  };
  xmlhttp.open("POST", "/Trans/Cashout", true);
  xmlhttp.send();
}

function resets()
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            clearTimeout(c);
            location.href = "/";
        }
    };
    xmlhttp.open("POST", "/Trans/ResetCashtemp", true);
    xmlhttp.send();
}

function getCashin() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          var data = JSON.parse(this.responseText);
          $('#buyCash').val(data.salsePrice);
          $('#nonCash').val(data.nonCash);
          $('#hasCash').val(data.cashIn);
          $('#giveCash').val(data.cashOut);
          if (data.buySuccess) {
              $('#buybutton').attr('disabled',false);
          } else {
              $('#buybutton').attr('disabled', true);
          }
    }
  };
  xmlhttp.open("POST", "/Trans/getCashin", true);
  xmlhttp.send();
}

function countdown1() {
  $('#crushpay_timer').html(cp1time - 1);
    if (cp1time == 0) {
        document.getElementById('formbuy').style.display = "none";
        document.getElementById('formpay').style.display = "none";
        document.getElementById('light').style.display = "none";
        clearTimeout(t);
        clearInterval(s);
        cp1time = 60;
    }
    else {
        t = setTimeout('countdown1()', 1000);
        cp1time--;
    }
}

function countdown2() {
  $('#cardpay_timer').html(cp2time - 1);
    if (cp2time == 0) {
        document.getElementById('formbuy').style.display = "none";
        document.getElementById('formpay').style.display = "none";
        document.getElementById('light').style.display = "none";
        clearTimeout(t);
        cp2time = 30;
    }
    else {
        t = setTimeout('countdown2()', 1000);
        cp2time--;
    }
}

function saleFunction(type, type2) {
    location.href = '/';   
}

function countdown3() {
    $('#finish_timer').html(cp3time - 1);
    if (cp3time == 0) {
        document.getElementById('formbuy').style.display = "none";
        document.getElementById('formpay').style.display = "none";
        document.getElementById('finishpage').style.display = "none";
        document.getElementById('light').style.display = "none";
        clearTimeout(t);
        cp3time = 30;
    }
    else {
        t = setTimeout('countdown3()', 1000);
        cp3time--;
    }
}

function stopcount() {
    clearTimeout(t);
    clearInterval(s);
    cp1time = 60;
    cp2time = 30;
    cp3time = 30;
    $('#buyCash').val(0);
    $('#nobCash').val(0);
    $('#giveCash').val(0);
    $('#hasCash').val(0);
}

function salse() {
    $('#formpay').submit();
    c = setTimeout(resets2,15000);
}

function resets2() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            clearTimeout(c);
            location.href = "/";
        }
    };
    xmlhttp.open("POST", "/Trans/Salsecomplete", true);
    xmlhttp.send();
}
