$(document).ready(function() {
    // $( "#draggable" ).draggable({ containment: "#containment-wrapper", scroll: false });
    $("#draggable").hide();
})

function keyboard(id) {
    var keyboardTxt = $(".keyboard__content-item div");
    var keyboardLength = keyboardTxt.length;

    move('draggable');

    keyboardTxt.unbind("click");

    if (id != false) {           

        if ($(window)[0].innerWidth < 900) {
            $("#draggable").hide();
        } else {
            $("#draggable").show();
        }
        
        var keyboard_text = $("#"+id).val(); 

        $(".focus").removeClass("focus");
        $("#"+id).addClass("focus");

        $('#input-clear').click(function() {
            keyboard_text = "";
            $("#"+id).val(keyboard_text);
        })

        $('#input-close').click(function() {
            $(".focus").removeClass("focus");
            $("#draggable").hide();
        })

        $(".keyboard__title-keyboard").html($("#"+id).attr("placeholder"));

        keyboardTxt.click(function () {
            var click = $(this)[0].innerText;

            if (click == "shift") {
                for (i=0; i < keyboardLength; i++) {
                    $(".keyboard__content-item div p:eq("+i+")").html($(".keyboard__content-item div p:eq("+i+")").html().toUpperCase());
                }
            } else if (click == "SHIFT") {
                for (i=0; i < keyboardLength; i++) {
                    $(".keyboard__content-item div p:eq("+i+")").html($(".keyboard__content-item div p:eq("+i+")").html().toLowerCase());
                }
            } else if (click == '空白') {
                keyboard_text = keyboard_text + ' ';
                $("#" + id).val(keyboard_text);
            } else {
                keyboard_text = keyboard_text + click;
                $("#" + id).val(keyboard_text);
            }
        })
    } else {
        $(".focus").removeClass("focus");
        $("#draggable").hide();
    }
}

function clearValue(val) {
    val = '';
    $(".focus").removeClass("focus");
    $("#draggable").hide();
}

function move(obj){
    var OffsetX = 0;
    var OffsetY = 0;
    var moveKg = false;
    var csZ = 0;

    function isObj(id) {
        return document.getElementById(id);
    }

    $("#"+obj+" .keyboard__title").bind("mousedown", function () {
        OffsetX = event.pageX - isObj(obj).offsetLeft;
        OffsetY = event.pageY - isObj(obj).offsetTop;
        csZ = $("#"+obj).css("z-index");
        $("#"+obj).css("z-index","9999");
        moveKg = true;
        boardmove();
    })

    function boardmove() {
        $(document).bind("mousemove", function () {
            var e = e || window.event;
            var mouswX = e.pageX;
            var mouswY = e.pageY;
            var moveX = mouswX - OffsetX;
            var moveY = mouswY - OffsetY;
            var maxX = $(window).width() - isObj(obj).offsetWidth;
            var maxY = $(window).height() - isObj(obj).offsetHeight;
            moveX=Math.min(maxX,Math.max(0,moveX));
            moveY=Math.min(maxY,Math.max(0,moveY));
            $("#"+obj).css({"left":moveX,"top":moveY});
        })
        $(document).bind("mouseup", function () {
            moveKg = false;
            $("#"+obj).css("z-index",csZ);
            $(document).unbind("mousemove mouseup");
        })
    }
}

