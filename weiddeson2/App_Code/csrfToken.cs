﻿//using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace weiddeson2.App_Code
{
    public class csrfToken
    {
        public string ReturnCsrf()
        {
            string token = "";
            string[] index = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };

            var rand = new Random();
            for(int k=1;k<=36;k++)
            {
                token += index[rand.Next(0,61)];
            }

            return token;
        }
    }
}