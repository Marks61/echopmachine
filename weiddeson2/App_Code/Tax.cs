﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Razor.Parser.SyntaxTree;
using System.Web.UI.WebControls;

namespace weiddeson2.App_Code
{
    public class Tax
    {
        public bool checkFormat(string word)
        {
            bool check = false;
            if (word.Length == 10)
            {
                string id = word;
                
                string[] eng = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
                "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W",
                "Z", "I", "O" };

                int intEng = 0;

                for (int i = 0; i < eng.Length; i++)
                {
                    if (eng[i] == id.Substring(0, 1).ToUpper())
                    {
                        intEng = i + 10;
                        break;
                    }

                }

                //假設n=17
                int n1 = intEng / 10;  //n1=1
                int n2 = intEng % 10;  //n2=7

                int[] a = new int[9];
                for (int i = 0; i < a.Length; i++)
                {
                    a[i] = Convert.ToInt16(id.Substring(i + 1, 1));

                }

                int sum = 0;
                for (int i = 0; i < 8; i++)
                {

                    sum += a[i] * (8 - i);
                }

                int n = 0;
                n = n1 + n2 * 9 + sum + a[8];

                //n1 +n2*9+a[0]*8+a[1]*7*a[2]*6+a[3]*5

                if (n % 10 == 0)
                {
                    check = true;
                }
                else
                {
                    check = false;
                }

                return check;
            }
            else if(word.Length == 8) {
                //8位數字分別乘上1、2、1、2、1、2、4、1
                int num = 0;
                int[] doubles = {1,2,1,2,1,2,4,1 };
                for(int sorts = 0; sorts <= 7; sorts++)
                {
                    int benchMark = Convert.ToInt32(word.Substring(sorts,1));
                    int product = benchMark * doubles[sorts];
                    
                    if(sorts==6 && benchMark==7)
                    {
                        continue;
                    }else if (product>=10)
                    {
                        int cut1 = Convert.ToInt32(Convert.ToString(product).Substring(0, 1));
                        int cut2 = Convert.ToInt32(Convert.ToString(product).Substring(1, 1));
                        num += cut1;
                        num += cut2;
                    }
                    else
                    {
                        num += product;
                    }
                }

                if(Convert.ToInt32(word.Substring(6, 1))==7){
                    if((num+1) % 10 == 0)
                    {
                        check = true;
                    }
                    else if((num) % 10 == 0)
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                    }
                }
                else
                {
                    if ((num) % 10 == 0)
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                    }
                }
                return check;
            }
            else
            {
                return check;
            }
        }
    }
}