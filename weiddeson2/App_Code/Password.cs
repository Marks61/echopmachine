﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Net.Security;
using weiddeson2.App_Code;

namespace weiddeson2.App_Code
{
    public class Password
    {
        privateRsa privateRsa = new privateRsa();
        private static readonly string sqlconn2 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private static readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);
        public string highSecurityPassword()
        {
            string token = "";
            string[] index = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };

            var rand = new Random();
            for (int k = 1; k <= 12; k++)
            {
                token += index[rand.Next(0, 61)];
            }

            return token;
        }

        public string Change(string Word)
        {
            //string Word = word;

            SHA512 sha512 =new SHA512CryptoServiceProvider();  //建立一個SHA512
            byte[] source = Encoding.Default.GetBytes(Word+"weideson");   //將字串轉為Byte[]
            byte[] crypto = sha512.ComputeHash(source);         //進行SHA512加密
                                                                //把加密後的字串從Byte[]轉為字串
            return Convert.ToBase64String(crypto);
        }

        public string Linepayv3Str(string Word,string sercret)
        {
            var encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(sercret);
            byte[] messageBytes = encoding.GetBytes(Word);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        public Auth loginCheck(string word,string pass)
        {
            Auth auth = new Auth();
            SqlConnection sql = new SqlConnection(sqlconn2);
            SqlCommand command1 = new SqlCommand("select username,password,vm_uid,First from admin_user where username=@name");
            command1.Parameters.Add(new SqlParameter("@name",word));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if(reader1.HasRows)
            {
                while (reader1.Read())
                {
                    if (reader1.GetString(reader1.GetOrdinal("password")) == Change(pass))
                    {
                        auth.pass = true;
                        auth.name = reader1.GetString(reader1.GetOrdinal("username"));
                        auth.vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid"));
                        auth.First = reader1.GetInt32(reader1.GetOrdinal("First"));
                        break;
                    }
                    else
                    {
                        auth.pass = false;
                        auth.name = null;
                        auth.vm_uid = -1;
                        break;
                    }
                }
            }
            sql.Close();
            return auth;
        }

        public Auth loginCheck2(string word,string usertype, string pass)
        {
            Auth auth = new Auth();
            SqlConnection sql = new SqlConnection(sqlconn5);
            bool usertypeChk = false;
            string usertypeStr = "";
            SqlCommand command2 = new SqlCommand("select cm_id from customers where isUse=1");
            command2.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command2.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    usertypeStr = reader1.GetString(reader1.GetOrdinal("cm_id"));
                    break;
                }
                usertypeChk = true;
            }
            else
            {
                auth.pass = false;
                auth.name = null;
                auth.cm_id = "";

                usertypeChk = false;
            }
            sql.Close();
            if (usertypeChk == true)
            {
                SqlCommand command1 = new SqlCommand("select username,password,name,cm_id from admin_user where cm_id=@usertype and First=1");
                command1.Parameters.Add(new SqlParameter("@usertype", usertype));
                command1.Connection = sql;
                sql.Open();
                SqlDataReader reader2 = command1.ExecuteReader();
                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        if (reader2.GetString(reader2.GetOrdinal("password")) == Change(pass) && reader2.GetString(reader2.GetOrdinal("username")) == word)
                        {
                            auth.pass = true;
                            auth.name = reader2.GetString(reader2.GetOrdinal("name"));
                            auth.cm_id = reader2.GetString(reader2.GetOrdinal("cm_id"));
                            break;
                        }
                        else
                        {
                            auth.pass = false;
                            auth.name = null;
                            auth.cm_id = "";
                            break;
                        }
                    }
                }
                sql.Close();
            }

            return auth;
        }
    }



    public class Auth
    {
        public bool pass { get; set; }
        public object name { get; set; }
        public int vm_uid { get; set; }
        public string cm_id { get; set; }
        public int First { get; set; }
    }
}