﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using weiddeson2.Models;
using weiddeson2.App_Code;
using System.Runtime.InteropServices;
using System.Web.Helpers;
using System.Runtime.Remoting.Messaging;
using System.Drawing.Printing;
using System.Web.Http.Cors;

namespace weiddeson2.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "GET,PUT,OPTIONS")]
    public class BackendController : Controller
    {
        private vmUserRepository vmuserRepo = new vmUserRepository();
        private MachineRepository machineRepo = new MachineRepository();
        private VmsortRepository VmsortRepo = new VmsortRepository();
        private ProductRepository productRepo = new ProductRepository();
        private MachineLasRepository MachineLasRepo = new MachineLasRepository();
        private OrdersRepository ordersRepo = new OrdersRepository();
        //private DateTime date = DateTime.Now;
        public string nowtime = DateTime.Now.ToString("yyyy/MM/dd");

        /*
         * 地端後台登入畫面(舊)
         */
        public ActionResult Locallogin()
        {
            Session.RemoveAll();
            Session["csrf"] =new csrfToken().ReturnCsrf();
            return View();
        }

        /*
         * 地端後台登入畫面(新)
         */
        public ActionResult Adminlogin()
        {
            Session.RemoveAll();
            Session["csrf"] = new csrfToken().ReturnCsrf();
            return View();
        }

        /*
         * 系統元件-跨網域攻擊驗證器
         */
        public JsonResult csrf()
        {
            DateTime dateTime = new DateTime();
            JsonResult json = new JsonResult();
            csrf cs = new csrf();
            Session["csrf"] = new csrfToken().ReturnCsrf();
            cs.csrfStr = Convert.ToString(Session["csrf"]);
            cs.timetoken = dateTime.ToString("yyyy/MM/dd,HH:mm:ss");
            json.Data = cs;
            return Json(cs,JsonRequestBehavior.AllowGet);
        }

        /*
         *系統元件-地端帳號快速設定
         */
        public ActionResult Createadmin()
        {
            JsonResult json = new JsonResult();
            Vmuser vmuser = vmuserRepo.Createadmin();
            json.Data=vmuser;
            return Redirect("/Backend/Adminlogin");
        }

       /* [HttpPost]
        public JsonResult Createadmin2()
        {
            JsonResult json = new JsonResult();
            Vmuser vmuser = vmuserRepo.Createadmin2();
            json.Data = vmuser;
            return json;
        }*/

        /*
         * 系統元件-設定一般帳號
         */
        public ActionResult CreateAccount()
        {
            Createadmin();
           // Createadmin2();
            vmuserRepo.CreateUser();
            return Redirect("/Backend/adminLogin");
        }

        [HttpPost]
        public JsonResult createDefault()
        {
            return new JsonResult();
        }

        /*
         * 登入流程Ver1
         */
        [HttpPost]
        public JsonResult Loginauth(string adminname,string adminpass,string csrf)
        {
            JsonResult json = new JsonResult();
            json.Data = new errorService().loginError();
            return json;
        }

        /*
         * 登入Ver2
         */
        public JsonResult Auth(string adminname, string adminpass, string csrf)
        {
            Error error = new Error();
            Password password = new Password();

            if (csrf != Convert.ToString(Session["csrf"]))
            {
                error.token = "401";
                error.message = "與伺服器的聯絡中發生不明錯誤。";
            }
            else
            {
                Auth auth = password.loginCheck(adminname, adminpass);
                if (auth.pass)
                {
                    Vmuser vmuserinfo = vmuserRepo.Vmuserinfo(auth.vm_uid);
                    Session["admin"] = vmuserinfo.admin;
                    Session["admin_name"] = vmuserinfo.name;
                    Session["admin_id"] = vmuserinfo.vm_uid;
                    Session["vm_num"] = vmuserinfo.vm_num;
                    error.token = "301";
                    error.message = "";
                    if (auth.First == 1)
                    {
                        error.First = false;
                    }
                    else if(auth.First == 0){
                        error.First = true;
                    }
                }
                else if (!auth.pass)
                {
                    error.token = "401";
                    error.message = "使用者帳號不存在。請重新登入。";
                }

            }
            JsonResult json = new JsonResult();
            json.Data = error;
            return json;
        }

        /*
         * 登入Ver2
        */
        public JsonResult Auth2(string adminname, string adminpass, string csrf,string taxid)
        {
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "*");
            ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
            Error error = new Error();
            Password password = new Password();
            string csrfCheck = Convert.ToString(Session["csrf"]);
            /*if (csrf != csrfCheck)
            {
                error.token = "401";
                error.message = "與伺服器的聯絡中發生不明錯誤。";
            }
            else
            {*/
                Auth auth = password.loginCheck2(adminname,taxid,adminpass);
                if (auth.pass)
                {
                    Vmuser vmuserinfo = vmuserRepo.Vmuserinfo2(taxid);
                    //Session["admin"] = vmuserinfo.admin;
                    //Session["admin_name"] = vmuserinfo.name;
                    //Session["admin_id"] = vmuserinfo.vm_uid;
                    error.token = "301";
                    error.message = "";
                    /*error.vm_uid = vmuserinfo.vm_uid;
                    error.vm_num = vmuserinfo.vm_num;*/
                }
                else if(!auth.pass)
                {
                    error.token = "401";
                    error.message = "使用者帳號不存在。請重新登入。";
                }
                
           // }
            JsonResult json = new JsonResult();
            json.Data = error;
            return json ;
        }

        public ActionResult createTest()
        {
            if (vmuserRepo.createTest())
            {
                return Redirect("/");
            }
            else
            {
                return Redirect("/Backend/Fail");
            } 
        }

        // GET: Backend
        /*
         * 地端後臺首頁
         */
        public ActionResult Index()
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                ViewData["uuid"] = Convert.ToString(Session["vm_num"]);

                return View();
            }
        }

        /*
         * 地端商品庫資料表
         */
        [HttpGet]
        public ActionResult Productlist(string sort="0")
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                ViewData["uuid"] = "機器編號:" + Convert.ToString(Session["vm_num"]) + "/ 機器ID:" + Convert.ToInt32(Session["admin_id"]) + "/ 用戶ID:" +Convert.ToString(Session["admin"]) + "/ 商品庫";
                ViewBag.vmsort = VmsortRepo.GetVmsorts(Convert.ToInt32(Session["admin_id"]));
                ViewBag.product = productRepo.GetMainproducts(sort);
                ViewData["nowSort"] = sort;
                return View();
            }
        }

        /*
         * 地端商品庫資料新增
         */
        public ActionResult Productinsert()
        {
            
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                //Session["csrf"] = new csrfToken().ReturnCsrf();
                ViewData["uuid"] = "設備商品新增";
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                ViewBag.vmsort = VmsortRepo.GetProductVmsorts(Convert.ToInt32(Session["admin_id"]));
                return View();
            }
        }

        /*
         * 地端商品庫資料編輯
         */
        [HttpGet]
        public ActionResult Productedit(int id=0)
        {

            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                //Session["csrf"] = new csrfToken().ReturnCsrf();
                ViewData["uuid"] = "設備商品新增";
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                ViewBag.data = productRepo.GetProductinfo(id);
                ViewBag.vmsort = VmsortRepo.GetProductVmsorts(Convert.ToInt32(Session["admin_id"]));
                return View();
            }
        }

        /*
         * 地端商品庫資料刪除
         */
        public ActionResult Productdelete(int id)
        {
            if (productRepo.deleteProductLocal(id))
            {
                return Redirect("/Backend/Productlist");
            }
            else
            {
                return Redirect("/Backend/Productlist");
            }
        }

        /*
         * 機器商品設定資料列表
         */
        public ActionResult MachineProlist(string layer="")
        {
            string sort = "0";
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {

                Session["csrf"] = new csrfToken().ReturnCsrf();
                int roadLayers = 0;
                if (layer != "")
                {
                    roadLayers = Convert.ToInt32(layer);
                }

                int count = 0;

                foreach (MachineLans lans in MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"])))
                {
                    count++;
                }

                if (roadLayers > count)
                {
                    return Redirect("/Backend/MachineProlist");
                }

                ViewData["uuid"] = "商品清單 機器編號:" + Convert.ToString(Session["vm_num"]) + "/ ID:" + Convert.ToInt32(Session["admin_id"]) + "";
                ViewBag.LansLayer = MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]));
                ViewBag.product = productRepo.getMachineProduct(layer,MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])));
                ViewBag.productList = productRepo.GetMainproducts(sort);
                ViewData["type"] = MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"]));
                ViewData["layer"] = layer;
                return View();
            }
        }

        /*
         * 機器貨道商品資料新增
         */
        public ActionResult MachineProinsert(int id = 0)
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                string sort = "0";
                //Session["csrf"] = new csrfToken().ReturnCsrf();
                ViewData["uuid"] = "機器貨道商品新增";
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                ViewData["model"] = machineRepo.getModel(Convert.ToString(Session["admin_id"]));
                ViewData["type"] = MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"]));
                ViewBag.product = productRepo.GetMainproducts(sort);
                ViewData["Layer"] = id;
                ViewBag.LansLayer = MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]));
                return View();
            }
        }

        /*
         * 機器貨道商品資料編輯
         */
        public ActionResult MachineProedit(int id = 0)
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                string sort = "0";
                //Session["csrf"] = new csrfToken().ReturnCsrf();
                Product product = new Product();
                List<MachineLans> lans = new List<MachineLans>();
                MachineLans lans1 = new MachineLans();
                product = productRepo.Getproduct(id, MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])));

                lans = MachineLasRepo.GetmachineLansmore(MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])), MachineLasRepo.getLayerinfo(MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])), Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]), product.roadId));
                ViewBag.lans = lans;
                ViewData["uuid"] = "機器貨道商品新增";
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                ViewData["model"] = machineRepo.getModel(Convert.ToString(Session["admin_id"]));
                ViewData["type"] = MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"]));
                ViewBag.productinfo = product;
                ViewBag.product = productRepo.GetMainproducts(sort);
                ViewBag.LansLayer = MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]));

                return View();
            }
        }

        /*
         * 機器貨道控制程式
         */
        public ActionResult Controllist()
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                Session["csrf"] = new csrfToken().ReturnCsrf();
                ViewData["uuid"] = Convert.ToString(Session["admin_id"]);
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                return View();
            }
        }

        /*
         * 貨道資料設定
         */
        public ActionResult Lanset(string road_layer="")
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                int roadLayers = 0;
                if (road_layer != "")
                {
                    roadLayers = Convert.ToInt32(road_layer);
                }

                int count = 0;
                
                foreach(MachineLans lans in MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"])))
                {
                    count++;
                }

                if (roadLayers > count)
                {
                    return Redirect("/Backend/Lanset");
                }

                Session["csrf"] = new csrfToken().ReturnCsrf();

                ViewData["uuid"] = Convert.ToString(Session["admin_id"]);
                ViewData["vmnum"] = Convert.ToString(Session["vm_num"]);
                ViewBag.LansLayer = MachineLasRepo.machineLayer(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]));
                ViewBag.Lans = MachineLasRepo.machineLans(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]), road_layer);
                ViewData["layer"] = road_layer;
                return View();
            }
        }

        /*
         * 快速按鈕-貨道可安排數量減少
         */
        public string Qtyminus(string roadid)
        {
            string roadqty = "";
            roadqty = MachineLasRepo.Qtyminus(roadid,Convert.ToInt32(Session["admin_id"]));
            productRepo.returnQty(roadid, Convert.ToInt32(Session["admin_id"]),roadqty);
            return roadqty;
        }

        /*
         * 快速按鈕-貨道可安排數量新增
         */
        public string Qtyplus(string roadid)
        {
            string roadqty = "";
            roadqty = MachineLasRepo.Qtyplus(roadid, Convert.ToInt32(Session["admin_id"]));
            productRepo.returnQty(roadid, Convert.ToInt32(Session["admin_id"]), roadqty);
            return roadqty;
        }

        /*
         * 快速按鈕-貨道功能關閉
         */
        public bool Roadoff(string roadid)
        {
            return MachineLasRepo.Roadoff(roadid, Convert.ToInt32(Session["admin_id"]));
        }

        /*
         * 快速按鈕-貨道功能開啟
         */
        public bool Roadon(string roadid)
        {
            return MachineLasRepo.Roadon(roadid, Convert.ToInt32(Session["admin_id"]));
        }


        public string ProductQtymiuns(string roadid)
        {
            string roadqty = "";
            roadqty = MachineLasRepo.ProductQtymiuns(roadid, Convert.ToInt32(Session["admin_id"]));
            return roadqty;
        }

        public string ProductQtyplus(string roadid)
        {
            string roadqty = "";
            roadqty = MachineLasRepo.ProductQtyplus(roadid, Convert.ToInt32(Session["admin_id"]));
            return roadqty;
        }

        public ActionResult LansetForm(string id)
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                Session["csrf"] = new csrfToken().ReturnCsrf();

                ViewBag.Lans = MachineLasRepo.GetmachineLans(Convert.ToInt32(Session["admin_id"]), Convert.ToString(Session["vm_num"]), id);
                return View();
            }
        }

        public ActionResult GroupCreatePage()
        {
            Session["csrf"] = new csrfToken().ReturnCsrf();
            return View();
        }

        public bool RoadCheck(int id)
        {
            return MachineLasRepo.roadCheck(id);
        }

        public bool RoadReset(string roadid,string date1,string date2,string time1)
        {
            return productRepo.Roadreset(roadid,date1,date2,time1);
        }

        public ActionResult Fillup2()
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                Session["csrf"] = new csrfToken().ReturnCsrf();
                return View();
            }
        }

        public ActionResult cloudToLocal()
        {
            if (productRepo.getClouddata(Convert.ToString(Session["vm_num"]), Convert.ToInt32(Session["admin_id"]))){
                return Redirect("/Backend/Productlist");
            }
            else
            {
                return Redirect("/Backend/Productlist");
            }
        }

        public ActionResult getCloudRoadinfo()
        {
            if (productRepo.getCloudRoadinfo(Convert.ToString(Session["vm_num"]), Convert.ToInt32(Session["admin_id"])))
            {
                return Redirect("/Backend/MachineProlist");
            }
            else
            {
                return Redirect("/Backend/MachineProlist");
            }
        }

        [HttpGet]
        public ActionResult salesReport(string start="", string end="",string type="")
        {
            if (Convert.ToString(Session["admin_id"]) == "")
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                Session["csrf"] = new csrfToken().ReturnCsrf();
                ViewBag.salse = ordersRepo.getSalselist(start,end,Convert.ToInt32(Session["admin_id"]),type);
                return View();
            }
        }

        public string chnage(string word= "lSgph1HU2Pnx")
        {
            Password password = new Password();
            return password.Change(word);
        }

        public bool GroupProduct(int id,int product)
        {
            return productRepo.groupProduct(id,product);
        }

    }



    public class csrf
    {
        public string csrfStr { get; set; }
        public string timetoken { get; set; } 
    }
}