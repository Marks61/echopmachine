﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Web.UI.WebControls;
using weiddeson2.App_Code;
using weiddeson2.Models;

namespace weiddeson2.Controllers
{
    public class IndexController : Controller
    {
        //機器相關 
        MachineRepository machineRepo = new MachineRepository();
        //訂單相關
        OrdersRepository ordersRepo = new OrdersRepository();

        // GET: Index
        /*
         * 販賣機首頁
         */
        public ActionResult Index()
        {
            csrfToken csrfToken = new csrfToken();
            Session.RemoveAll();
            Session["CSRF_TOKEN"] = csrfToken.ReturnCsrf();
            Session["listQty"] = new List<Product>();
            ViewData["vmcode"] = machineRepo.getVmuid();
            ProductRepository product = new ProductRepository();
            Session["overTime"] = new List<Product>();
            List<Product> productList = new List<Product>();
            //productList = 
            productList = product.frontedGetLayerVer2();
            foreach(Product product2 in product.frontedGetLayers())
            {
                overTime(product2.expiryDate,product2);
            }
            foreach (Product product1 in productList)
            {
                listQty(product1.proQty, product1);
            }
            //sendMail((List<Product>)Session["overTime"]);
            //sendMail2((List<Product>)Session["listQty"]);
            ViewBag.productList = productList;
            return View();
        }

        /*
         * 過期商品篩選
         */
        public bool overTime(string Datatitme, Product product)
        {
            DateTime date = DateTime.Now;
            DateTime date1 = Convert.ToDateTime(Datatitme);
            if (date >= date1.AddDays(-3))
            {
                List<Product> products = new List<Product>();
                products = (List<Product>)Session["overTime"];
                products.Add(product);
                Session["overTime"] = products;
            }
            return true;
        }

        /*
         *即將售鑿商品篩選
         */
        public bool listQty(int qty, Product product)
        {

            if (qty <= 3)
            {
                List<Product> products = new List<Product>();
                products = (List<Product>)Session["listQty"];
                products.Add(product);
                Session["listQty"] = products;
            }
            return true;
        }

        /*
         * 過期商品通知寄出
         */
        public bool sendMail()
        {
            int num = 0;
            int count = 1;
            foreach (Product product in (List<Product>)Session["overTime"])
            {
                num++;
            }

            mailInfo mail = getmailInfo(machineRepo.getVmuid());

            if (num > 0)
            {
                System.Net.Mail.SmtpClient mmail = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                mmail.Credentials = new System.Net.NetworkCredential("smaacg2011", "mmo357653");
                mmail.EnableSsl = true;
                string frm = "smaacg2011@gmail.com";
                string sject = mail.name + "-商品即將過期通知";
                string by = "下述商品即將過期，請台主多加留意。";
                foreach (Product product in (List<Product>)Session["overTime"])
                {
                    by += product.pdName+"(貨道"+product.roadId+");";
                }
                mmail.Send(frm, mail.email, sject, by);
            }

            return true;
        }

        /*
         * 寄出商品即將售鑿通知
         */
        public bool sendMail2()
        {
            int num = 0;
            int count = 1;
            foreach (Product product in (List<Product>)Session["listQty"])
            {
                num++;
            }

            mailInfo mail = getmailInfo(machineRepo.getVmuid());

            if (num > 0)
            {
                System.Net.Mail.SmtpClient mmail = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                mmail.Credentials = new System.Net.NetworkCredential("smaacg2011", "mmo357653");
                //mmail.Credentials = new System.Net.NetworkCredential("service.weideson", "weideson18915");
                mmail.EnableSsl = true;
                //string frm = "service.weideson@gmail.com";
                string frm = "smaacg2011@gmail.com";
                string sject = mail.name + "-商品存貨不足通知";
                string by = "下述商品即將售鑿，請台主多加留意。";
                foreach (Product product in (List<Product>)Session["listQty"])
                {
                    by += product.pdName+";";
                }
                mmail.Send(frm, mail.email, sject, by);
            }

            return true;
        }

        /*
         * 取得收件人訊息
         */
        public mailInfo getmailInfo(int uid)
        {
            return machineRepo.getMailinfo(uid);
        }

        /*
         * 將當日銷售紀錄回傳雲端系統
         */
        public JsonResult salseReportReturn(string vmcode,string date1)
        {
            List<Orders> orders=ordersRepo.LocaltoCloud(vmcode,date1);
            int i = 0;
            int j = 0;
            foreach (Orders orders1 in orders)
            {
                i++;
            }
            JsonResult linepayinfo = new JsonResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://192.168.0.100:4404/Api2/salseReportReturn");
            httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip;
            // 加入憑證驗證
            httpWebRequest.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string requestJSON = "";
                requestJSON += "[";
                foreach(Orders orders12 in orders)
                {
                    j++;
                    if (j == i)
                    {
                        requestJSON += "{\"od_id\": " + orders12.od_id + ",";
                        requestJSON += "\"pdname\": \"" + orders12.pdname + "\",";
                        requestJSON += "\"roadid\": \"" + orders12.roadid + "\",";
                        requestJSON += "\"road_num\": \"" + orders12.road_num + "\",";
                        requestJSON += "\"receive\": \"" + orders12.receive + "\",";
                        requestJSON += "\"vm_num\": \"" + orders12.vm_num + "\",";
                        requestJSON += "\"pro_id\": \"" + orders12.pro_id + "\",";
                        requestJSON += "\"ordernum\": \"" + orders12.ordernum + "\",";
                        requestJSON += "\"order_date\": \"" + orders12.order_date + "\",";
                        requestJSON += "\"order_time\": \"" + orders12.order_time + "\",";
                        requestJSON += "\"order_type\": \"" + orders12.order_type + "\",";
                        requestJSON += "\"incom_type\": \"" + orders12.incom_type + "\",";
                        requestJSON += "\"pay_mode\": \"" + orders12.pay_mode + "\",";
                        requestJSON += "\"pay_type\": \"" + orders12.pay_type + "\",";
                        requestJSON += "\"site_type\": \"" + orders12.site_type + "\",";
                        requestJSON += "\"forder_date\": \"" + orders12.forder_date + "\",";
                        requestJSON += "\"forder_time\": \"" + orders12.forder_time + "\",";
                        requestJSON += "\"unit_price\": \"" + orders12.unit_price + "\",";
                        requestJSON += "\"cost_price\": \"" + orders12.cost_price + "\",";
                        requestJSON += "\"vm_uid\": " + orders12.vm_uid + ",";
                        requestJSON += "\"bonus\": \"" + orders12.bonus + "\",";
                        requestJSON += "\"point\": \"" + orders12.point + "\",";
                        requestJSON += "\"od_repdate\": \"" + orders12.od_repdate + "\",";
                        requestJSON += "\"is_update\": " + orders12.is_update + ",";
                        requestJSON += "\"cash_in\": \"" + orders12.cash_in + "\",";
                        requestJSON += "\"cash_out\": \"" + orders12.cash_out + "\"}";
                    }
                    else
                    {
                        requestJSON += "{\"od_id\": " + orders12.od_id + ",";
                        requestJSON += "\"pdname\": \"" + orders12.pdname + "\",";
                        requestJSON += "\"roadid\": \"" + orders12.roadid + "\",";
                        requestJSON += "\"road_num\": \"" + orders12.road_num + "\",";
                        requestJSON += "\"receive\": \"" + orders12.receive + "\",";
                        requestJSON += "\"vm_num\": \"" + orders12.vm_num + "\",";
                        requestJSON += "\"pro_id\": \"" + orders12.pro_id + "\",";
                        requestJSON += "\"ordernum\": \"" + orders12.ordernum + "\",";
                        requestJSON += "\"order_date\": \"" + orders12.order_date + "\",";
                        requestJSON += "\"order_time\": \"" + orders12.order_time + "\",";
                        requestJSON += "\"order_type\": \"" + orders12.order_type + "\",";
                        requestJSON += "\"incom_type\": \"" + orders12.incom_type + "\",";
                        requestJSON += "\"pay_mode\": \"" + orders12.pay_mode + "\",";
                        requestJSON += "\"pay_type\": \"" + orders12.pay_type + "\",";
                        requestJSON += "\"site_type\": \"" + orders12.site_type + "\",";
                        requestJSON += "\"forder_date\": \"" + orders12.forder_date + "\",";
                        requestJSON += "\"forder_time\": \"" + orders12.forder_time + "\",";
                        requestJSON += "\"unit_price\": \"" + orders12.unit_price + "\",";
                        requestJSON += "\"cost_price\": \"" + orders12.cost_price + "\",";
                        requestJSON += "\"vm_uid\": " + orders12.vm_uid + ",";
                        requestJSON += "\"bonus\": \"" + orders12.bonus + "\",";
                        requestJSON += "\"point\": \"" + orders12.point + "\",";
                        requestJSON += "\"od_repdate\": \"" + orders12.od_repdate + "\",";
                        requestJSON += "\"is_update\": " + orders12.is_update + ",";
                        requestJSON += "\"cash_in\": \"" + orders12.cash_in + "\",";
                        requestJSON += "\"cash_out\": \"" + orders12.cash_out + "\"},";
                    }
                }

                requestJSON += "]";

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(status));
                    status model = (status)deseralizer.ReadObject(ms);// //反序列化ReadObject

                    linepayinfo.Data = model;

                }
            }
            return linepayinfo;
        }

        /*
         * 取得販賣機溫度
         */
        public JsonResult getTemprature(int vmuid)
        {
            Temprature temprature = machineRepo.GetTemprature(vmuid);
            JsonResult json = new JsonResult();
            json.Data = temprature;
            return Json(temprature, JsonRequestBehavior.AllowGet);
        }

        public class mailInfo
        {
            public string cm_id { get; set; }
            public string st_id { get; set; }
            public int vm_uid { get; set; }
            public string email { get; set; }
            public string name { get; set; }
        }
    }
}