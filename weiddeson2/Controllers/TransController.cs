﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using weiddeson2.Models;
using weiddeson2.App_Code;
using Newtonsoft.Json;
using System.Net.Security;

namespace weiddeson2.Controllers
{
    public class TransController : Controller
    {
        private ProductRepository productRepo = new ProductRepository();
        private CashtempRepository cashtempRepo = new CashtempRepository();
        private OrdersRepository ordersRepo = new OrdersRepository();

        //建立交易資料暫存檔
        [HttpGet]
        // GET: Trans
        public int getRealProce(int roadid)
        {
            Session["Roadid"] = roadid;
            Session["price"] = productRepo.getRealPrice(roadid);
            Session["cash_in"] = 0;
            Session["noncash"] = Convert.ToInt32(Session["price"]) - Convert.ToInt32(Session["cash_in"]);
            Session["cash_out"] = 0;
            return productRepo.getRealPrice(roadid);
        }

        //建立交易資料暫存檔
        [HttpGet]
        // GET: Trans
        public int getRealProce2(int roadid)
        {
            Session["Roadid"] = productRepo.getRealId(roadid);
            Session["price"] = productRepo.getRealPrice(Convert.ToInt32(Session["Roadid"]));
            Session["cash_in"] = 0;
            Session["noncash"] = Convert.ToInt32(Session["price"]) - Convert.ToInt32(Session["cash_in"]);
            Session["cash_out"] = 0;
            return productRepo.getRealPrice(Convert.ToInt32(Session["Roadid"]));
        }

        //初始化錢箱，將商品銷售價格寫入
        [HttpPost]
        public bool cashstampStart()
        {
            if (cashtempRepo.cashtempStart(Convert.ToInt32(Session["price"])))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //保護機制--避免前台資料遭受竄改(適應舊版型，新版型沿用)
        [HttpPost]
        public JsonResult checkData()
        {
            checkDataForamt checkData = new checkDataForamt
            {
                product = Convert.ToInt32(Session["product"]),
                price = Convert.ToInt32(Session["price"])
            };

            JsonResult jsonResult = new JsonResult();
            jsonResult.Data = checkData;
            return jsonResult;
        }

        //計算錢箱狀態並回傳即時資料
        [HttpPost]
        public JsonResult getCashin()
        {
            Cashtemp cashtemp = new Cashtemp();
            JsonResult json = new JsonResult();
            Cashtemp getCashtemp = cashtempRepo.getCashin();
            if (getCashtemp.coollision == true)
            {
                cashtemp.cashIn = Convert.ToString(Session["cash_in"]);
                cashtemp.salsePrice = Convert.ToString(Session["price"]);
                cashtemp.cashOut = Convert.ToString(Session["cash_out"]);
                cashtemp.nonCash = Convert.ToInt32(Session["noncash"]);
                if (Convert.ToInt32(cashtemp.cashIn) - Convert.ToInt32(cashtemp.salsePrice) >= 0)
                {
                    cashtemp.buySuccess = true;
                }
                else
                {
                    cashtemp.buySuccess = false;
                }

                json.Data = cashtemp;
            }
            else
            {
                Session["cash_in"] = getCashtemp.cashIn;
                Session["noncash"] = getCashtemp.nonCash;
                Session["cash_out"] = getCashtemp.cashOut;
                json.Data = getCashtemp;
            }

            return json;
        }

        //寫入銷售資料
        [HttpPost]
        public JsonResult Salse(string csrf)
        {
            JsonResult json = new JsonResult();
            checkData returnData = new checkData();
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = date1.ToString("yyMMddffffff") + num; //訂單編號規則:年(4)+月(2)+日(2)+小時(2)+分鐘(2)+秒數(2)+毫秒(4)+random(1單位，0~9)，總長19碼
            Session["dingdan"] = dingdan;
            if (csrf == Convert.ToString(Session["CSRF_TOKEN"]))
            {
                if (ordersRepo.paymentAct(Convert.ToString(Session["Roadid"]), date1, dingdan, Session["cash_in"]))
                {
                    //productRepo.updateQtyData(Convert.ToString(Session["Roadid"]));
                    //cashtempRepo.Cashout2();
                }
                returnData.tokenCheck = true;
            }
            else
            {
                returnData.tokenCheck = false;
            }
            json.Data = returnData;
            return json;
        }

        //退幣
        [HttpPost]
        public bool Cashout()
        {
            return cashtempRepo.Cashout(Convert.ToInt32(Session["cash_in"]));
        }

        //銷售退幣流程
        [HttpPost]
        public bool Cashout2()
        {
            return cashtempRepo.Cashout2();
        }


        //退幣後初始化錢箱
        [HttpPost]

        public bool ResetCashtemp()
        {
            return cashtempRepo.resetCashtemp();
        }


        //完成銷售流程
        /*[HttpPost]

        public bool Salsecomplete()
        {
            DateTime date1 = DateTime.Now;

            if(ordersRepo.Salsecomplete(Convert.ToString(Session["dingdan"]), date1)){
                return ResetCashtemp();
            }
            else
            {
                return false;
            }
        }*/

        //v2

       [HttpPost]
        public JsonResult LinepayOrder()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = date1.ToString("yyyyMMddHHmmssffff") + num;
            Session["dingdan"] = dingdan;

            JsonResult linepayinfo = new JsonResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://sandbox-api-pay.line.me/v2/payments/request");
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("X-LINE-ChannelId", System.Configuration.ConfigurationManager.AppSettings["lineChannleid"]);
            httpWebRequest.Headers.Add("X-LINE-ChannelSecret", System.Configuration.ConfigurationManager.AppSettings["lineChannlekey"]);
            int i = 0;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                string requestJSON = "{\"productName\": \"" + Convert.ToString(productRepo.getRealName(Convert.ToInt32(Session["Roadid"]))) + "\"," +

                                    "\"productImageUrl\": \"" + "https://" + System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/images/pic/product/" + Convert.ToString(productRepo.getRealPic(Convert.ToInt32(Session["Roadid"]))) + "\"," +

                                    "\"amount\": " + Convert.ToInt32(Session["price"]) + "," +

                                    "\"currency\": \"TWD\"," +

                                    "\"orderId\": \"" + dingdan + "\"," +

                                    "\"confirmUrl\": \"" + "https://"+ System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/Trans/LinePay" + "\"," +

                                    "\"cancelUrl\": \"" + "https://"+ System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/Trans/Cancle" + "\"," +

                                    "\"capture\": \"true\"," +

                                    "\"confirmUrlType\": \"CLIENT\"}";

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Linepayorder));
                    Linepayorder model = (Linepayorder)deseralizer.ReadObject(ms);// //反序列化ReadObject
    
                    linepayinfo.Data = model;
                }
            }
            return linepayinfo;
        }

        [HttpPost]

        public JsonResult LinepayOffline(string linepay)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = date1.ToString("yyMMddffffff") + num;
            Session["dingdan"] = dingdan;

            JsonResult linepayinfo = new JsonResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api-pay.line.me/v2/payments/oneTimeKeys/pay");
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("X-LINE-ChannelId", System.Configuration.ConfigurationManager.AppSettings["lineChannleidoff"]);
            httpWebRequest.Headers.Add("X-LINE-ChannelSecret", System.Configuration.ConfigurationManager.AppSettings["lineChannlekeyoff"]);
            int i = 0;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                string requestJSON = "{\"productName\": \"" + Convert.ToString(productRepo.getRealName(Convert.ToInt32(Session["Roadid"]))) + "\"," +

                                    "\"amount\": " + Convert.ToInt32(Session["price"]) + "," +

                                    "\"currency\": \"TWD\"," +

                                    "\"orderId\": \"" + dingdan + "\"," +

                                    "\"oneTimeKey\":\""+linepay+"\","+

                                    "\"capture\": \"true\"" +"}";

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Linepayoffer));
                    Linepayoffer model = (Linepayoffer)deseralizer.ReadObject(ms);// //反序列化ReadObject

                    linepayinfo.Data = model;
                }
            }
            return linepayinfo;
        }

        //v3order

        [HttpPost]
        public JsonResult LinepayOrderv3()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = date1.ToString("yyyyMMddHHmmssffff") + num;
            Session["dingdan"] = dingdan;
            string nonse = Convert.ToString(Guid.NewGuid());
            string requestJSON = "{\"amount\": " + Convert.ToInt32(Session["price"]) + "," +
                    
                "\"currency\": \"TWD\"," +
                 
                "\"orderId\": \"" + dingdan + "\"," +
                
                "\"packages\":[{" +
                
                "\"id\":\"" + System.Configuration.ConfigurationManager.AppSettings["machine"] + "\"," +
                
                "\"amount\":" + Convert.ToInt32(Session["price"]) + "," +
                
                "\"name\":\"" + System.Configuration.ConfigurationManager.AppSettings["shopname"] + "\"," +
                
                "\"products\":[{" +
                
                "\"name\":\"" + Convert.ToString(productRepo.getRealName(Convert.ToInt32(Session["Roadid"]))) + "\"," +
                
                "\"imageUrl\":\""+ "https://" + System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/images/pic/product/" + Convert.ToString(productRepo.getRealPic(Convert.ToInt32(Session["Roadid"]))) + "\","+
                
                "\"quantity\":1,"+ 
                
                "\"price\":"+ Convert.ToInt32(Session["price"]) +
                
                "}]"+
                
                "}],"+
                
                "\"redirectUrls\":{"+
                
                "\"confirmUrl\": \"" + "https://" + System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/Trans/Linepay" + "\"," +
                               
                "\"confirmUrlType\": \"CLIENT\"," +

                "\"cancelUrl\": \"" + "https://" + System.Configuration.ConfigurationManager.AppSettings["webRoute"] + "/Trans/Cancle" + "\"" +
                
                "},"+

                "\"options\":{" +

                "\"payment\":{"+
                                   
                "\"capture\": \"true\"" +
            
                "}" +
            
                "}}";

            string AutohorizationStr = System.Configuration.ConfigurationManager.AppSettings["lineChannlekey"] + "/v3/payments/request" + requestJSON + nonse;
            Password password = new Password();
            string Autohorization = password.Linepayv3Str(AutohorizationStr, System.Configuration.ConfigurationManager.AppSettings["lineChannlekey"]);
            JsonResult linepayinfo = new JsonResult();

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://sandbox-api-pay.line.me/v3/payments/request");
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("X-LINE-ChannelId", System.Configuration.ConfigurationManager.AppSettings["lineChannleid"]);
            httpWebRequest.Headers.Add("X-LINE-Authorization-Nonce",nonse);
            httpWebRequest.Headers.Add("X-LINE-Authorization",Autohorization);

            int i = 0;
            
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Linepayorder));
                    Linepayorder model = (Linepayorder)deseralizer.ReadObject(ms);// //反序列化ReadObject

                    linepayinfo.Data = model;
                }
            }
            return linepayinfo;
        }

        [HttpGet]
        public ActionResult Linepay(string transactionId,string orderId="")
        {
            csrfToken csrfToken = new csrfToken();
            Session["CSRF_TOKEN"] = csrfToken.ReturnCsrf();
            ViewBag.transactionid = transactionId;

            return View();
        }

        [HttpPost]
        public JsonResult LinePayreturn(string transactionid)
        {
            JsonResult linepayinfo = new JsonResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://sandbox-api-pay.line.me/v2/payments/" + transactionid + "/confirm");
            httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip;
            // 加入憑證驗證
            httpWebRequest.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("X-LINE-ChannelId", System.Configuration.ConfigurationManager.AppSettings["lineChannleid"]);
            httpWebRequest.Headers.Add("X-LINE-ChannelSecret", System.Configuration.ConfigurationManager.AppSettings["lineChannlekey"]);
            int i = 0;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                string requestJSON = "{\"amount\": " + Convert.ToInt32(Session["price"]) + "," +

                                    "\"currency\": \"TWD\"}";

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(Linepayoffer));
                    Linepayoffer model = (Linepayoffer)deseralizer.ReadObject(ms);// //反序列化ReadObject

                    linepayinfo.Data = model;

                }
            }
            return linepayinfo;
        }

        //寫入銷售資料
        [HttpPost]
        public bool LinePaySalse(string csrf,int amount,string transid)
        {
            DateTime date1 = DateTime.Now;
            if (csrf == Convert.ToString(Session["CSRF_TOKEN"]))
            {
                if (ordersRepo.LinepaymentAct(Convert.ToString(Session["Roadid"]), date1, Convert.ToString(Session["dingdan"]),amount,transid)) //Convert.ToInt32(Session["price"])))
                {
                    linePaysync(amount,transid);
                }
                return true;
            }
            else
            {
                return true;
            }
        }

        public JsonResult linePaysync(int amount,string transid)
        {
            string dingdan = Convert.ToString(Session["dingdan"]);
            JsonResult linepayinfo = new JsonResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://35.201.220.177/Api2/Linepaysalse");
            httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip;
            // 加入憑證驗證
            httpWebRequest.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                string requestJSON = "{\"amount\": " +amount + "," +

                                    "\"order\": \""+dingdan+"\","+
                                    "\"transid\": \"" + transid + "\"}";

                streamWriter.Write(requestJSON);

                streamWriter.Flush();

                streamWriter.Close();

            }

            //取得回覆資訊

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            //解讀回覆資訊

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))

            {

                string responseJSON = streamReader.ReadToEnd();
                streamReader.Close();

                //將 JSON 轉為物件

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseJSON)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(status));
                    status model = (status)deseralizer.ReadObject(ms);// //反序列化ReadObject

                    linepayinfo.Data = model;

                }
            }
            return linepayinfo;
        }
    }

    public class status
    {
        public string Status { get; set; }
        public string message { get; set; }
    }

    public class checkDataForamt
    {
        public int product { get; set; } //驗證項目--貨道
        public int price { get; set; } //驗證項目--價格
    }

    public class checkData
    {
        public bool tokenCheck { get; set; } //CSRF驗證
        public string token { get; } = Convert.ToString("00010");
    }

    public class Linepayorder
    {
        public string returnCode { get; set; }
        public string returnMessage { get; set; }
        public LinePayorderinfo info { get; set; }
    }

    public class LinePayorderinfo
    {
        public LinePayUrl paymentUrl { get; set; }
        public string transactionId { get; set; }
        public string paymentAccessToken { get; set; }

    }

    public class LinePayUrl
    {
        public string web { get; set; }
        public string app { get; set; }
    }

    public class Linepayoffer
    {
        public string returnCode { get; set; }
        public string returnMessage { get; set; }
        public Linepayofferinfo info { get; set; }
        public string needCheck { get; set; }
        public string transactionDate { get; set; }
    }

    public class Linepayofferinfo
    {
        public string transactionId { get; set; }
        public string orderId { get; set; }
        public List<LineCreditinfo> payInfo { get; set; }
    }

    public class LineCreditinfo
    {
        public string method { get; set; }
        public string amount { get; set; }
        public string maskedCreditCardNumber { get; set; }
    }
}
