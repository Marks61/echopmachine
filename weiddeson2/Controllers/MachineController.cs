﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using weiddeson2.Models;
using System.Web.Http.Cors;
using System.Net;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using System.Web.Http;

namespace weiddeson2.Controllers
{
    public class MachineController : Controller
    {
        MachineRepository MachineRepo = new MachineRepository();
        OrdersRepository ordersRepo = new OrdersRepository();
        ProductRepository productRepo = new ProductRepository();
        MachineLasRepository MachineLasRepo = new MachineLasRepository();

        // GET: Machine
        public ActionResult Index()
        {
            return View();
        }

        [System.Web.Http.HttpGet]
        public JsonResult getTemprature()
        {
            Temprature temprature = MachineRepo.GetTemprature(Convert.ToInt32(Session["admin_id"]));
            JsonResult json = new JsonResult();
            json.Data = temprature;
            return Json(temprature,JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpGet]
        public JsonResult Todaysalse(string date)
        {
            TodaySalse todaySalse =ordersRepo.TodaySalse(Convert.ToInt32(Session["admin_id"]),date);
            JsonResult json = new JsonResult();
            json.Data = todaySalse;
            return Json(todaySalse, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public ActionResult Insertproduct(string bigsortid,string csrf,string pdname,int unit_price,int cost_price,int amount,string shelf_life_day, HttpPostedFileBase file,string content1)
        {
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = date1.ToString("yyyyMMddHHmmssff") + num;
            string filename = "pro" + dingdan + "."+Convert.ToString(file.ContentType).Split('/')[1];
            string csrfCheck = Convert.ToString(Session["csrf"]);

            if (csrf != csrfCheck)
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                //檢查是否有選擇檔案
                if (file != null)
                {
                    string savedName = Path.Combine(Server.MapPath("~/images/pic/Product"),"pro"+dingdan+"."+ Convert.ToString(file.ContentType).Split('/')[1]);
                    file.SaveAs(savedName);

                    productRepo.insertProduct(bigsortid,pdname,unit_price,cost_price,content1,amount,shelf_life_day,filename, MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num,Convert.ToInt32(Session["admin_id"]));
                }
                return Redirect("/Backend/Productlist");
            }
            
        }

        [System.Web.Http.HttpGet]
        public JsonResult mailTest()
        {
            SmtpClient mmail = new SmtpClient("smtp.gmail.com", 587);
            mmail.Credentials = new NetworkCredential("service.weideson", "weideson18915");
            mmail.EnableSsl = true;
            string frm = "service.weideson@gmail.com";
            string o = "ericli@weideson.com";
            string sject = "信件測試";
            var by = "信件測試。";
            mmail.Send(frm, o, sject, by);

            return Json(new Error()
            {
                pageError = "success"
            }, JsonRequestBehavior.AllowGet);

        }

        [System.Web.Http.HttpPost]
        public ActionResult editProduct(int id,string bigsortid, string csrf, string pdname, int unit_price, int cost_price, int amount, string shelf_life_day, HttpPostedFileBase file, string content1)
        {
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string filename = "";

            string csrfCheck = Convert.ToString(Session["csrf"]);

            if (csrf != csrfCheck)
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                //檢查是否有選擇檔案
                if (file != null)
                {
                    string num = Convert.ToString(randoms.Next(0, 9));
                    string dingdan = date1.ToString("yyyyMMddHHmmssff") + num;
                    filename = "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1];
                    string savedName = Path.Combine(Server.MapPath("~/images/pic/Product"), "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1]);
                    file.SaveAs(savedName);
                }

                Mainproduct mainproduct = new Mainproduct();

                mainproduct=productRepo.editProduct(id,bigsortid, pdname, unit_price, cost_price, content1, amount, shelf_life_day, filename);

                return Redirect("/Backend/Productlist");
            }
        }

        [EnableCors(origins: "https://192.168.50.124", headers: "*", methods: "*")]
        public JsonResult Lanset(string vm,string sort)
        {
            List<MachineLans> machineLans = new List<MachineLans>();
            JsonResult json = new JsonResult();
            machineLans=MachineLasRepo.machineLans(Convert.ToInt32(Session["admin_id"]), MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num, sort);
            json.Data = machineLans;
            return Json(machineLans, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LanSetlayer(string sort,string type)
        {
            List<MachineLans> machineLans = new List<MachineLans>();
            JsonResult json = new JsonResult();
            machineLans = MachineLasRepo.machineLans(Convert.ToInt32(Session["admin_id"]), MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num, sort);
            json.Data = machineLans;
            return Json(machineLans, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public ActionResult MachineProinsert(int product,string layer,string road2,string road,int proqty,string csrf)
        {
            if (csrf != Convert.ToString(Session["csrf"]))
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            { 
                productRepo.MachineProinsert(MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num, Convert.ToInt32(Session["admin_id"]), product,layer,road2,road,proqty, MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])), MachineRepo.getModel(Convert.ToString(Session["admin_id"])));
                return Redirect("/Backend/MachineProlist?layer=1");

            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult MachineProedit(int product, string layer, string road2, string road, int proqty, string csrf,int vmprid)
        {
            if (csrf != Convert.ToString(Session["csrf"]))
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                productRepo.MachineProedit(MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num, Convert.ToInt32(Session["admin_id"]), product, layer, road2, road, proqty, MachineLasRepo.getMachineType(Convert.ToInt32(Session["admin_id"])), MachineRepo.getModel(Convert.ToString(Session["admin_id"])),vmprid);
                return Redirect("/Backend/MachineProlist?layer=1");

            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult Fillup(string number,int day,string csrf)
        {
            if (csrf != Convert.ToString(Session["csrf"]))
            {
                return Redirect("/Backend/Adminlogin");
            }
            else
            {
                DateTime date1 = new DateTime();
                DateTime date2 = new DateTime();
                date1 = DateTime.Now;
                date2 = date1.AddDays(day);

                string updata = date1.ToString("yyyy/MM/dd");
                string uptime = date1.ToString("HH:mm:ss");
                string expiry = date2.ToString("yyyy/MM/dd");
                productRepo.Fillup(Convert.ToInt32(Session["admin_id"]),updata,uptime,expiry,Convert.ToInt32(number));
                return Redirect("/Backend/MachineProlist?layer=1");

            }
        }
    }
}