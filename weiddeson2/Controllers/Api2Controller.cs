﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using weiddeson2.Models;
using weiddeson2.App_Code;


namespace weiddeson2.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "GET,PUT,OPTIONS")]
    public class Api2Controller : Controller
    {
        MachineRepository MachineRepo = new MachineRepository();
        OrdersRepository OrdersRepo = new OrdersRepository();
        ProductRepository productRepo = new ProductRepository();
        vmUserRepository VmUserRepo = new vmUserRepository();
        VmsortRepository vmsortRepo = new VmsortRepository();
        CloudcustormersRepository cloudcustormersRepo = new CloudcustormersRepository();
        CloudstoreRepository cloudstoreRepo = new CloudstoreRepository();
        CloudVmlistRepository cloudVmlistRepo = new CloudVmlistRepository();
        LogintempRepository logintempRepo = new LogintempRepository();

        //系統登入2.0
        [HttpPost]
        public JsonResult Auth(string adminname, string adminpass, string csrf, string taxid)
        {
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

            Error error = new Error();
            Password password = new Password();
            string csrfCheck = Convert.ToString(Session["csrf"]);
            /*if (csrf != csrfCheck)
            {
                error.token = "401";
                error.message = "與伺服器的聯絡中發生不明錯誤。";
            }
            else
            {*/
            Auth auth = password.loginCheck2(adminname, taxid, adminpass);
            if (auth.pass)
            {
                //Vmuser vmuserinfo = VmUserRepo.Vmuserinfo2(taxid);
                // Session["admin"] = auth.name;
                //Session["cm_id"] = auth.cm_id;
                //Session["admin_name"] = vmuserinfo.name;
                //Session["admin_id"] = vmuserinfo.vm_uid;
                error.token = "301";
                error.message = "";
                /*error.vm_uid = vmuserinfo.vm_uid;
                error.vm_num = vmuserinfo.vm_num;*/
                Logintemp logintemp = new Logintemp();
                logintemp = logintempRepo.login(auth.cm_id, Convert.ToString(auth.name));
                error.cm_id = logintemp.cm_id;
                error.name = logintemp.name;
            }
            else if (!auth.pass)
            {
                error.token = "401";
                error.message = "使用者帳號不存在。請重新登入。";
            }

            // }
            JsonResult json = new JsonResult();
            json.Data = error;
            return Json(error, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public bool logout()
        {
            return logintempRepo.logout();
        }

        //首頁資訊
        [HttpGet]
        // GET: Api
        //首頁資訊
        public JsonResult Index(string vm_num, int uid = 6)
        {
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Request-Headers", " X-PINGOTHER, Content-Type");
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Request-Methods", "POST,PUT,GET,OPTIONS");
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", " X-PINGOTHER, Content-Type");
            //ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST,PUT,GET,OPTIONS");
            JsonResult json = new JsonResult();

            index indexS = new index();

            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();
            if (logintemp.cm_id != null)
            {
                indexS.temprate = MachineRepo.GetTemprature(uid).temperature;
                indexS.cash = OrdersRepo.MonSalse(uid, "1");
                indexS.line = OrdersRepo.MonSalse(uid, "5");
            }
            indexS.name = logintemp.name;
            indexS.cm_id = logintemp.cm_id;
            return Json(indexS, JsonRequestBehavior.AllowGet);
        }

        //產品列表
        [HttpGet]
        public JsonResult productlist(string sort = "")
        {
            JsonResult json = new JsonResult();

            Products products1 = new Products();

            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();
            if (logintemp.cm_id != null)
            {
                products1.data = productRepo.GetMainproductsC(sort);
            }
            products1.name = logintemp.name;
            products1.cm_id = logintemp.cm_id;

            return Json(products1, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult productInfo(int id)
        {
            JsonResult json = new JsonResult();

            Mainproduct product = new Mainproduct();

            product = productRepo.GetProductinfoC(id);

            return Json(product, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Insertproduct(string bigsortid, string csrf, string pdname, int unit_price, int cost_price, int amount, string shelf_life_day, HttpPostedFileBase file, string content1 = "")
        {
            DateTime date1 = new DateTime();
            date1 = DateTime.Now;
            Random randoms = new Random();
            string num = Convert.ToString(randoms.Next(0, 9));
            string dingdan = "";
            dingdan = date1.ToString("yyyyMMddHHmmssff") + num;
            string filename = "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1];
            string csrfCheck = Convert.ToString(Session["csrf"]);

            /*if (csrf != csrfCheck)
            {
                JsonResult json = new JsonResult();
                json.Data = new status() {
                    Status = "failed",
                    message = "憑證過期或不正確。"
                };

                return json;

            }
            else
            {*/
            //檢查是否有選擇檔案

            if (file != null)
            {
                string savedName = Path.Combine(Server.MapPath("~/images/pic/Product"), "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1]);
                file.SaveAs(savedName);

                productRepo.insertProductC(bigsortid, pdname, unit_price, cost_price, content1, amount, shelf_life_day, filename, MachineRepo.getuuid(Convert.ToInt32(Session["admin_id"])).vm_num, Convert.ToInt32(Session["admin_id"]));
            }


            JsonResult json = new JsonResult();
            json.Data = new status()
            {
                Status = "success",
                message = "新增成功"
            };

            return json;
        }

        //        }

        [HttpPost]
        public JsonResult editProduct(int id, string bigsortid, string csrf, string pdname, int unit_price, int cost_price, int amount, string shelf_life_day, string content1, HttpPostedFileBase file = null)
        {
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string filename = "";

            string csrfCheck = Convert.ToString(Session["csrf"]);

            /*          if (csrf != csrfCheck)
                      {
                          JsonResult json = new JsonResult();
                          json.Data = new status()
                          {
                              Status = "failed",
                              message = "憑證過期或不正確。"
                          };

                          return json;
                      }
                      else
                      {*/
            //檢查是否有選擇檔案
            if (file != null)
            {
                string num = Convert.ToString(randoms.Next(0, 9));
                string dingdan = date1.ToString("yyyyMMddHHmmssff") + num;
                filename = "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1];
                string savedName = Path.Combine(Server.MapPath("~/images/pic/Product"), "pro" + dingdan + "." + Convert.ToString(file.ContentType).Split('/')[1]);
                file.SaveAs(savedName);
            }
            JsonResult json = new JsonResult();
            if (productRepo.editProductC(id, bigsortid, pdname, unit_price, cost_price, content1, amount, shelf_life_day, filename))
            {
                json.Data = new status()
                {
                    Status = "success",
                    message = "修改成功"
                };
            }
            else
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "修改失敗"
                };
            }
            return json;
        }
        //}

        [HttpPost]
        public JsonResult deleteProduct(int id)
        {
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string filename = "";
            JsonResult json = new JsonResult();
            string csrfCheck = Convert.ToString(Session["csrf"]);

            if (productRepo.deleteProduct(id))
            {
                json.Data = new status()
                {
                    Status = "success",
                    message = "新增成功"
                };

                return json;
            }
            else
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "新增失敗"
                };

                return json;
            }
        }

        /*public JsonResult accountInsert()
        {
            DateTime date1 = DateTime.Now;
            Random randoms = new Random();
            string filename = "";

            Vmuser vmuser = new Vmuser();
             = productRepo.editProduct(id, bigsortid, pdname, unit_price, cost_price, content1, amount, shelf_life_day, filename);

            JsonResult json = new JsonResult();
            json.Data = new status()
            {
                Status = "success",
                message = "修改成功"
            };

            return json;
        }*/

        [HttpGet]
        public JsonResult vmSortlist(int uid = 0)
        {
            JsonResult json = new JsonResult();
            List<Vmsort> vmsorts = new List<Vmsort>();
            vmsorts = vmsortRepo.GetVmsortsC(uid);

            vnSortlists sortlist = new vnSortlists();

            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();
            if (logintemp.cm_id != null)
            {
                sortlist.Data = vmsorts;
            }
            sortlist.name = logintemp.name;
            sortlist.cm_id = logintemp.cm_id;

            //json.Data = vmsorts;

            return Json(sortlist, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult vmSortInsert(string name, string csrf, int uid = 0)
        {
            JsonResult json = new JsonResult();
            string csrfCheck = Convert.ToString(Session["csrf"]);

            /*if (csrf != csrfCheck)
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "憑證過期或不正確。"
                };

                return json;
            }
            else
            {*/
            if (vmsortRepo.Vmsortinsert(uid, name))
            {
                json.Data = new status()
                {
                    Status = "success",
                    message = "新增成功"
                };

                return json;
            }
            else
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "新增失敗"
                };

                return json;
            }
            //}

        }

        [HttpGet]
        public JsonResult VmsortInfo(int id)
        {
            JsonResult json = new JsonResult();

            Vmsort sort = new Vmsort();

            sort = vmsortRepo.GetVmsort(id);

            return Json(sort, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult vmSortedit(string name, int big_sort_id, string csrf)
        {
            JsonResult json = new JsonResult();
            string csrfCheck = Convert.ToString(Session["csrf"]);

            /*if (csrf != csrfCheck)
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "憑證過期或不正確。"
                };

                return json;
            }
            else
            {*/
            if (vmsortRepo.Vmsortedit(name, big_sort_id))
            {
                json.Data = new status()
                {
                    Status = "success",
                    message = "修改成功"
                };

                return json;
            }
            else
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "修改失敗"
                };

                return json;
            }
            //}

        }

        [HttpPost]
        public JsonResult deleteVmsort(int id)
        {
            JsonResult json = new JsonResult();

            if (vmsortRepo.deleteSort(id))
            {
                json.Data = new status()
                {
                    Status = "success",
                    message = "刪除成功"
                };

                return json;
            }
            else
            {
                json.Data = new status()
                {
                    Status = "failed",
                    message = "刪除失敗"
                };

                return json;
            }
        }

        [HttpGet]
        public JsonResult salses(string start, int uid, string end = "", string type = "")
        {
            //JsonResult json = new JsonResult();

            salseData salse = new salseData();

            List<salseOrder> salseOrders = new List<salseOrder>();

            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();

            if (logintemp.cm_id != null)
            {
                salseOrders = OrdersRepo.getSalselist(start, end, uid, type);
                int num = 0;

                foreach (salseOrder salse1 in salseOrders)
                {
                    num += salse1.unit_price;
                }

                salse.salses = salseOrders;
                salse.count = num;
            }
            salse.name = logintemp.name;
            salse.cm_id = logintemp.cm_id;


            //json.Data = salse;

            return Json(salse, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getCustormers()
        {
            Ccustormers ccustormers = new Ccustormers();
            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();

            if (logintemp.cm_id != null)
            {
                ccustormers.data = cloudcustormersRepo.cloudcustormers();
            }

            ccustormers.name = logintemp.name;
            ccustormers.cm_id = logintemp.cm_id;

            return Json(ccustormers, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getStores(string id)
        {
            Stores stores = new Stores();
            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();

            if (logintemp.cm_id != null)
            {
                stores.data = cloudstoreRepo.cloudStores(id);
            }

            stores.name = logintemp.name;
            stores.cm_id = logintemp.cm_id;

            return Json(stores, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getVmlist(string id)
        {
            vMlist cvMlist = new vMlist();
            Logintemp logintemp = new Logintemp();
            logintemp = logintempRepo.getLogin();

            if (logintemp.cm_id != null)
            {
                cvMlist.data = cloudVmlistRepo.cloudVms(id);
            }

            cvMlist.name = logintemp.name;
            cvMlist.cm_id = logintemp.cm_id;


            return Json(cvMlist, JsonRequestBehavior.AllowGet);
        }

        public bool Createadmin2(string taxid, string email)
        {
            return VmUserRepo.Createadmin2(taxid, email);
        }

        public ActionResult iPass(string id)
        {
            ViewData["token"] = id;
            return View();
        }

        [HttpGet]
        public JsonResult doPass(string password, string token, string csrf)
        {
            if (VmUserRepo.doPass(password, token))
            {
                return Json(
                    new status()
                    {
                        Status = "success",
                        message = "驗證成功"
                    }
            , JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(
                new status()
                {
                    Status = "failed",
                    message = "驗證失敗"
                }
                , JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Custormersiseert(string taxid, string name, string email, string phone, string country, string city, int zip, string address, string type, string note = "null", string fax = "null", string mobile = "null")
        {
            JsonResult json = new JsonResult();
            //Ccustormers ccustormers = new Ccustormers();
            if (cloudcustormersRepo.insert(taxid, name, email, type, phone, fax, mobile, country, city, zip, address, note))
            {
                if (this.Createadmin2(taxid, email))
                {

                    if (Createstore(taxid, country, city, zip, address))
                    {
                        return Json(
                            new status()
                            {
                                Status = "success",
                                message = "新增成功"
                            }
                    , JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(
                        new status()
                        {
                            Status = "failed",
                            message = "新增失敗"
                        }
                        , JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(
                    new status()
                    {
                        Status = "failed",
                        message = "新增失敗"
                    }
                    , JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(
                    new status()
                    {
                        Status = "failed",
                        message = "新增失敗"
                    }
               , JsonRequestBehavior.AllowGet);
            }

            //return Json(ccustormers, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Customersinfo(string taxid)
        {
            Cloudcustormers cloudcustormers = cloudcustormersRepo.info(taxid);
            return Json(cloudcustormers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Custormersedit(string taxid, string name, string email, string phone, string country, string city, int zip, string address, string type, string note = "null", string fax = "null", string mobile = "null")
        {
            JsonResult json = new JsonResult();
            //Ccustormers ccustormers = new Ccustormers();
            if (cloudcustormersRepo.edit(taxid, name, email, type, phone, fax, mobile, country, city, zip, address, note))
            {
                return Json(
                   new status()
                   {
                       Status = "success",
                       message = "修改成功"
                   }
              , JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(
                    new status()
                    {
                        Status = "failed",
                        message = "新增失敗"
                    }
               , JsonRequestBehavior.AllowGet);
            }

            //return Json(ccustormers, JsonRequestBehavior.AllowGet);
        }

        public bool Createstore(string taxid, string country, string city, int zip, string address, string name = "預設商店", string location = "預設商店")
        {
            return cloudstoreRepo.insert(taxid, country, city, zip, address, name, location);
        }



        public class vnSortlists
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<Vmsort> Data { get; set; }
        }
            
        public class Products
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<Mainproduct> data { get; set; }
        }
        
        public class Ccustormers
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<Cloudcustormers> data { get; set; }
        }

        public class Stores
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<CloudStore> data { get; set; }
        }

        public class vMlist
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<CloudVmlist> data { get; set; }
        }

        public class index
        {
            public string name { get;set;} 
            public string cm_id { get; set; }
            public string temprate { get; set; }

            public List<salseProtal> cash { get; set; }
            public List<salseProtal> line { get; set; }
        }

        public class Trans
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public string pdname { get; set; }
            public int amount { get; set; }
        }

        public class status
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public string Status { get; set; }

            public string message { get; set; }
        }

        public class salseData
        {
            public string name { get; set; }
            public string cm_id { get; set; }

            public List<salseOrder> salses { get; set; }
            public int count { get; set; }
        }
    }
}