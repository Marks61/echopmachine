﻿var ret = true;
var taxData = {};
var phoneNumberRule = /^0([2-9][0-9]{7,8})$/;
var faxNumberRule = /^0([2-8][0-9]{7,8})$/;
var emailRuleText = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

$('.scrollbar-macosx').scrollbar();

function poster() {

    var taxValue = document.getElementById('tax').value;

    taxData = {
        'taxId': taxValue
    };

    $("input[name=open]:checked").each(function () {
        $('#open').val($(this).val());
    });

    if ($('#tax').val() == "") {
        $('#taxAlert2').removeClass('member__flex-item-error_show');
        $('#taxAlert').addClass('member__flex-item-error_show');
        $('#tax').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        taxCheck($('#tax').val());
    }

    if ($('#personId').val() == "") {
        $('#personIdAlert2').removeClass('member__flex-item-error_show');
        $('#personIdAlert').addClass('member__flex-item-error_show');
        $('#personId').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        personCheck($('#personId').val());
    }

    if ($('#groupName').val() == "") {
        $('#groupNameAlert').addClass('member__flex-item-error_show');
        $('#groupName').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#groupName').removeClass('member__flex-item-input_error');
        $('#groupNameAlert').removeClass('member__flex-item-error_show');
    }

    if ($('#contact').val() == "") {
        $('#contactAlert').addClass('member__flex-item-error_show');
        $('#contact').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#contact').removeClass('member__flex-item-input_error');
        $('#contactAlert').removeClass('member__flex-item-error_show');
    }

    if ($('#telNumber').val() == "") {
        $('#telNumberAlert').addClass('member__flex-item-error_show');
        $('#telNumber').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        telPhoneRule($('#telNumber').val());
    }

    if (!$('#fax').val() == "") {
        faxPhoneRule($('#fax').val());
    }

    if ($('#email').val() == "") {
        $('#emailAlert').addClass('member__flex-item-error_show');
        $('#email').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        emailRule($('#email').val());
    }

    if ($('#address').val() == "") {
        $('#addressAlert').addClass('member__flex-item-error_show');
        $('#address').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#address').removeClass('member__flex-item-input_error');
        $('#addressAlert').removeClass('member__flex-item-error_show');
    }

    if ($('#capital').val() == "" || !isNaN(parseInt($('#capital').val()))) {
        $('#capitalAlert').removeClass('member__flex-item-error_show');
        $('#capital').removeClass('member__flex-item-input_error');
        $('#capital').val(parseInt($('#capital').val()));
    }
    else if (isNaN(parseInt($('#capital').val()))) {
        $('#capitalAlert').addClass('member__flex-item-error_show');
        $('#capital').addClass('member__flex-item-input_error');
        ret = false;
    }

    if ($('#manager').val() == "") {
        $('#managerAlert').addClass('member__flex-item-error_show');
        $('#manager').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#manager').removeClass('member__flex-item-input_error');
        $('#managerAlert').removeClass('member__flex-item-error_show');
    }

    if ($('#business').val() == "") {
        $('#businessAlert').addClass('member__flex-item-error_show');
        $('#business').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#business').removeClass('member__flex-item-input_error');
        $('#businessAlert').removeClass('member__flex-item-error_show');
    }

    if (!($('#open').val() == "0" || $('#open').val() == "1")) {
        ret = false;
    }

    if (ret) {
        $('#form').submit();
    } else {
        return false;
    }

    /*if ($('#tax').val() == "" || $('#groupName').val() == "" || $('#contact').val() == "" || $('#telNumber').val() == "" || $('#fax').val() == "" || $('#email').val() == "" || $('#address').val() == "" || $('#capital').val() == "" || $('#manager').val() == "" || $('#business').val() == "") {

        return false;
    } else if (!($('#open').val() == "0" || $('#open').val() == "1")) {
        return false;
    } else {
        taxCheck();
    }*/
}

function personCheck(personStr) {
    var check = false;

    var person = personStr.split("");

    var eng = Array();

    eng = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
        "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W",
        "Z", "I", "O"];

    var intEng = 0;

    //留意規則1:身分證字號10碼乘上對應倍數。
    //留意規則2:英文字母轉換成後的二位數，累進值先累加上十位數，在累加個位數的9倍乘積。
    //留意規則3:如果累進值除與10可以整除即為正確。
    //其餘計算規則參考驗證規則:https://web.fg.tp.edu.tw/~anny/idtest.htm。

    for (var i = 0; i < eng.length; i++) {
        if (eng[i] == person[0].toUpperCase()) {
            intEng = i + 10;
            console.log(intEng);
            break;
        }
    }

    var n1 = parseInt(intEng / 10);
    var n2 = intEng % 10;

    var sum = 0;
    for (var j = 0; j < 8; j++) {
        sum += Number(person[j + 1]) * (8 - j);
    }

    var num2 = 0;
    num2 = n1 + (n2 * 9) + sum + Number(person[9]);

    if (num2 % 10 != 0) {
        $('#personIdAlert').removeClass('member__flex-item-error_show');
        $('#personIdAlert2').addClass('member__flex-item-error_show');
        $('#personId').addClass('member__flex-item-input_error');
        ret = false;
    } else {
        $('#personId').removeClass('member__flex-item-input_error');
        $('#personIdAlert2').removeClass('member__flex-item-error_show');
        $('#personIdAlert').removeClass('member__flex-item-error_show');
    }
}

function taxCheck(taxStr) {

    //8位數字分別乘上1、2、1、2、1、2、4、1
    var num = 0;
    var tax = taxStr.split("");
    var doubles = Array();
    doubles = [1, 2, 1, 2, 1, 2, 4, 1];

    //規則1:統一編號8碼乘上對應倍數。
    //規則2:乘積大於等於10，累進值分別累加十位數以及個位數。
    //規則3:統一編號第7碼為7者，必須去分別判斷累進值+0或+1的結果，因此迴圈在第七次累進跳過。
    //規則4:如果累進值除與10可以整除即為正確。

    for (var sorts = 0; sorts <= 7; sorts++) {
        var benchMark = Number(tax[sorts]);
        var product = benchMark * Number(doubles[sorts]);

        if (sorts == 6 && benchMark == 7) {
            continue;
        } else if (product >= 10) {
            product = String(product).split("");
            var cut1 = Number(product[0]);
            var cut2 = Number(product[1]);
            num += cut1;
            num += cut2;
        } else {
            num += product;
        }
    }

    if (Number(tax[6]) == 7) {
        if ((num + 1) % 10 == 0) {
            $('#tax').removeClass('member__flex-item-input_error');
            $('#taxAlert2').removeClass('member__flex-item-error_show');
            $('#taxAlert').removeClass('member__flex-item-error_show');
        } else if (num % 10 == 0) {
            $('#tax').removeClass('member__flex-item-input_error');
            $('#taxAlert2').removeClass('member__flex-item-error_show');
            $('#taxAlert').removeClass('member__flex-item-error_show');
        } else {
            $('#taxAlert').removeClass('member__flex-item-error_show');
            $('#taxAlert2').addClass('member__flex-item-error_show');
            $('#tax').addClass('member__flex-item-input_error');
            ret = false;
        }
    } else {
        if (num % 10 == 0) {
            $('#tax').removeClass('member__flex-item-input_error');
            $('#taxAlert2').removeClass('member__flex-item-error_show');
            $('#taxAlert').removeClass('member__flex-item-error_show');
        } else {
            $('#taxAlert').removeClass('member__flex-item-error_show');
            $('#taxAlert2').addClass('member__flex-item-error_show');
            $('#tax').addClass('member__flex-item-input_error');
            ret = false;
        }
    }
}

function telPhoneRule(telPhoneStr) {
    //連絡電話正則
    if (telPhoneStr.search(phoneNumberRule) != -1) {
        $('#telNumber').removeClass('member__flex-item-input_error');
        $('#telNumberAlert2').removeClass('member__flex-item-error_show');
        $('#telNumberAlert').removeClass('member__flex-item-error_show');
    } else {
        $('#telNumberAlert').removeClass('member__flex-item-error_show');
        $('#telNumberAlert2').addClass('member__flex-item-error_show');
        $('#telNumber').addClass('member__flex-item-input_error');
        ret = false;
    }
}

function faxPhoneRule(faxPhoneStr) {
    //傳真號碼正則
    if (faxPhoneStr.search(faxNumberRule) != -1) {
        $('#faxAlert').removeClass('member__flex-item-error_show');
        $('#fax').removeClass('member__flex-item-input_error');
    } else {
        $('#faxAlert').addClass('member__flex-item-error_show');
        $('#fax').addClass('member__flex-item-input_error');
        ret = false;
    }
}

function emailRule(emailStr) {
    //電子信箱正則
    if (emailStr.search(emailRuleText) != -1) {
        $('#email').removeClass('member__flex-item-input_error');
        $('#emailAlert2').removeClass('member__flex-item-error_show');
        $('#emailAlert').removeClass('member__flex-item-error_show');
    } else {
        $('#emailAlert').removeClass('member__flex-item-error_show');
        $('#emailAlert2').addClass('member__flex-item-error_show');
        $('#email').addClass('member__flex-item-input_error');
        ret = false;
    }
}

