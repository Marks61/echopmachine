$(document).ready(function () {
    $('.login__form-item-error').hide();
});

function poster() {
    if ($('#userName').val() == "" || $('#userPassword').val() == "" || $('#taxId').val() == "" ) {
        if ($('#userName').val() == "") {
            $('#usernameAlert').show();
            $('#userName').addClass('login__form-item-input_error');
        } else {
            $('#userName').removeClass('login__form-item-input_error');
            $('#usernameAlert').hide();
        }

        if ($('#userPassword').val() == "") {
            $('#userPassword').addClass('login__form-item-input_error');
            $('#passwordAlert').show();
        } else {
            $('#userPassword').removeClass('login__form-item-input_error');
            $('#passwordAlert').hide();
        }

        if ($('#taxId').val() == "") {
            $('#taxId').addClass('login__form-item-input_error');
            $('#taxIdAlert').show();
        } else {
            $('#userPassword').removeClass('login__form-item-input_error');
            $('#passwordAlert').hide();
        }

        return false;
    } else {
        $('#userName').removeClass('login__form-item-input_error');
        $('#userPassword').removeClass('login__form-item-input_error');
        $('.login__form-item-error').hide();
        $('#form').submit();
    }
}