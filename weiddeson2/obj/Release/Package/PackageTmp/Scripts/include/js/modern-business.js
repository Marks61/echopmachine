 
var goods = 0;
var goodsPrice = 0;
var cp1 = document.getElementById('crushpay_timer');
var cp2 = document.getElementById('cardpay_timer');
var cp3 = document.getElementById('finish_timer');
var buycash = document.getElementById('buyCash');
var noncash = document.getElementById('nonCash');
var hascash = document.getElementById('hasCash');
var givecash = document.getElementById('giveCash');
var proid = document.getElementById('proid');
var cashOutcach = "";
var cp1time = 60;  // 現金時間
var cp2time = 30;  // 讀卡時間
var cp3time = 30;  // 完成時間
var count = 0;
var timer;
var imgTimeOut;
var payShowTime = 0;
var timeIndex = 0;
var finishTime;   //購買完成的時間
var payTime;      //付款時候的時間
var adType = true;
var listenCash = true;
var imgUrl = new Array();
imgUrl = ['/images/pic/pic-0022.jpg', '/images/pic/pic-004.png','/images/pic/pic-0023.jpg'];


//time
let hour = '';
let minutes = '';
let second = '';
let year = '';
let month = '';
let day = '';
let weekend = '';

function getTime() {
    const date = new Date();
    hour = date.getHours();
    minutes = date.getMinutes();
    second = date.getSeconds();
    year = date.getFullYear();
    month = date.getMonth() + 1;
    day = date.getDate();
    weekend = date.getDay();
}

function startTime() {
    getTime();
    hour = checkTime(hour);
    minutes = checkTime(minutes);
    second = checkTime(second);
    document.getElementById('time__date').innerHTML = year + "/" + month + "/" + day;
    document.getElementById('time__clock').innerHTML = hour + ":" + minutes + ":" + second;
    let timeoutId = setTimeout(startTime, 1000);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
} 


function showformbuy(num, price) {

  goods = num;
    
  goodsPrice = price;
  $('#buypic').attr("src", $("#productinfo" + num).data("img"));
  $('#buyname').text($("#productinfo" + num).data("pdname"));
    $('#discription').text($("#productinfo" + num).data("content"));
    $('#buyprice').text($("#productinfo" + num).data("unit"));
  //document.getElementById("formpro").style.display = "block";
  //document.getElementById("light").style.display = "block";
  $('#proid').val(num);
    //getRealPrice(num);
    getRealPrice2(num);
    payShowTime++;
}

function showformpay(type) {
  document.getElementById("cash").style.display = "none";
  document.getElementById("linePayImg").style.display = "none";
  document.getElementById(type).style.display = "block";

  $('.popup').removeClass('popup_show');
  $('#formpay').addClass('popup_show');
  $('#cardView').hide();
  $('#cashView').hide();
  //$('#linepay').hide();
  payShowTime++;

  switch (type) {
    case 'cash':
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
          if (this.readyState == 4 && this.status == 200) {
              test();
              s = setInterval(test, 700);
              listenCash = true;
        }
          };
      xmlhttp.open("post", config.ApiUrl+"/Trans/cashstampStart", true);
      xmlhttp.send();
      $('#cashView').show();
    break;
      case 'linePayImg':
          listenCash = false;
          $('#buyCashLine').text(goodsPrice);
      $('#cashView').hide();
          $('#cardView').show();
          $('#linepay').focus();
    break;
  
    default:
      break;
  }
}

function test() {
  checkData($('#proid').val(), $('#buyCash').val());
  getCashin();
 // xmlhttpRequest();
}

function checkData(num, price) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      if (!(data.price == $('#buyCash').val())) {
        $('#buyCash').val(data.price);
      } else if (!(data.product == $('#proid').val())) {
        $('#proid').val(data.product);
      }
    }
  };
  xmlhttp.open("post", config.ApiUrl +"/Trans/checkData", true);
  xmlhttp.send();
}

/**
 * 現金的付款時間
 * cp1time 遞減
 * 判斷cp1time是否為0
 * 如果不是就過一秒在執行一次
 * 如果是就重置時間、clearTimeout(payTime)、timeGo();
 */
function countdown1() {
  $('#crushpay_timer').html(cp1time - 1);
  cp1time--;
  if (cp1time == 0) {
    cp1time = 60;
    payShowTime = 0;
    timeGo();
    $('#crushpay_timer').html(cp1time); 
      $('.popup').removeClass('popup_show');
      cashOutcach = $('#hasCash').text();
      Cashout();
      clearTimeout(payTime);
      clearInterval(s);
  }
  else {
    payTime = setTimeout('countdown1()', 1000);
  }
}

/**
 * 讀卡的付款時間
 * cp2time 遞減
 * 判斷cp2time是否為0
 * 如果不是就過一秒在執行一次
 * 如果是就重置時間、clearTimeout(payTime)、timeGo();
 */
function countdown2() {
  $('#cardpay_timer').html(cp2time - 1);
  cp2time--;
  if (cp2time == 0) {
    cp2time = 30;
    payShowTime = 0;
    timeGo();
    $('#cardpay_timer').html(cp2time);
    $('.popup').removeClass('popup_show');
    clearTimeout(payTime);
  }
  else {
    payTime = setTimeout('countdown2()', 1000);
  }
}

function saleFunction(type, type2) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      location.href = '/Index/Finish';
    }
  };
    xmlhttp.open("Post", config.ApiUrl +"/Trans/Salse", true);
  xmlhttp.send();
}

/**
 * 完成的付款時間
 * cp3time 遞減
 * 判斷cp3time是否為0
 * 如果不是就過一秒在執行一次
 * 如果是就重置時間、clearTimeout(finishTime)、timeGo();
 */
function countdown3() {
    $('#finish_timer').html(cp3time - 1);
    cp3time--;
    if (cp3time == 0) {
      cp3time = 30;
      payShowTime = 0;
      timeGo();
      $('#finish_timer').html(cp3time);
      $('.popup').removeClass('popup_show');
      clearTimeout(finishTime);
      $('#refund').addClass('popup_show');
      document.getElementById('refundTxt').innerHTML = "請稍後。";
      setTimeout(function () { resets();location.href = "/"; }, 3000);
    }
    else {
      finishTime = setTimeout('countdown3()', 1000);
    }
}

/**
 * 取消付款或退幣
 * 執行過一秒後重置時間、clearTimeout(payTime);
 * 重置金錢欄位
 * timeGo();
 */
function stopcount() {
  setTimeout(() => {
      clearTimeout(payTime);
      if (listenCash) {
          clearInterval(s);
      }
    cp1time = 60;
    cp2time = 30;
    cp3time = 30;
    $('#crushpay_timer').html(cp1time);
    $('#cardpay_timer').html(cp2time);
  }, 1000);
    cashOutcach = $('#hasCash').text();

    $('#buyCash').text(0);
    $('#nobCash').text(0);
    $('#giveCash').text(0);
    $('#hasCash').text(0);
  payShowTime = 0
  $('.popup').removeClass('popup_show');
  timeGo()
}

/**
 * 現金付款
 * 執行過一秒後重置時間、clearTimeout(payTime)、countdown3();
 * 重置金錢欄位
 * timeGo();
 */
function salse() {
  setTimeout(() => {
    clearTimeout(payTime);
    cp1time = 60;
    $('#crushpay_timer').html(cp1time);
    countdown3();
  }, 1000);
    $('#buyCash').text(0);
    $('#nobCash').text(0);
    $('#giveCash').text(0);
    $('#hasCash').text(0);
  payShowTime = 0
  $('.popup').removeClass('popup_show');
  $('#formpay').submit();
    $('#finish').addClass('popup_show');
    $('.popup__finish-button').hide();
    setTimeout(function () { $('.popup__finish-button').show(); }, 15000);
}

/**
 * 關閉完成畫面
 * 執行過一秒後重置時間、clearTimeout(finishTime);
 */
function closeBtn() {
  timeGo();
  setTimeout(() => {
    clearTimeout(finishTime);
    cp1time = 60;
    cp2time = 30;
    cp3time = 30;
    $('#crushpay_timer').html(cp1time);
    $('#cardpay_timer').html(cp2time);
    $('#finish_timer').html(cp3time);
  }, 1000);

    $('#finish').removeClass('popup_show');
    $('#refund').addClass('popup_show');
    document.getElementById('refundTxt').innerHTML = "請稍後。";
    
    setTimeout(function () { resets();location.href = "/"; }, 3000);
}

function overTime() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //$('#buyCash').val(this.responseText);
        }
    };
    xmlhttp.open("post", config.ApiUrl + "/Index/sendMail", true);
    xmlhttp.send();
}

function listQty() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //$('#buyCash').val(this.responseText);
        }
    };
    xmlhttp.open("post", config.ApiUrl + "/Index/sendMail2", true);
    xmlhttp.send();
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else {
       // x.innerHTML = "瀏覽器不支持，或是系統拒絕權限。";
    }
}

function showPosition(position) {
    $('#x').val(position.coords.latitude);
    $('#y').val(position.coords.longitude);

    returnGeojson();
}

function returnGeojson() {
    var data = new FormData();
    data.append("vmcode", $('#vmcode').val());
    data.append("x", $('#x').val());
    data.append("y", $('#y').val());
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //$('#buyCash').val(this.responseText);
        }
    };
    xmlhttp.open("post", config.Cloud + "/Index/updateGeojson", true);
    xmlhttp.send(data);
}

function salseReportReturn() {
    var data = new FormData();
    data.append("vmcode", $('#vmcode').val());
    data.append("date1", $('#date1').val());
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //$('#buyCash').val(this.responseText);
        }
    };
    xmlhttp.open("post", config.ApiUrl + "/Index/salseReportReturn", true);
    xmlhttp.send(data);
}

function refreshtemp() {
    var data = new FormData();
    data.append("vmuid", $('#vmcode').val());
    var xhr = new XMLHttpRequest();
    xhr.open('post', '/Index/getTemprature', true);
    xhr.onload = function () {
        respoce = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            $('.nav-time__temp #temp').text(respoce.temperature);
            //todaysalse('@date');
        }
    };
    xhr.send(data);
}

$('body').ready(function () {

    startTime();
    getLocation();
    //overTime();
    //listQty();
    salseReportReturn();
    refreshtemp();
    setInterval(refreshtemp,30000);
    setInterval(salseReportReturn, 60000);
    setInterval(getLocation,300000);
    //setInterval(overTime, 900000);
    //setInterval(listQty, 900000);


  // chrome 直接加上下面这个就行了，但是ff無法
  $('body').css('zoom', 'reset');
  
	$(document).keydown(function (event) {
    //event.metaKey mac的command键
    if ((event.ctrlKey === true || event.metaKey === true)&& (event.which === 61 || event.which === 107 || event.which === 173 || event.which === 109 || event.which === 187  || event.which === 189)){
      event.preventDefault();
    }
  });
  
	$(window).bind('mousewheel DOMMouseScroll', function(event) {
    if (event.ctrlKey === true || event.metaKey) {
      event.preventDefault();
    }
  })

  // 呼叫scrollbar
  $('.body-list').overlayScrollbars({
    callbacks:{ //套件提供事件
      onScrollStop: function(e) { //提供的參數
        scrollTop = e.target.scrollTop;
    
        if (scrollTop > 0) {
          $('.top-button').addClass('top-button_show');
        } else {
          $('.top-button').removeClass('top-button_show');
        }

        $('.top-button').click(function() {
          $('.top-button').removeClass('top-button_show');
          e.target.scrollTop = 0;
        })
      }
    },
    
  });  

  $('.body-list__item').click(function () {
    payShowTime ++;
    $('.popup').removeClass('popup_show');
    $('#commodity').addClass('popup_show');
  })

  $('#commodityCancel').click(function () {
    payShowTime = 0;
    $('.popup').removeClass('popup_show');
    timeGo()
  })

  $('#close').click(function() {
    payShowTime = 0;
    $('.popup').removeClass('popup_show');
    $('#videoPlayer')[0].currentTime = 0;
    document.getElementById('videoPlayer').pause();
    timeGo()
  })

  $('#closeImg').click(function() {
    payShowTime = 0;
    clearInterval(imgTimeOut)
    $('.popup').removeClass('popup_show');
    timeGo()
  })

  $('.body').on('click',function() {
    setTime();
  })

  // 初始化計時待機
  initTime = setTimeout(()=>{
    $('#ad').addClass('popup_show');
    adType = false;
  }, 30000)
})

function scrollPic() {
  var number = Math.floor(Math.random() * imgUrl.length);
  document.getElementById("adImg").src=imgUrl[number];

  imgTimeOut = setInterval(() => {
    if (timeIndex > imgUrl.length - 1) {
      timeIndex = 0;
    }
    
    for (var i = 0; i < imgUrl.length; i++){
      if (i == timeIndex) {
        document.getElementById("adImg").src=imgUrl[i];
      }
    }
    timeIndex++;
  }, 10000);
}

function changeAd() {
  $('#img1').addClass('popup__photo_show');
  $('#img2').removeClass('popup__photo_show');
  $('#videoPlayer')[0].currentTime = 0;
  document.getElementById('videoPlayer').pause();

  setTimeout(() => {
    $('#img1').removeClass('popup__photo_show');
    $('#img2').addClass('popup__photo_show');
    document.getElementById('videoPlayer').play();
  }, 10000);
}

// 清除待機計時
// 判斷是不是在商品彈窗
function setTime() {
  clearTimeout(initTime);
  if (payShowTime === 0) {
    timeGo();
  } else {
    payShowTime = 0;
    clearTimeout(timer);
  }
}

// 待機計數器
function timeGo() {
  clearTimeout(timer);
  count ++;
  if (count == 1) {
    timer = setTimeout(()=>{
      if (adType) {
        $('#ad').addClass('popup_show');
        document.getElementById('videoPlayer').play();
        adType = false;
      } else {
        $('#adPhoto').addClass('popup_show');
        scrollPic();
        adType = true;
      }
    }, 30000)
  } else {
    count = 0;
    timeGo();
  }
}

function getRealPrice(proid) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      $('#buyCash').val(this.responseText);
    }
  };
    xmlhttp.open("get", config.ApiUrl +"/Trans/getRealProce?roadid=" + proid, true);
  xmlhttp.send();
}

function getRealPrice2(proid) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            $('#buyCash').val(this.responseText);
        }
    };
    xmlhttp.open("get", config.ApiUrl + "/Trans/getRealProce2?roadid=" + proid, true);
    xmlhttp.send();
}

function getCashin() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          var data = JSON.parse(this.responseText);
          $('#buyCash').text(data.salsePrice);
          $('#nonCash').text(data.nonCash);
          $('#hasCash').text(data.cashIn);
          $('#giveCash').text(data.cashOut);
          if (data.buySuccess == true) {
              $('#buybutton').attr('disabled', false);
              $('#buybutton').removeClass('payment-show-button__pay-click_none');
          } else {
              //$('#buybutton').attr('disabled', false);
              //$('#buybutton').removeClass('payment-show-button__pay-click_none');

             $('#buybutton').attr('disabled', true);
          }
    }
  };
    xmlhttp.open("POST", config.ApiUrl +"/Trans/getCashin", true);
  xmlhttp.send();
}

function Cashout() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        if (this.responseText) {
            if (cashOutcach == "0") {
                document.getElementById('refundTxt').innerHTML = "請稍後。";
            } else {
                document.getElementById('refundTxt').innerHTML = "退幣中。";
            }
          c = setTimeout(resets, 3000);
        $('#refund').addClass('popup_show');
      }
    }
  };

    xmlhttp.open("POST", config.ApiUrl +"/Trans/Cashout", true);
  xmlhttp.send();
}

function resets() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      clearTimeout(c);
      location.href = "/";
    }
  };

    xmlhttp.open("POST", config.ApiUrl +"/Trans/ResetCashtemp", true);
  xmlhttp.send();
}

function linePay() {
    $('#linepay').attr('disabled',true);
    var datas = new FormData();
    datas.append('linepay', $('#linepay').val());
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          var json = JSON.parse(this.responseText);
          if (json.returnCode == "0000") {
              salseOffline(json.info.payInfo[0].amount, json.info.transactionId);

          } else {
              document.getElementById('refundTxt').innerHTML = "交易失敗，返回首頁。(錯誤代號:" + json.returnCode + ")";
              $('#refund').addClass('popup_show');
              setTimeout(function () { location.href = "/"; }, 3000);
          }
      }
    };
    xmlhttp.open("post", config.ApiUrl + "/Trans/LinepayOffline", true);
  xmlhttp.send(datas);
}

function salseOffline(amount,transid) {
    var datas = new FormData();
    datas.append('amount', amount);
    datas.append('transid',transid);
    datas.append('csrf',$('#cardView #csrf').val());
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('refundTxt').innerHTML = "交易進行中，請稍後。";
            $('#refund').addClass('popup_show');
            setTimeout(function () { document.getElementById('refundTxt').innerHTML = "出貨中，請稍後。"; }, 5000);
            setTimeout(function () { location.href = "/"; }, 10000);
        }
    };
    xmlhttp.open("post", config.ApiUrl + "/Trans/LinePaySalse", true);
    xmlhttp.send(datas);
}

