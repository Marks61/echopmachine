$(function(){

	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var sideBarWidth = windowWidth*0.8;
	//設定側邊欄左邊寬度與右邊高度
	$(".sideBar-left").height(windowHeight);
	$(".sideBar-right").height(windowHeight);
	//側邊欄由左向右滑動
	$(".nav-icon").on("click",function(){
		$(".sideBar").animate({left: "0"},350);
	});
	//點選退出，側邊欄由右向左滑動
	$(".exit").on("click",function(){
		$(".sideBar").animate({left: "-100%"},350);
	});

})