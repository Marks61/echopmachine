//獲取瀏覽器頁面可見高度和寬度
var _PageHeight = document.documentElement.clientHeight,
    _PageWidth = document.documentElement.clientWidth;
//計算loading框距離頂部和左部的距離（loading框的寬度為215px，高度為250px）
var _LoadingTop = _PageHeight > 250 ? (_PageHeight - 250) / 2 : 0,
    _LoadingLeft = _PageWidth > 200 ? (_PageWidth - 200) / 2 : 0;
//在頁面未加載完畢之前顯示的loading Html自定義內容
var _LoadingHtml = '<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:' + _PageHeight + 'px;top:0;background:#f3f8ff;opacity:1;filter:alpha(opacity=80);z-index:10000;"><div style="position: absolute; cursor1: wait; left: ' + _LoadingLeft + 'px; top:' + _LoadingTop + 'px; width: auto; height: 57px; line-height: 57px; padding-left: 50px; padding-right: 5px; background: #fff url(/images/loading.gif) no-repeat scroll 5px 10px; border: 2px solid #95B8E7; color: #696969;">網頁載入中，稍請等待........</div></div>';

//var _LoadingHtml = '<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:' + _PageHeight + 'px;top:0;background:#eef0f1;z-index:10000;"><div style="position: absolute; cursor1: wait; left: ' + _LoadingLeft + 'px; top:' + _LoadingTop + 'px; width: 128px; height: 128px; line-height: 128px;  padding-right: 5px; background:#eef0f1 url(images/loading.gif) no-repeat scroll -64px -64px;"></div></div>';

//呈現loading效果
document.write(_LoadingHtml);

//監聽加載狀態改變
document.onreadystatechange = completeLoading;

//加載狀態為complete時移除loading效果
function completeLoading() {
    if (document.readyState == "complete") {
        var loadingMask = document.getElementById('loadingDiv');
        loadingMask.parentNode.removeChild(loadingMask);
    }
}
