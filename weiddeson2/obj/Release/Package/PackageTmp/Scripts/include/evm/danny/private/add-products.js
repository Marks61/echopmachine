var regexp = /^[1-9]\d*$/;

function checkText() {
    submits = true;
    var product = $('#product').val();

    product != undefined ? pass('product') : failed('product');
   
    if (submits == true) {
        $('#form').submit();
    }

    var value = $('.form-list__item-option-math').val();
    regexp.test(value) ? $('.form-list__item-option-math').removeClass('form-list__item-option-math_error'): $('.form-list__item-option-math').addClass('form-list__item-option-math_error');
}

function failed(type) {
    $('#' + type).addClass('list-content__item-option-input_error');
    submits = false;
}

function pass(type) {
    $('#' + type).removeClass('list-content__item-option-input_error');
    //submits = true;
}
