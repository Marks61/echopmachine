
var regexp = /^[1-9]\d*$/;

function checkText() {
    submits = true;
    var cost = $('#cost').val();
    var price = $('#price').val();
    var stock = $('#stock').val();
    var name = $('#name').val();
    var image = $('#uploadImg').val();
    var routeStr = $('#route').val();
    var sort = $('#vmsort').val();
    var date = $('#date').val();
    regexp.test(cost) ? pass('cost') : failed('cost');
    regexp.test(price) ? pass('price') : failed('price');
    regexp.test(stock) ? pass('stock') : failed('stock');
    regexp.test(date) ? pass('date') : failed('date');
    Number(cost) < Number(price) ? pass('price') : failed('price');
    name !== '' ? pass('name') : failed('name');
    image != '' ? console.log('圖檔合法。') : noimage(routeStr);
    sort != '' ? pass('vmsort') : failed('vmsort');

    if (submits==true) {
        $('#form').submit();
    } 
}

function failed(type) {
    $('#' + type).addClass('list-content__item-option-input_error');
    submits = false;
}

function pass(type) {
    $('#' + type).removeClass('list-content__item-option-input_error');
    //submits = true;
}

function noimage(routeStr) {
    if (routeStr != "Productedit") {
        alert('請上傳商品圖片。');
        submits = false;
    }
}

$(document).ready(() => {
    var submis = true;
    $('#uploadImg').change(function () {
        var file = $('#uploadImg')[0].files[0];
        if ((file.type !== 'image/jpeg' && file.type !== 'image/jpg' && file.type !== 'image/png' && file.type !== 'image/bmp') || file.size >= 307200) {
            var imgValue = document.getElementById('uploadImg');
            imgValue.value = '';
            alert('請上傳正確圖片')
        } else {
            var reader = new FileReader;
            reader.onload = function(e) {
                $('#imgUrl').attr('src', e.target.result);
            };
            reader.readAsDataURL(file);
        }
    });
})


