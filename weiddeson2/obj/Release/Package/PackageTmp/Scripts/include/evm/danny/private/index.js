let hour = '';
let minutes = '';
let second = '';
let year = '';
let month = '';
let day = '';
let weekend = '';

// 時間
function getTime() {
    const date = new Date();
    hour = date.getHours();
    minutes = date.getMinutes(); 
    second = date.getSeconds();
    year = date.getFullYear();
    month = date.getMonth() + 1;
    day = date.getDate();
    weekend = date.getDay();
}

function startTime() {
    getTime();
    hour = checkTime(hour);
    minutes = checkTime(minutes);
    second = checkTime(second);
    var weekendValue = '';
    switch (weekend) {
        case 1:
            weekendValue = '星期一'
            break;
        case 2:
            weekendValue = '星期二'
            break;
        case 3:
            weekendValue = '星期三'
            break;
        case 4:
            weekendValue = '星期四'
            break;
        case 5:
            weekendValue = '星期五'
            break;
        case 6:
            weekendValue = '星期六'
            break;
        case 7:
            weekendValue = '星期七'
            break;
    
        default:
            break;
    }
    document.getElementsByClassName('grid-list__long-txt')[0].innerHTML = year + "/" + month + "/" + day + ' ' + weekendValue + ' ' + hour + ":" + minutes + ":" + second;
    
    let timeoutId = setTimeout(startTime, 1000);
}

function checkTime(i) {
    if(i < 10) {
        i = "0" + i;
    }
    return i;
}

$(window).ready(function() {
    startTime();
})