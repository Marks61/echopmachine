﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class VmsortRepository
    {
        privateRsa privateRsa = new privateRsa();
        private static readonly string sqlconn1 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private static readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);
        public List<Vmsort> GetVmsorts(int uid)
        {
            //SqlConnection sql1 = new SqlConnection(sqlconn5);
            SqlConnection sql1 = new SqlConnection(sqlconn1);
            List<Vmsort> vmsorts = new List<Vmsort>();
            SqlCommand command1 = new SqlCommand(@"select vm_sort.big_sort_id as sort,sort_name from vm_sort inner join product on vm_sort.big_sort_id = product.big_sort_id where product.pro_id in (select pro_id from product) group by vm_sort.big_sort_id,vm_sort.sort_name");
            command1.Parameters.Add(new SqlParameter("@uid", uid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Vmsort vmsort = new Vmsort()
                    {
                        big_sort_id = reader.GetInt32(reader.GetOrdinal("sort")),
                        sort_name = reader.GetString(reader.GetOrdinal("sort_name"))
                    };
                    vmsorts.Add(vmsort);
                }
            }
            sql1.Close();

            return vmsorts;

        }
        public List<Vmsort> GetProductVmsorts(int uid)
        {
            //SqlConnection sql1 = new SqlConnection(sqlconn5);
            SqlConnection sql1 = new SqlConnection(sqlconn1);
            List<Vmsort> vmsorts = new List<Vmsort>();
            SqlCommand command1 = new SqlCommand(@"select vm_sort.big_sort_id as sort,sort_name from vm_sort");
            command1.Parameters.Add(new SqlParameter("@uid", uid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Vmsort vmsort = new Vmsort()
                    {
                        big_sort_id = reader.GetInt32(reader.GetOrdinal("sort")),
                        sort_name = reader.GetString(reader.GetOrdinal("sort_name"))
                    };
                    vmsorts.Add(vmsort);
                }
            }
            sql1.Close();

            return vmsorts;

        }

        public List<Vmsort> GetVmsortsC(int uid)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            List<Vmsort> vmsorts = new List<Vmsort>();
            SqlCommand command1 = new SqlCommand(@"select big_sort_id,sort_name from vm_sort where isUse=1");
            command1.Parameters.Add(new SqlParameter("@uid", uid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Vmsort vmsort = new Vmsort()
                    {
                        big_sort_id = reader.GetInt32(reader.GetOrdinal("big_sort_id")),
                        sort_name = reader.GetString(reader.GetOrdinal("sort_name"))
                    };
                    vmsorts.Add(vmsort);
                }
            }
            sql1.Close();

            return vmsorts;

        }

        public bool Vmsortinsert(int uid,string name)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            try { 
            SqlCommand command1 = new SqlCommand(@"insert into vm_sort(sort_name,isUse) values(@name,1)");
            //command1.Parameters.Add(new SqlParameter("@uid", uid));
            command1.Parameters.Add(new SqlParameter("@name",name));
            command1.Connection = sql1;
            sql1.Open();
            command1.ExecuteNonQuery();
            sql1.Close();
                return true;
            } catch (Exception e)
            {
                return false;
            }
        }

        public bool Vmsortedit(string name,int big_sort_id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            try
            {
                SqlCommand command1 = new SqlCommand(@"update vm_sort set sort_name=@name where big_sort_id=@id");
                command1.Parameters.Add(new SqlParameter("@id",big_sort_id));
                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Connection = sql1;
                sql1.Open();
                command1.ExecuteNonQuery();
                sql1.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Vmsort GetVmsort(int id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            Vmsort vmsort = new Vmsort();
            SqlCommand command1 = new SqlCommand(@"select big_sort_id,sort_name from vm_sort where big_sort_id=@id");
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //big_sort_id = reader.GetInt32(reader.GetOrdinal("big_sort_id")),
                    vmsort.sort_name = reader.GetString(reader.GetOrdinal("sort_name"));
                }
            }
            sql1.Close();

            return vmsort;

        }

        public bool deleteSort(int id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            try
            {
                SqlCommand command1 = new SqlCommand(@"update vm_sort set isUse=0 where big_sort_id=@id");
                command1.Parameters.Add(new SqlParameter("@id", id));
//                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Connection = sql1;
                sql1.Open();
                command1.ExecuteNonQuery();
                sql1.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

    public class Vmsort
    {
        public int big_sort_id { get; set; }
        public string sort_name { get; set; }
        public int vm_uid { get; set; }
    }
}