﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class groupRepository
    {
        privateRsa privateRsa = new privateRsa();
        private static readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn3"]);

        public bool createGroup()
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command1 = new SqlCommand(@"insert into groups " +
                    "(taxId,name,contact,telPhone,faxNumber,email,address,capital,registration,item,is_use,remark,createdAt,createdBy) " +
                    "values(@tax,@comapany,@contact,@tel,@fax,@email,@address,@capital,@registration,@item,1,@remark,@createdAt,@createdBy)");

                return true;
            }catch(Exception e)
            {
                return false;
            }
        }
    }

    public class Group
    {
        public int taxId { get; set; }
        public string name { get; set; }
        public string contact { get; set; }
        public string telPhone { get; set; }
        public object faxNumber { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string capital { get; set; }
        public string registration { get; set; }
        public string item { get; set; }
        public int is_use { get; set; }
        public object remark { get; set; }
        public string createdAt { get; set; }
        public string createdBy { get; set; }
        public object updatedAt { get; set; }
        public object updatedBy { get; set; }
    }
}