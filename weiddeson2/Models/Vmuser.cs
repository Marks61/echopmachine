﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class vmUserRepository
    {
        privateRsa privateRsa = new privateRsa();
        private static readonly string sqlconn1 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private static readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);
        private Password Password = new Password();
        public Vmuser Createadmin()
        {
            /*SqlConnection sql1 = new SqlConnection(sqlconn1);
             string str = Password.Change("lSgph1HU2Pnx");
             Vmuser vmuser = new Vmuser();
             try
             {
                 SqlCommand command1 = new SqlCommand("insert into vm_user(admin,password,name,cm_tel,mail,cm_ad,power) output inserted.vm_uid,inserted.admin,inserted.name,inserted.cm_tel,inserted.mail values(@admin,@password,@name,@cm_tel,@mail,1,0)");
                 command1.Parameters.Add(new SqlParameter("@admin","admin"));
                 command1.Parameters.Add(new SqlParameter("@password",str));
                 command1.Parameters.Add(new SqlParameter("@name","Administrator"));
                 command1.Parameters.Add(new SqlParameter("@cm_tel", "0958260928"));
                 command1.Parameters.Add(new SqlParameter("@mail","ericli@weideson.com"));
                 command1.Connection = sql1;
                 sql1.Open();
                 SqlDataReader reader1 = command1.ExecuteReader();
                 while(reader1.Read())
                 {
                     vmuser.vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid"));
                     vmuser.admin = reader1.GetString(reader1.GetOrdinal("admin"));
                     vmuser.password = str;
                     vmuser.name = reader1.GetString(reader1.GetOrdinal("name"));
                     vmuser.mail = reader1.GetString(reader1.GetOrdinal("mail"));
                 }
                 sql1.Close();
                 return vmuser;
             } catch (Exception e) {
                 return new Vmuser()
                 {
                     vm_uid = 0,
                     admin = null,
                     password = null,
                     name = null,
                     mail = null,
                     cm_tel=null

                 };
             }*/

            SqlConnection sql1 = new SqlConnection(sqlconn1);
            string str = Password.Change("lSgph1HU2Pnx");
            int vmuid = 0;
            Vmuser vmuser = new Vmuser();
            SqlCommand command2 = new SqlCommand(@"select vm_uid from vm_setp_list where vm_setp_id='1'");
            command2.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command2.ExecuteReader();
            if (reader.HasRows)
            {
                while(reader.Read())
                {
                    vmuid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                    break;
                }
            }
            sql1.Close();
            try
            {
                /*SqlCommand command1 = new SqlCommand("insert into admin_user(username,password,flq,power,mail,First) values(@username,@password,0,1,@mail,0)");
                command1.Parameters.Add(new SqlParameter("@username", "admin"));
                command1.Parameters.Add(new SqlParameter("@password", str));
                command1.Parameters.Add(new SqlParameter("@mail", "ericli@weideson.com"));*/
                SqlCommand command1 = new SqlCommand(@"insert into admin_user(username,password,flg,power,First,vm_uid) values(@username,@password,1,1,1,@vmuid)");
                command1.Parameters.Add(new SqlParameter("@username","admin"));
                command1.Parameters.Add(new SqlParameter("@password",Password.Change("weideson")));
                command1.Parameters.Add(new SqlParameter("@vmuid",vmuid));
                command1.Connection = sql1;
                sql1.Open();
                SqlDataReader reader1 = command1.ExecuteReader();
                while (reader1.Read())
                {
                    vmuser.password = "";
                    vmuser.mail = reader1.GetString(reader1.GetOrdinal("mail"));
                }
                sql1.Close();
                return vmuser;
            }
            catch (Exception e)
            {
                return new Vmuser()
                {
                    vm_uid = 0,
                    admin = null,
                    password = null,
                    name = null,
                    mail = null,
                    cm_tel = null

                };
            }
        }

        public bool Createadmin2(string taxid,string mail)
        {
            DateTime date1 = new DateTime();
            date1 = DateTime.Now;
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            string password = Password.highSecurityPassword();
            string str = Password.Change(password);
            Vmuser vmuser = new Vmuser();
            try
            {
                SqlCommand command1 = new SqlCommand("insert into admin_user(username,name,created_at,First,email,cm_id,mailType) values(@username,@username,'"+date1.ToString("yyyy-MM-dd HH:mm:ss")+"',0,@email,@cmid,@Token)");
                command1.Parameters.Add(new SqlParameter("@username", "admin"));
                //command1.Parameters.Add(new SqlParameter("@password", str));
                command1.Parameters.Add(new SqlParameter("@cmid",taxid));
                command1.Parameters.Add(new SqlParameter("@Token",password));
                command1.Parameters.Add(new SqlParameter("@email",mail));
                command1.Connection = sql1;
                sql1.Open();
                command1.ExecuteNonQuery();
                sql1.Close();

                System.Net.Mail.SmtpClient mmail = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                mmail.Credentials = new System.Net.NetworkCredential("service.weideson", "weideson18915");
                mmail.EnableSsl = true;
                string frm = "service.weideson@gmail.com";
                string sject = "帳號開通";
                var by = "請開通帳號，變更密碼。開通網址:http://192.168.50.124/Api2/iPass/"+password;
                mmail.Send(frm, mail, sject, by);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Vmuser CreateUser()
        {
            SqlConnection sql1 = new SqlConnection(sqlconn1);
            string str = Password.Change("123456");
            Vmuser vmuser = new Vmuser();
            try
            {
                SqlCommand command1 = new SqlCommand("insert into admin_user(username,password,flq,power,mail,First) values(@username,@password,0,1,@mail,0)");
                command1.Parameters.Add(new SqlParameter("@username", "user"));
                command1.Parameters.Add(new SqlParameter("@password", str));
                command1.Parameters.Add(new SqlParameter("@mail", "ericli@weideson.com"));
                command1.Connection = sql1;
                sql1.Open();
                SqlDataReader reader1 = command1.ExecuteReader();
                while (reader1.Read())
                {
                    vmuser.password = str;
                    vmuser.mail = reader1.GetString(reader1.GetOrdinal("mail"));
                }
                sql1.Close();
                return vmuser;
            }
            catch (Exception e)
            {
                return new Vmuser()
                {
                    vm_uid = 0,
                    admin = null,
                    password = null,
                    name = null,
                    mail = null,
                    cm_tel = null

                };
            }
        }

        public bool createTest()
        {

            SqlConnection sql1 = new SqlConnection(sqlconn1);
            string str = Password.Change("123456");
            Vmuser vmuser = new Vmuser();
            try
            {
                SqlCommand command1 = new SqlCommand("insert into vm_user(self_uid,admin,password,name,cm_tel,mobile,cm_website,cm_address,u_type,u_zt,power,vmuserid) values(8,@admin,@password,@name,@cm_tel,@mobile,@cm_website,@cm_address,@u_type,@u_zt,5,0)");
                command1.Parameters.Add(new SqlParameter("@admin", "admin2"));
                command1.Parameters.Add(new SqlParameter("@password", str));
                command1.Parameters.Add(new SqlParameter("@name","購便利連鎖無人商店"));
                command1.Parameters.Add(new SqlParameter("cm_tel","07-7675186"));
                command1.Parameters.Add(new SqlParameter("@mobile","0905286198"));
                command1.Parameters.Add(new SqlParameter("@cm_website","http://www.z-go.com.tw"));
                command1.Parameters.Add(new SqlParameter("@cm_address","高雄市鳳山區南京路262號"));
                command1.Parameters.Add(new SqlParameter("@u_type","企業"));
                command1.Parameters.Add(new SqlParameter("@u_zt","1"));
                command1.Connection = sql1;
                sql1.Open();
                command1.ExecuteNonQuery();
                sql1.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
         
        }

        public Vmuser Vmuserinfo(int uid)
        {
            Vmuser vmuser = new Vmuser();
            SqlConnection sql1 = new SqlConnection(sqlconn1);
            SqlCommand command1 = new SqlCommand("select username from admin_user where vm_uid=@aid");
            command1.Parameters.Add(new SqlParameter("@aid",uid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            while (reader1.Read())
            {
                vmuser.name = reader1.GetString(reader1.GetOrdinal("username"));
                vmuser.admin = reader1.GetString(reader1.GetOrdinal("username"));
                //vmuser.vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid"));
                break;
            }
            sql1.Close();

            SqlCommand command2 = new SqlCommand(@"select vm_num,vm_uid from vm_setp_list where vm_uid=@uid");
            command2.Connection = sql1;
            command2.Parameters.Add(new SqlParameter("@uid",uid));
            sql1.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            while (reader2.Read())
            {
                vmuser.vm_uid = reader2.GetInt32(reader2.GetOrdinal("vm_uid"));
                vmuser.vm_num = reader2.GetString(reader2.GetOrdinal("vm_num"));
                break;
            }
            sql1.Close();

            return vmuser;
        }

        public Vmuser Vmuserinfo2(string uid)
        {
            Vmuser vmuser = new Vmuser();
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            SqlCommand command1 = new SqlCommand("select admin,name from sub_user where usertype=@usertype");
            command1.Parameters.Add(new SqlParameter("@usertype", uid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            while (reader1.Read())
            {
                vmuser.name = reader1.GetString(reader1.GetOrdinal("name"));
                vmuser.admin = reader1.GetString(reader1.GetOrdinal("admin"));
                //vmuser.vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid"));
                break;
            }
            sql1.Close();

            /*SqlCommand command2 = new SqlCommand(@"select vm_uid,vm_num from vm_setp_list where usertype=@uid");
            command2.Parameters.Add(new SqlParameter("@uid", uid));
            command2.Connection = sql1;
            sql1.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
            while (reader2.Read())
            {
                vmuser.vm_uid = reader2.GetInt32(reader2.GetOrdinal("vm_uid"));
                vmuser.vm_num = reader2.GetString(reader2.GetOrdinal("vm_num"));
                break;
            }
            }

            sql1.Close();*/

            return vmuser;
        }

        public bool doPass(string password,string token)
        {
            DateTime date1 = new DateTime();
            date1 = DateTime.Now;
            DateTime date2 = new DateTime();
            SqlConnection sql = new SqlConnection(sqlconn5);
            int ad = 0;
            string mail = "";
            Password password1 = new Password();

            SqlCommand command1 = new SqlCommand(@"select ad_id,created_at,email from admin_user where mailType=@token");
            command1.Parameters.Add(new SqlParameter("@token",token));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if(reader1.HasRows)
            {
                while(reader1.Read())
                {
                    ad = reader1.GetInt32(reader1.GetOrdinal("ad_id"));
                    date2 = reader1.GetDateTime(reader1.GetOrdinal("created_at"));
                    mail = reader1.GetString(reader1.GetOrdinal("email"));
                }
            }
            else
            {
                return false;
            }
            sql.Close();

            if (date2.AddHours(3) < date1)
            {
                SqlCommand command3 = new SqlCommand("update admin_user set created_at="+date1.ToString("yyyy-MM-dd HH:mm:ss")+",mailType=@token where ad_id=@id");
                command3.Parameters.Add(new SqlParameter("@token",password1.highSecurityPassword()));
                //command1.Parameters.Add(new SqlParameter("@password", str));
                command3.Parameters.Add(new SqlParameter("@ad", ad));
                command3.Connection = sql;
                sql.Open();
                command3.ExecuteNonQuery();
                sql.Close();

                System.Net.Mail.SmtpClient mmail = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                mmail.Credentials = new System.Net.NetworkCredential("service.weideson", "weideson18915");
                mmail.EnableSsl = true;
                string frm = mail;
                string sject = "帳號開通";
                var by = "請開通帳號，變更密碼。開通網址:http://192.168.50.124/Api2/iPass/" + password;
                mmail.Send(frm, mail, sject, by);
                return false;
            }
            else
            {
                SqlCommand command2 = new SqlCommand(@"update admin_user set First=1,password=@password,mailType=null where ad_id=@id");
                command2.Parameters.Add(new SqlParameter("@password",password1.Change(password)));
                command2.Parameters.Add(new SqlParameter("@id", ad));
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();

                return true;
            }
        }

    }

    public class Vmuser
    {
        public int vm_uid { get; set; }
        public string admin { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string cm_name { get; set; }
        public string cm_tel { get; set; }
        public string mobile { get; set; }
        public string mail { get; set; }
        public string bscode { get; set; }
        public string cm_website { get; set; }
        public string cm_address { get; set; }
        public string cm_ad { get; set; }
        public string u_type { get; set; }
        public int u_zt { get; set; }
        public int power { get; set; }
        public string vmuserid { get; set; }
        public string bradcast_info { get; set; }
        public int preopenday { get; set; }

        public string vm_num { get; set; }
    }
}