﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Management;
using System.Web.UI;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class MachineRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);


        //取得主機號碼
        public int getVmuid()
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            int uid = 0;

            SqlCommand command1 = new SqlCommand(@"select vm_uid from vm_setp_list");
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    uid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                    break;
                }
            }
            sql1.Close();

            return uid;
        }

        public Machine getuuid(int vmuid)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            Machine machine = new Machine();

            SqlCommand command1 = new SqlCommand(@"select vm_num from vm_setp_list where vm_uid=@uid");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    machine.vm_num = reader.GetString(reader.GetOrdinal("vm_num"));
                    break;
                }
            }
            sql1.Close();

            return machine;
        }

        public int getCloudVmuid(string vm_id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            Machine machine = new Machine();
            int uid = 0;
            SqlCommand command1 = new SqlCommand(@"select vm_uid from vm_list where vm_id=@uid");
            command1.Parameters.Add(new SqlParameter("@uid", vm_id));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    uid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                    break;
                }
            }
            sql1.Close();

            return uid;
        }

        public Temprature GetTemprature(int vmuid)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            Temprature temprature = new Temprature();

            SqlCommand command1 = new SqlCommand(@"select top 1 temperature from temperature_log where vm_uid=@uid order by temperature_log_id desc");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    temprature.temperature = reader.GetString(reader.GetOrdinal("temperature"));
                    break;
                }
            }
            sql1.Close();

            return temprature;
        }

        public string getModel(string vmuid)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            string model = "";

            SqlCommand command1 = new SqlCommand(@"select vm_mode_name from vm_mode_list where vm_mode_id=(select vm_mode_id from vm_setp_list where vm_uid=@uid)");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    model = reader.GetString(reader.GetOrdinal("vm_mode_name"));
                    break;
                }
            }
            sql1.Close();

            return model;
        }

        public weiddeson2.Controllers.IndexController.mailInfo getMailinfo(int uid)
        {
            weiddeson2.Controllers.IndexController.mailInfo mailInfo = new Controllers.IndexController.mailInfo();
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlCommand command1 = new SqlCommand(@"select cm_id,st_id from vm_list where vm_uid=@uid");
            command1.Parameters.Add(new SqlParameter("@uid",uid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if(reader1.HasRows)
            {
                while (reader1.Read())
                {
                    mailInfo.cm_id = reader1.GetString(reader1.GetOrdinal("cm_id"));
                    mailInfo.st_id = reader1.GetString(reader1.GetOrdinal("st_id"));
                    break;
                }
            }
            sql.Close();

            //get mail，name

            SqlCommand command2 = new SqlCommand(@"select admin_user.email as mail,customers.cm_name as name from admin_user inner join customers on admin_user.cm_id=customers.cm_id where customers.cm_id=@id");
            command2.Parameters.Add(new SqlParameter("@id",mailInfo.cm_id));
            command2.Connection = sql;
            sql.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    mailInfo.email = reader2.GetString(reader2.GetOrdinal("mail"));
                    mailInfo.name = reader2.GetString(reader2.GetOrdinal("name"));
                    break;
                }
            }
            sql.Close();

            return mailInfo;
        }
    }

    public class Machine
    {
        public int vm_setp_id { get; set; }
        public string vm_num { get; set; }
        public int vm_mode_ud { get; set; }
        public int vm_layer_qty { get; set; }
        public string cloud_data_ip { get; set; }
        public string cloud_api_url { get; set; }
        public int vm_uid { get; set; }
        public int infrared_sensor { get; set; }
        public int MACHINETYPE { get; set; }
    }

    public class Temprature
    {
        public int iemperature_log_id { get; set; }
        public string temperature { get; set; }
        public string tp_date { get; set; }
        public string tp_time { get; set; }
        public string vm_num { get; set; }
        public int vm_uid { get; set; }
    }

    public class Model
    {
        public int vm_mode_id { get; set; }
        public string vm_mode_name { get; set; }
        public string vm_mode_num { get; set; }
        public string p_size { get; set; }
        public string ac_power { get; set; }
        public int is_sp { get; set; }
        public int is_lifts { get; set; }
    }
}