﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Razor.Text;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class CloudstoreRepository
    {
        privateRsa privateRsa = new privateRsa();
        //private readonly string sqlconn = System.Configuration.ConfigurationManager.AppSettings["sqlconn"];
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);
    
        public List<CloudStore> cloudStores(string id)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            List<CloudStore> cloudStores = new List<CloudStore>();
            SqlCommand command = new SqlCommand(@"select st_id,cm_id,name,country,city,zip,address,location from store_location where cm_id=@id and isUse=1");
            command.Connection = sql;
            command.Parameters.Add(new SqlParameter("@id",id));
            sql.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CloudStore cloudStore = new CloudStore()
                    {
                        st_id = reader.GetString(reader.GetOrdinal("st_id")),
                        cm_id=reader.GetString(reader.GetOrdinal("cm_id")),
                        name=reader.GetString(reader.GetOrdinal("name")),
                        country=reader.GetString(reader.GetOrdinal("country")),
                        city=reader.GetString(reader.GetOrdinal("city")),
                        zip=reader.GetString(reader.GetOrdinal("zip")),
                        address=reader.GetString(reader.GetOrdinal("address")),
                        location=reader.GetString(reader.GetOrdinal("location"))
                    };

                    cloudStores.Add(cloudStore);
                }
            }
            sql.Close();
            return cloudStores;
        }

        public bool insert(string taxid, string country, string city, int zip, string address, string location, string name)
        {
            string sqlcommandStr = "";
            SqlConnection sql = new SqlConnection(sqlconn5);
            try
            {
                sqlcommandStr += "insert into store_location (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "st_id,"+
                    "cm_id," +
                    "name," +
                    "country," +
                    "city," +
                    "zip," +
                    "address," +
                    "location," +
                    "isUse)";
                sqlcommandStr += " values('"+ Convert.ToString(Guid.NewGuid()) +"',"+
                    "@cmid,"+
                    "@name" + "," +
                    "@country," +
                    "@city," +
                    "@zip," +
                    "@address," +
                    "@location" +
                    ",1)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@cmid", taxid));
                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Parameters.Add(new SqlParameter("@country", country));
                command1.Parameters.Add(new SqlParameter("@city", city));
                command1.Parameters.Add(new SqlParameter("@zip", zip));
                command1.Parameters.Add(new SqlParameter("@address", address));
                command1.Parameters.Add(new SqlParameter("@location", location));
                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
        public class CloudStore
    {
        public string st_id { get; set; }
        public string cm_id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string address { get; set; }
        public string location { get; set; }
    }
}