﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace weiddeson2.Models
{
    public class errorService
    { 
        public List<Error> loginError()
        {
            List<Error> errors = new List<Error>();

            errors.Add(
               new Error()
               {
                   token = "401001",
                   message = "帳號欄位不可留空。",
                   pageError = "401"
               }
            );

            errors.Add(
               new Error()
               {
                   token = "401002",
                   message = "密碼欄位不可留空。",
                   pageError = "401"
               }
            );

            errors.Add(
                new Error()
                {
                    token = "401003",
                    message = "登入資訊不正確。",
                    pageError = "401"
                }
             );

            return errors;
        }
    }

    public class Error
    {
        public string token { get; set; }
        public string message { get; set; }
        public string pageError { get; set; }
        public int vm_uid { get; set; }
        public string cm_id { get; set; }
        public string vm_num { get; set; }
        public string name { get; set; }
        public bool First { get; set; }
    }
}