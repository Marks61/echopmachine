﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http.Results;
using System.Web.UI.WebControls;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{

    public class CloudcustormersRepository
    {
        privateRsa privateRsa = new privateRsa();
        //private readonly string sqlconn = System.Configuration.ConfigurationManager.AppSettings["sqlconn"];
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);
    
        public List<Cloudcustormers> cloudcustormers()
        {
            List<Cloudcustormers> cloudcustormers = new List<Cloudcustormers>();
            SqlConnection sql = new SqlConnection(sqlconn5);

            SqlCommand command = new SqlCommand(@"select cm_id,cm_name,cm_email,cm_type,phone,fax,mobile,country,city,zip,address,note from customers where isUse=1");
            command.Connection = sql;
            sql.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while(reader.Read())
                {
                    Cloudcustormers cloudcustormers1 = new Cloudcustormers() {
                        cm_id=reader.GetString(reader.GetOrdinal("cm_id")),
                        cm_name = reader.GetString(reader.GetOrdinal("cm_name")),
                        cm_email = reader.GetString(reader.GetOrdinal("cm_email")),
                        cm_type = reader.IsDBNull(3) ? "" : reader.GetString(reader.GetOrdinal("cm_type")),
                        phone = reader.GetString(reader.GetOrdinal("phone")),
                        fax = reader.IsDBNull(5) ? "" : reader.GetString(reader.GetOrdinal("fax")),
                        mobile = reader.IsDBNull(6) ? "" : reader.GetString(reader.GetOrdinal("mobile")),
                        country = reader.IsDBNull(7) ? "" : reader.GetString(reader.GetOrdinal("country")),
                        city = reader.GetString(reader.GetOrdinal("city")),
                        zip=reader.GetString(reader.GetOrdinal("zip")),
                        address=reader.GetString(reader.GetOrdinal("address")),
                        note= reader.IsDBNull(11) ? "" : reader.GetString(reader.GetOrdinal("note"))
                    };

                    cloudcustormers.Add(cloudcustormers1);
                }
            }
            sql.Close();

            return cloudcustormers;
           
        }

        public bool insert(string taxid,string name, string email, string type, string phone, string fax, string mobile, string country, string city, int zip, string address, string note)
        {
            string sqlcommandStr = "";
            SqlConnection sql = new SqlConnection(sqlconn5);
            /*int pro_id = 0;
            SqlCommand command2 = new SqlCommand(@"select top 1 cm_id from customers order by cm_id desc");
            command2.Connection = sql;
            sql.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            while (reader2.Read())
            {
                pro_id = reader2.GetInt32(reader2.GetOrdinal("cm_id")) + 1;
                break;
            }
            sql.Close();*/

            try
            {
                sqlcommandStr += "insert into customers (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "cm_id," +
                    "cm_name," +
                    "cm_email," +
                    "cm_type," +
                    "phone," +
                    "fax," +
                    "mobile," +
                    "country," +
                    "city," +
                    "zip," +
                    "address," +
                    "note," +
                    "isUse)";
                sqlcommandStr += " values(";
                sqlcommandStr += "@cmid,"
                    + "@name" + "," +
                    "@email," +
                    (type != "null" ? "@type," : "null,") +
                    phone + "," +
                    fax + "," +
                    mobile + "," +
                    "@country," +
                    "@city," +
                    "@zip," +
                    "@address," +
                    (note != "null"? "@note":"null") +
                    ",1)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@cmid", taxid));
                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Parameters.Add(new SqlParameter("@email", email));
                command1.Parameters.Add(new SqlParameter("@type", type));
                command1.Parameters.Add(new SqlParameter("@country", country));
                command1.Parameters.Add(new SqlParameter("@city", city));
                command1.Parameters.Add(new SqlParameter("@zip", zip));
                command1.Parameters.Add(new SqlParameter("@address", address));
                command1.Parameters.Add(new SqlParameter("@note", note));
                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Cloudcustormers info(string taxid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            Cloudcustormers cloudcustormers = new Cloudcustormers();
            SqlCommand command = new SqlCommand(@"SELECT cm_name,cm_email,cm_type,phone,fax,mobile,country,city,zip,address,note FROM customers where cm_id=@tax");
            command.Parameters.Add(new SqlParameter("@tax",taxid));
            command.Connection = sql;
            sql.Open();
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    cloudcustormers.cm_name = reader.GetString(reader.GetOrdinal("cm_name"));
                    cloudcustormers.cm_email = reader.GetString(reader.GetOrdinal("cm_email"));
                    cloudcustormers.cm_type = reader.GetString(reader.GetOrdinal("cm_type"));
                    cloudcustormers.phone = reader.GetString(reader.GetOrdinal("phone"));
                    cloudcustormers.fax = reader.IsDBNull(4)?"":reader.GetString(reader.GetOrdinal("fax"));
                    cloudcustormers.mobile = reader.IsDBNull(5)?"":reader.GetString(reader.GetOrdinal("mobile"));
                    cloudcustormers.country = reader.GetString(reader.GetOrdinal("country"));
                    cloudcustormers.city = reader.GetString(reader.GetOrdinal("city"));
                    cloudcustormers.zip = reader.GetString(reader.GetOrdinal("zip"));
                    cloudcustormers.address = reader.GetString(reader.GetOrdinal("address"));
                    cloudcustormers.note = reader.IsDBNull(10)?"":reader.GetString(reader.GetOrdinal("note"));
                }
            }
            sql.Close();

            return cloudcustormers;
        }

        public bool edit(string taxid, string name, string email, string type, string phone, string fax, string mobile, string country, string city, int zip, string address, string note)
        {
            string sqlcommandStr = "";
            SqlConnection sql = new SqlConnection(sqlconn5);
            try
            {
                sqlcommandStr += "update customers set ";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "cm_name=@name," +
                    "cm_email=@email," +
                    "cm_type=@type," +
                    "phone=@phone," +
                    (fax == "" ? "" : "fax=@fax,") +
                    (mobile == "" ? "" : "mobile=@mobile,") +
                    "country=@country," +
                    "city=@city," +
                    "zip=@zip," +
                    "address=@address," +
                    (note == "" ? "" : "note=@note") +")";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Parameters.Add(new SqlParameter("@email", email));
                command1.Parameters.Add(new SqlParameter("@type", type));
                command1.Parameters.Add(new SqlParameter("@phone",phone));
                command1.Parameters.Add(new SqlParameter("@fax", fax));
                command1.Parameters.Add(new SqlParameter("@mobile",mobile));
                command1.Parameters.Add(new SqlParameter("@country", country));
                command1.Parameters.Add(new SqlParameter("@city", city));
                command1.Parameters.Add(new SqlParameter("@zip", zip));
                command1.Parameters.Add(new SqlParameter("@address", address));
                command1.Parameters.Add(new SqlParameter("@note", note));
                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
        public class Cloudcustormers
    {
        public string cm_id { get; set; }
        public string cm_name { get; set; }
        public string cm_email { get; set; }
        public string cm_type { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string mobile { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string address { get; set; }
        public string note { get; set; }

    }
}