﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing.Printing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.WebSockets;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class ProductRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);

        /*
         * 商品盤整並顯示
         */
        public List<Product> frontedGetLayerVer2()
        {
            List<Product> products = new List<Product>();
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select pro_id,pdName,content1,pic,unit_price,Shelf_life_day from vm_pro_qty where pro_id != 0 and road_act=1 group by pic,pro_id,pdname,content1,unit_price,Shelf_life_day order by pro_id asc");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Product product = new Product()
                    {
                        //roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer")),
                        //roadId = reader1.GetString(reader1.GetOrdinal("roadid")),
                        proId = reader1.GetInt32(reader1.GetOrdinal("pro_id")),
                        pdName = reader1.GetString(reader1.GetOrdinal("pdname")),
                        content = reader1.GetString(reader1.GetOrdinal("content1")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        unitPrice = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        proQty = productQty(reader1.GetInt32(reader1.GetOrdinal("pro_id"))),
                        //expiryDate = reader1.GetString(reader1.GetOrdinal("expiry_date")),
                        shelfLifeDay = reader1.GetString(reader1.GetOrdinal("Shelf_life_day"))
                    };

                    products.Add(product);
                }
            }
            sql.Close();

            return products;
        }

        /*
         * 商品貨道狀態輪巡
         */
        public List<Product> frontedGetLayers()
        {
            List<Product> products = new List<Product>();
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select road_layer,roadid,pro_id,pdName,content1,pic,unit_price,proqty,expiry_date,Shelf_life_day from vm_pro_qty where pro_id != 0 and road_act=1");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Product product = new Product()
                    {
                        roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer")),
                        roadId = reader1.GetString(reader1.GetOrdinal("roadid")),
                        proId = reader1.GetInt32(reader1.GetOrdinal("pro_id")),
                        pdName = reader1.GetString(reader1.GetOrdinal("pdname")),
                        content = reader1.GetString(reader1.GetOrdinal("content1")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        unitPrice = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        proQty = reader1.GetInt32(reader1.GetOrdinal("proqty")),
                        expiryDate = reader1.GetString(reader1.GetOrdinal("expiry_date")),
                        shelfLifeDay = reader1.GetString(reader1.GetOrdinal("Shelf_life_day"))
                    };

                    products.Add(product);
                }
            }
            sql.Close();

            return products;
        }
        
        public int productQty(int proid)
        {
            int count = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select sum(proqty) as proqtys from vm_pro_qty where pro_id=@id");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@id",proid));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    count = reader1.IsDBNull(0)?0:reader1.GetInt32(reader1.GetOrdinal("proqtys"));
                    break;
                }
            }
            sql.Close();
            try {
                SqlCommand command2 = new SqlCommand("update product set amount=@amount where pro_id=@id");
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@id", proid));
                command2.Parameters.Add(new SqlParameter("@amount",count));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
            }
            catch(Exception e) { }
            return count;
        }

        public int frontedGetProductQty(int roadid)
        {
            int count = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select proqty from vm_pro_qty where roadid=@roadid");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid",roadid));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while(reader1.Read())
                {
                    count = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                }
            }
            sql.Close();
            return count;
        }

        public int getRealId(int roadid)
        {
            int road = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            //SqlCommand command1 = new SqlCommand("select unit_price from vm_pro_qty where roadid=@roadid and vm_num='202007150001'");
            SqlCommand command1 = new SqlCommand("select top 1 roadid from vm_pro_qty where pro_id=@roadid and proqty > 0 and expiry_date > '"+DateTime.Now.ToString("yyyy/MM/dd")+"' order by expiry_date asc,roadid asc ");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid", roadid));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while(reader1.Read())
                {
                    road = Convert.ToInt32(reader1.GetString(reader1.GetOrdinal("roadid")));
                    break;
                }

            }
            sql.Close();
            return road;
        }

        public int getRealPrice(int roadid)
        {
            string road = "";
            if (Convert.ToInt32(roadid) <= 9)
            {
                road = "0" + roadid;
            }
            else
            {
                road = Convert.ToString(roadid);
            }
            int price = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            //SqlCommand command1 = new SqlCommand("select unit_price from vm_pro_qty where roadid=@roadid and vm_num='202007150001'");
            SqlCommand command1 = new SqlCommand("select unit_price from vm_pro_qty where roadid=@roadid");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid", road));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    price = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                }
            }
            sql.Close();
            return price;
        }

        public string getRealName(int roadid)
        {
            string road = "";
            if (Convert.ToInt32(roadid) <= 9)
            {
                road = "0" + roadid;
            }
            else
            {
                road = Convert.ToString(roadid);
            }
            string pdname = "";
            SqlConnection sql = new SqlConnection(sqlconn);
            //SqlCommand command1 = new SqlCommand("select unit_price from vm_pro_qty where roadid=@roadid and vm_num='202007150001'");
            SqlCommand command1 = new SqlCommand("select pdname from vm_pro_qty where roadid=@roadid");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid", road));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    pdname = reader1.GetString(reader1.GetOrdinal("pdname"));
                }
            }
            sql.Close();
            return pdname;
        }

        public string getRealPic(int roadid)
        {
            string road = "";
            if (Convert.ToInt32(roadid) <= 9)
            {
                road = "0" + roadid;
            }
            else
            {
                road = Convert.ToString(roadid);
            }
            string pic = "";
            SqlConnection sql = new SqlConnection(sqlconn);
            //SqlCommand command1 = new SqlCommand("select unit_price from vm_pro_qty where roadid=@roadid and vm_num='202007150001'");
            SqlCommand command1 = new SqlCommand("select pic from vm_pro_qty where roadid=@roadid");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid", road));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    pic = reader1.GetString(reader1.GetOrdinal("pic"));
                }
            }
            sql.Close();
            return pic;
        }

        public bool updateQtyData(string roadid)
        {
            string road = "";
            Product product1 = new Product();
            if (Convert.ToInt32(roadid) <= 9)
            {
                road = "0" + roadid;
            }
            else
            {
                road = roadid;
            }
            int pCount = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select proqty from vm_pro_qty where roadid=@roadid");
            command1.Connection = sql;
            command1.Parameters.Add(new SqlParameter("@roadid", road));
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    pCount = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                }
            }
            sql.Close();

            pCount --;

            try {
                SqlCommand command2 = new SqlCommand(@"update vm_pro_qty set proqty="+pCount+" where roadid=@roadid");
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@roadid", road));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public Product getAll(string Roadid)
        {
            string road = "";
            if (Convert.ToInt32(Roadid) <= 9)
            {
                road = "0" + Roadid;
            }
            else
            {
                road = Convert.ToString(Roadid);
            }
            Product product1 = new Product();
            SqlConnection sql = new SqlConnection(sqlconn);
            //SqlCommand command1 = new SqlCommand("select pdname,road_num,road_layer,roadid,layer_num,roadtyp,vm_num,unit_price,cost_price,bonus,point,pro_id,proqty from vm_pro_qty where vm_num=@vm_num and roadid=@roadid");
            SqlCommand command1 = new SqlCommand("select vm_uid,pdname,road_num,road_layer,roadid,layer_num,roadtyp,vm_num,unit_price,cost_price,bonus,point,pro_id,proqty from vm_pro_qty where roadid=@roadid");
            command1.Parameters.Add(new SqlParameter("@roadid", road));
            //command1.Parameters.Add(new SqlParameter("@vm_num", "202007150001"));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    product1.vmUid = reader1.GetInt32(reader1.GetOrdinal("vm_uid"));
                    product1.pdName = reader1.GetString(reader1.GetOrdinal("pdname"));
                    //product1.roadNum = reader1.GetString(reader1.GetOrdinal("road_num"));
                    product1.roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer"));
                    product1.roadId = reader1.GetString(reader1.GetOrdinal("roadid"));
                    product1.layerNum = reader1.GetInt32(reader1.GetOrdinal("road_layer"));
                    product1.roadtype = reader1.GetString(reader1.GetOrdinal("roadtyp"));
                    product1.vmNum = reader1.GetString(reader1.GetOrdinal("vm_num"));
                    product1.proId = reader1.GetInt32(reader1.GetOrdinal("pro_id"));
                    product1.unitPrice = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                    product1.costPrice = reader1.GetInt32(reader1.GetOrdinal("cost_price"));
                    //product1.bonus = reader1.GetString(reader1.GetOrdinal("bonus"));
                    //product1.point = reader1.GetString(reader1.GetOrdinal("point"));
                    product1.proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                   
                    break;
                }
            }
            sql.Close();

            return product1;
        }

        public List<Mainproduct> GetMainproducts(string sort)
        {
            List<Mainproduct> products = new List<Mainproduct>();
            SqlConnection sql = new SqlConnection(sqlconn);
            
            string str = "";
            
            if(sort == "0")
            {
                str = "select pdname,unit_price,pic,content1,id,cost_price,amount,vm_uid,vm_num,big_sort_id,pro_id from product where isUse=1 order by id desc";
            }
            else
            {
                str = "select pdname,unit_price,pic,content1,id,cost_price,amount,vm_uid,vm_num,big_sort_id,pro_id from product where isUse=1 and big_sort_id=@sort order by id desc";
            
            }
            SqlCommand command1 = new SqlCommand(str);
            command1.Parameters.Add(new SqlParameter("@sort",sort));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Mainproduct product = new Mainproduct()
                    {
                        id = reader1.GetInt32(reader1.GetOrdinal("id")),
                        pdname = reader1.GetString(reader1.GetOrdinal("pdname")),
                        unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        content1 = reader1.IsDBNull(3) ? "" : reader1.GetString(reader1.GetOrdinal("content1")),
                        cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        amount = reader1.GetString(reader1.GetOrdinal("amount")),
                        vm_num = reader1.GetString(reader1.GetOrdinal("vm_num")),
                        vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid")),
                        big_sort_id = reader1.GetInt32(reader1.GetOrdinal("big_sort_id")),
                        sortName = getSortname(reader1.GetInt32(reader1.GetOrdinal("big_sort_id"))),
                        pro_id = reader1.GetInt32(reader1.GetOrdinal("pro_id"))
                    };

                    products.Add(product);
                }
            }
            sql.Close();

            return products;
        }

        public Product Getproduct(int id,int type)
        {
            Product product = new Product();
            SqlConnection sql = new SqlConnection(sqlconn);
            string str = "";
            if (type == 1)
            {
                str = "select layer_num,road_num,pro_id,proqty,roadid,vmprid from vm_pro_qty where vmprid=@id";
            }
            else
            {
                str = "select road_layer,pro_id,proqty,roadid,vmprid from vm_pro_qty where vmprid=@id";
            }
            SqlCommand command1 = new SqlCommand(str);
            command1.Parameters.Add(new SqlParameter("@id",id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    if (type == 1)
                    {
                        product.layerNum = reader1.GetInt32(reader1.GetOrdinal("layer_num"));
                        product.roadNum = reader1.GetString(reader1.GetOrdinal("road_num"));
                        product.proId = reader1.GetInt32(reader1.GetOrdinal("pro_id"));
                        product.proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                        product.roadId = reader1.GetString(reader1.GetOrdinal("roadid"));
                        product.vmprid = reader1.GetInt32(reader1.GetOrdinal("vmprid"));
                    }
                    else
                    {
                        product.roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer"));
                        product.proId = reader1.GetInt32(reader1.GetOrdinal("pro_id"));
                        product.proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                        product.roadId = reader1.GetString(reader1.GetOrdinal("roadid"));
                        product.vmprid = reader1.GetInt32(reader1.GetOrdinal("vmprid"));
                    }
                    break;
                }
            }
            sql.Close();

            return product;
        }

        public List<Mainproduct> GetMainproductsC(string sort)
        {
            List<Mainproduct> products = new List<Mainproduct>();
            SqlConnection sql = new SqlConnection(sqlconn5);
            string str = "";
            if (sort == "")
            {
                str = "select pdname,unit_price,pic,content1,id,cost_price,amount,vm_uid,vm_num,big_sort_id,shelf_life_day from product where isUse=1 order by id desc";
            }
            else
            {
                str = "select pdname,unit_price,pic,content,id,cost_price,amount,vm_uid,vm_num,big_sort_id,shelf_life_day from product where big_sort_id=@sort and isUse=1 order by id desc";
            }
            SqlCommand command1 = new SqlCommand(str);
            command1.Parameters.Add(new SqlParameter("@sort", sort));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Mainproduct product = new Mainproduct()
                    {
                        id = reader1.GetInt32(reader1.GetOrdinal("id")),
                        pdname = reader1.GetString(reader1.GetOrdinal("pdname")),
                        unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        content1 = reader1.IsDBNull(3) ? "" : reader1.GetString(reader1.GetOrdinal("content1")),
                        cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                        pic = System.Configuration.ConfigurationManager.AppSettings["picture"] +"/images/pic/Product/" + reader1.GetString(reader1.GetOrdinal("pic")),
                        amount = reader1.GetString(reader1.GetOrdinal("amount")),
                       // vm_num = reader1.GetString(reader1.GetOrdinal("vm_num")),
                       // vm_uid = reader1.GetInt32(reader1.GetOrdinal("vm_uid")),
                        big_sort_id = reader1.GetInt32(reader1.GetOrdinal("big_sort_id")),
                        sortName = getSortname(reader1.GetInt32(reader1.GetOrdinal("big_sort_id"))),
                        shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day"))
                    };

                    products.Add(product);
                }
            }
            sql.Close();

            return products;
        }

        public string getSortname(int id)
        {
            SqlConnection sql = new SqlConnection(sqlconn);

            string sort = "";
         
            SqlCommand command1 = new SqlCommand(@"select sort_name from vm_sort where big_sort_id=@id");
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    sort = reader1.GetString(reader1.GetOrdinal("sort_name"));
                }
            }
            sql.Close();

            return sort;
        }

        public bool insertProduct(string bigsortid,string pdname,int unit_price,int cost_price,string content1,int amount,string shelf_life_day,string filename,string vmnum,int vmuid)
        {
            int pro_id = 0;
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command2 = new SqlCommand(@"select top 1 pro_id from product where vm_num=@vmnum order by pro_id desc");
            command2.Parameters.Add(new SqlParameter("@vmnum",vmnum));
            command2.Connection = sql;
            sql.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            while(reader2.Read())
            {
                pro_id = reader2.GetInt32(reader2.GetOrdinal("pro_id"))+1;
                break;
            }
            sql.Close();

            try {
                string sortId = "";
                int bonus = 0;
                int point = 0;
                string sqlcommandStr = "";
                if (bigsortid != null)
                {
                    sortId = "'" + bigsortid + "'";
                }
                else
                {
                    sortId = "NULL";
                }

                if (content1 == "")
                {
                    content1 = "暫無簡介";
                }

                sqlcommandStr += "insert into product (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "pro_id," +
                    "big_sort_id," +
                    "pdname," +
                    "amount," +
                    "vm_num," +
                    "pic," +
                    "content1,"+
                    "unit_price," +
                    "cost_price," +
                    "bonus," +
                    "point," +
                    "vm_uid," +
                    "vm_tenant_id," +
                    "shelf_life_day)";
                sqlcommandStr += " values(";
                sqlcommandStr += "@proid,"
                    + sortId + "," +
                    "@pdname," +
                    "@amount," +
                    "@vmnum,"
                    +"@filename,"+
                    "@content1,"+
                    "@unit," +
                    "@cost," +
                    "@bonus," +
                    "@point," +
                    "@vmuid," +
                    "0," +
                    "@lifeday)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@pdname",pdname));
                command1.Parameters.Add(new SqlParameter("@amount", amount));
                command1.Parameters.Add(new SqlParameter("@vmnum", vmnum));
                command1.Parameters.Add(new SqlParameter("@proid", pro_id));
                command1.Parameters.Add(new SqlParameter("@filename", filename));
                command1.Parameters.Add(new SqlParameter("@content1",content1));
                command1.Parameters.Add(new SqlParameter("@unit", unit_price));
                command1.Parameters.Add(new SqlParameter("@cost", cost_price));
                command1.Parameters.Add(new SqlParameter("@bonus", bonus));
                command1.Parameters.Add(new SqlParameter("@point", point));
                command1.Parameters.Add(new SqlParameter("@vmuid", vmuid));
                command1.Parameters.Add(new SqlParameter("@lifeday",shelf_life_day));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public bool insertProductC(string bigsortid, string pdname, int unit_price, int cost_price, string content1, int amount, string shelf_life_day, string filename, string vmnum="", int vmuid=0)
        {
            int pro_id = 0;
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlCommand command2 = new SqlCommand(@"select top 1 id from product order by pro_id desc");
            //command2.Parameters.Add(new SqlParameter("@vmnum", vmnum));
            command2.Connection = sql;
            sql.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            while (reader2.Read())
            {
                pro_id = reader2.GetInt32(reader2.GetOrdinal("id")) + 1;
                break;
            }
            sql.Close();

            try
            {
                string sortId = "";
                int bonus = 0;
                int point = 0;
                string sqlcommandStr = "";
                if (bigsortid != null)
                {
                    sortId = "'" + bigsortid + "'";
                }
                else
                {
                    sortId = "NULL";
                }

                if (content1 == "")
                {
                    content1 = "暫無簡介";
                }

                sqlcommandStr += "insert into product (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "pro_id," +
                    "big_sort_id," +
                    "pdname," +
                    "amount," +
                    "pic," +
                    "content1," +
                    "unit_price," +
                    "cost_price," +
                    "bonus," +
                    "point," +
                    "vm_tenant_id," +
                    "shelf_life_day," +
                    "isUse)";
                sqlcommandStr += " values(";
                sqlcommandStr += "@proid,"
                    + sortId + "," +
                    "@pdname," +
                    "@amount," +
                    "@filename," +
                    "@content1," +
                    "@unit," +
                    "@cost," +
                    "@bonus," +
                    "@point," +
                    "0," +
                    "@lifeday" +
                    ",1)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@pdname", pdname));
                command1.Parameters.Add(new SqlParameter("@amount", amount));
                //command1.Parameters.Add(new SqlParameter("@vmnum", vmnum));
                command1.Parameters.Add(new SqlParameter("@proid", pro_id));
                command1.Parameters.Add(new SqlParameter("@filename", filename));
                command1.Parameters.Add(new SqlParameter("@content1", content1));
                command1.Parameters.Add(new SqlParameter("@unit", unit_price));
                command1.Parameters.Add(new SqlParameter("@cost", cost_price));
                command1.Parameters.Add(new SqlParameter("@bonus", bonus));
                command1.Parameters.Add(new SqlParameter("@point", point));
                //command1.Parameters.Add(new SqlParameter("@vmuid", vmuid));
                command1.Parameters.Add(new SqlParameter("@lifeday", shelf_life_day));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool returnQty(string roadid,int uid,string qty)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            try {
                SqlCommand command = new SqlCommand("update vm_pro_qty set roadqty=@qty where roadid=@roadid and vm_uid=@id");
                command.Parameters.Add(new SqlParameter("@qty",qty));
                command.Parameters.Add(new SqlParameter("@roadid",roadid));
                command.Parameters.Add(new SqlParameter("@id",uid));
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public Mainproduct GetProductinfo(int id)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            Mainproduct mainproductInfo = new Mainproduct();

            SqlCommand command1 = new SqlCommand(@"select big_sort_id,pdname,shelf_life_day,cost_price,unit_price,amount,content1,pic from product where id=@id");
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    mainproductInfo.id = id;
                    mainproductInfo.pdname = reader1.GetString(reader1.GetOrdinal("pdname"));
                    mainproductInfo.unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                    mainproductInfo.content1 = reader1.IsDBNull(6) ? "暫無簡介" : reader1.GetString(reader1.GetOrdinal("content1"));
                    mainproductInfo.cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price"));
                    mainproductInfo.pic = reader1.GetString(reader1.GetOrdinal("pic"));
                    mainproductInfo.big_sort_id = reader1.IsDBNull(0) ? 0 : reader1.GetInt32(reader1.GetOrdinal("big_sort_id"));
                    mainproductInfo.amount = reader1.GetString(reader1.GetOrdinal("amount"));
                    mainproductInfo.shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day"));
                }
            }
            sql.Close();

            return mainproductInfo;
        }

        public Mainproduct GetProductinfo2(int id)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            Mainproduct mainproductInfo = new Mainproduct();

            SqlCommand command1 = new SqlCommand(@"select big_sort_id,pdname,shelf_life_day,cost_price,unit_price,amount,content1,pic from product where pro_id=@id");
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    mainproductInfo.id = id;
                    mainproductInfo.pdname = reader1.GetString(reader1.GetOrdinal("pdname"));
                    mainproductInfo.unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                    mainproductInfo.content1 = reader1.IsDBNull(6) ? "暫無簡介" : reader1.GetString(reader1.GetOrdinal("content1"));
                    mainproductInfo.cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price"));
                    mainproductInfo.pic = reader1.GetString(reader1.GetOrdinal("pic"));
                    mainproductInfo.big_sort_id = reader1.IsDBNull(0) ? 0 : reader1.GetInt32(reader1.GetOrdinal("big_sort_id"));
                    mainproductInfo.amount = reader1.GetString(reader1.GetOrdinal("amount"));
                    mainproductInfo.shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day"));
                }
            }
            sql.Close();

            return mainproductInfo;
        }

        public Mainproduct GetProductinfoC(int id)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            Mainproduct mainproductInfo = new Mainproduct();

            SqlCommand command1 = new SqlCommand(@"select big_sort_id,pdname,shelf_life_day,cost_price,unit_price,amount,content1,pic from product where id=@id");
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    mainproductInfo.id = id;
                    mainproductInfo.pdname = reader1.GetString(reader1.GetOrdinal("pdname"));
                    mainproductInfo.unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                    mainproductInfo.content1 = reader1.IsDBNull(6) ? "暫無簡介" : reader1.GetString(reader1.GetOrdinal("content1"));
                    mainproductInfo.cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price"));
                    mainproductInfo.pic = reader1.GetString(reader1.GetOrdinal("pic"));
                    mainproductInfo.big_sort_id = reader1.IsDBNull(0) ? 0 : reader1.GetInt32(reader1.GetOrdinal("big_sort_id"));
                    mainproductInfo.amount = reader1.GetString(reader1.GetOrdinal("amount"));
                    mainproductInfo.shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day"));
                }
            }
            sql.Close();

            return mainproductInfo;
        }

        public Mainproduct editProduct(int id,string bigsortid, string pdname, int unit_price, int cost_price, string content1, int amount, string shelf_life_day, string filename)
        {
            Mainproduct mainproductInfo = new Mainproduct();

            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                string sortId = "";
                int bonus = 0;
                int point = 0;
                string sqlcommandStr = "";
                if (bigsortid != null)
                {
                    sortId = "'" + bigsortid + "'";
                }
                else
                {
                    sortId = "NULL";
                }

                if (content1 == "")
                {
                    content1 = "暫無簡介";
                }

                sqlcommandStr += "update product set ";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "big_sort_id=" + sortId + ",pdname=@pdname," +
                    "amount=@amount," +
                    (filename == "" ? "" : "pic=@filename,") +
                    "content1=@content1," +
                    "unit_price=@unit," +
                    "cost_price=@cost," +
                    "bonus=@bonus," +
                    "point=@point," +
                    "shelf_life_day=@lifeday where id=@id";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@id",id));
                command1.Parameters.Add(new SqlParameter("@pdname", pdname));
                command1.Parameters.Add(new SqlParameter("@amount", amount));
                command1.Parameters.Add(new SqlParameter("@filename", filename));
                command1.Parameters.Add(new SqlParameter("@content1", content1));
                command1.Parameters.Add(new SqlParameter("@unit", unit_price));
                command1.Parameters.Add(new SqlParameter("@cost", cost_price));
                command1.Parameters.Add(new SqlParameter("@bonus", bonus));
                command1.Parameters.Add(new SqlParameter("@point", point));
                command1.Parameters.Add(new SqlParameter("@lifeday", shelf_life_day));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();

                SqlCommand command2 = new SqlCommand(@"select pro_id from product where id=@id");
                command1.Parameters.Add(new SqlParameter("@id", id));
                command1.Connection = sql;
                sql.Open();
                SqlDataReader reader1 = command1.ExecuteReader();
                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        mainproductInfo.id = id;
                        mainproductInfo.pdname = reader1.GetString(reader1.GetOrdinal("pdname"));
                        mainproductInfo.unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price"));
                        mainproductInfo.content1 = reader1.GetString(reader1.GetOrdinal("content1"));
                        mainproductInfo.cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price"));
                        mainproductInfo.pic = reader1.GetString(reader1.GetOrdinal("pic"));
                        mainproductInfo.big_sort_id = reader1.GetInt32(reader1.GetOrdinal("big_sort_id"));
                        mainproductInfo.amount = reader1.GetString(reader1.GetOrdinal("amount"));
                        mainproductInfo.shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day"));
                    }
                }
                sql.Close();
                return mainproductInfo;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool editProductC(int id, string bigsortid, string pdname, int unit_price, int cost_price, string content1, int amount, string shelf_life_day, string filename)
        {
            Mainproduct mainproductInfo = new Mainproduct();

            SqlConnection sql = new SqlConnection(sqlconn5);
            try
            {
                string sortId = "";
                int bonus = 0;
                int point = 0;
                string sqlcommandStr = "";
                if (bigsortid != null)
                {
                    sortId = "'" + bigsortid + "'";
                }
                else
                {
                    sortId = "NULL";
                }

                if (content1 == "")
                {
                    content1 = "暫無簡介";
                }

                sqlcommandStr += "update product set ";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "big_sort_id=" + sortId + ",pdname=@pdname," +
                    "amount=@amount," +
                    (filename == "" ? "" : "pic=@filename,") +
                    "content1=@content1," +
                    "unit_price=@unit," +
                    "cost_price=@cost," +
                    "bonus=@bonus," +
                    "point=@point," +
                    "shelf_life_day=@lifeday where id=@id";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@id", id));
                command1.Parameters.Add(new SqlParameter("@pdname", pdname));
                command1.Parameters.Add(new SqlParameter("@amount", amount));
                command1.Parameters.Add(new SqlParameter("@filename", filename));
                command1.Parameters.Add(new SqlParameter("@content1", content1));
                command1.Parameters.Add(new SqlParameter("@unit", unit_price));
                command1.Parameters.Add(new SqlParameter("@cost", cost_price));
                command1.Parameters.Add(new SqlParameter("@bonus", bonus));
                command1.Parameters.Add(new SqlParameter("@point", point));
                command1.Parameters.Add(new SqlParameter("@lifeday", shelf_life_day));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false ;
            }
        }

        public List<Product> getMachineProduct(string layer,int type)
        {
            
            SqlConnection sql = new SqlConnection(sqlconn);
            string commandStr = "";
            List<Product> products = new List<Product>();
            if (type == 1)
            {
                if (layer == "")
                {
                    commandStr = "select vmprid,roadid,pic,pdname,unit_price,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where road_act=1 order by roadid asc";
                }
                else
                {
                    commandStr = "select vmprid,roadid,pic,pdname,unit_price,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where layer_num=@layer and road_act=1 order by roadid asc";
                }

            }
            else
            {
                if (layer == "")
                {
                    commandStr = "select vmprid,roadid,pic,pdname,unit_price,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where road_act=1 order by roadid asc";
                }
                else
                {
                    commandStr = "select vmprid,roadid,pic,pdname,unit_price,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where road_layer=@layer and road_act=1 order by roadid asc";
                }
            }
            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@layer", layer));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Product product1 = new Product()
                    {
                        vmprid = reader1.GetInt32(reader1.GetOrdinal("vmprid")),
                        roadId = reader1.GetString(reader1.GetOrdinal("roadid")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        pdName = reader1.GetString(reader1.GetOrdinal("pdname")),
                        unitPrice = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        roadRty = reader1.GetString(reader1.GetOrdinal("roadqty")),
                        upData = reader1.GetString(reader1.GetOrdinal("up_data")),
                        shelfLifeDay = reader1.GetString(reader1.GetOrdinal("shelf_life_day")),
                        expiryDate = reader1.GetString(reader1.GetOrdinal("expiry_date")),
                        proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"))
                };

                    products.Add(product1);
                }
            }
            sql.Close();

            return products;

        }

        public bool Roadreset(string roadid,string date1,string date2,string time)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command2 = new SqlCommand(@"update vm_pro_qty set up_data=@date1,up_time=@time1,expiry_date=@date2 where roadid=@roadid");
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@roadid", roadid));
                command2.Parameters.Add(new SqlParameter("@date1",date1));
                command2.Parameters.Add(new SqlParameter("@time1",time));
                command2.Parameters.Add(new SqlParameter("@date2",date2));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool MachineProinsert(string vmnum,int vmuid,int product,string layer,string road,string road2,int proqty,int type,string mode)
        {
            SqlConnection sql = new SqlConnection(sqlconn);

            MachineLans machineLans = new MachineLans();

            MachineLasRepository lasRepository = new MachineLasRepository();

            machineLans = lasRepository.GetmachineLans(vmuid, vmnum, road);

            Mainproduct product1 = new Mainproduct();

            product1 = this.GetProductinfo(product);

            DateTime date1 = DateTime.Now;

            string commandStr = "";

            if(type == 1)
            {
                commandStr = "insert into vm_pro_qty (vm_num,layer_num,road_num,roadid,road_code,roadtyp,roadqty,pro_id,pdname,content1,pic,proqty,vm_mode,act_mode,vm_uid,unit_proce,cost_price,sp_act,road_act.up_data,up_time,up_repdate,shelf_life_day,expiry_date) values(@vmnum,@layer,@road,@roadid,@roadid,@typ,@qty,@proid,@pdname,@content1,@pic,@proqty,@mode,@act,@uid,@unit,@cost,0,1,@upData,@upTime,@data,@life,@expiry)";
            }
            else
            {
                commandStr = "insert into vm_pro_qty (vm_num,road_layer,roadid,road_code,roadtyp,roadqty,pro_id,pdname,content1,pic,proqty,vm_mode,act_mode,vm_uid,unit_price,cost_price,sp_act,road_act,up_data,up_time,up_repdate,shelf_life_day,expiry_date) values(@vmnum,@layer,@roadid,@roadid,@typ,@qty,@proid,@pdname,@content1,@pic,@proqty,@mode,@act,@uid,@unit,@cost,0,1,@upData,@upTime,@data,@life,@expiry)";
            }


            try
            {
                SqlCommand command2 = new SqlCommand(commandStr);
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@vmnum",vmnum));
                command2.Parameters.Add(new SqlParameter("@layer",layer));
                if (type == 1)
                {
                    command2.Parameters.Add(new SqlParameter("@road",machineLans.road_num));
                }
                command2.Parameters.Add(new SqlParameter("@roadid",road));
                command2.Parameters.Add(new SqlParameter("@typ",machineLans.roadtyp));
                command2.Parameters.Add(new SqlParameter("@qty",machineLans.roadqty));
                command2.Parameters.Add(new SqlParameter("@proid",product));
                command2.Parameters.Add(new SqlParameter("@pdname",product1.pdname));
                command2.Parameters.Add(new SqlParameter("@content1",product1.content1));
                command2.Parameters.Add(new SqlParameter("@pic",product1.pic));
                command2.Parameters.Add(new SqlParameter("@proqty", proqty));
                command2.Parameters.Add(new SqlParameter("@mode", mode));
                command2.Parameters.Add(new SqlParameter("@act",machineLans.act_mode));
                command2.Parameters.Add(new SqlParameter("@uid",vmuid));
                command2.Parameters.Add(new SqlParameter("@unit",product1.unit_price));
                command2.Parameters.Add(new SqlParameter("@cost",product1.cost_price));
                command2.Parameters.Add(new SqlParameter("@upData",date1.ToString("yyyy/MM/dd")));
                command2.Parameters.Add(new SqlParameter("@upTime",date1.ToString("HH:mm:ss")));
                command2.Parameters.Add(new SqlParameter("@data", date1.ToString("yyyyMMdd")));
                command2.Parameters.Add(new SqlParameter("@life",product1.shelf_life_day));
                command2.Parameters.Add(new SqlParameter("expiry",date1.AddDays(Convert.ToInt32(product1.shelf_life_day)).ToString("yyyy/MM/dd")));
                //command2.Parameters.Add(new SqlParameter("@mode",lasRepository.mode));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool MachineProedit(string vmnum, int vmuid, int product, string layer, string road, string road2, int proqty, int type, string mode,int vmprid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);

            MachineLans machineLans = new MachineLans();

            MachineLasRepository lasRepository = new MachineLasRepository();

            machineLans = lasRepository.GetmachineLans(vmuid, vmnum, road);

            Mainproduct product1 = new Mainproduct();

            product1 = this.GetProductinfo(product);

            DateTime date1 = DateTime.Now;

            string commandStr = "";

            if (type == 1)
            {
                commandStr = "update vm_pro_qty set vm_num=@vmnum,layer_num=@layer,road_num=@road,roadid=@roadid,road_code=@roadid,roadtyp=@typ,roadqty=@qty,pro_id=@proid,pdname=@pdname,content1=@content1,pic=@pic,proqty=@proqty,vm_mode=@mode,act_mode=@act,vm_uid=@uid,unit_price=@unit,cost_price=@cost,sp_act=0,road_act=1,up_data=@upData,up_time=@upTime,up_repdate=@data,shelf_life_day=@life,expiry_date=@expiry where vmprid=@vmprid";
            }
            else
            {
                commandStr = "update vm_pro_qty set vm_num=@vmnum,road_layer=@layer,roadid=@roadid,road_code=@roadid,roadtyp=@typ,roadqty=@qty,pro_id=@proid,pdname=@pdname,content1=@content1,pic=@pic,proqty=@proqty,vm_mode=@mode,act_mode=@act,vm_uid=@uid,unit_price=@unit,cost_price=@cost,sp_act=0,road_act=1,up_data=@upData,up_time=@upTime,up_repdate=@data,shelf_life_day=@life,expiry_date=@expiry where vmprid=@vmprid";
            }

            try
            {
                SqlCommand command2 = new SqlCommand(commandStr);
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@vmnum", vmnum));
                command2.Parameters.Add(new SqlParameter("@layer", layer));
                if (type == 1)
                {
                    command2.Parameters.Add(new SqlParameter("@road", machineLans.road_num));
                }
                command2.Parameters.Add(new SqlParameter("@roadid", road));
                command2.Parameters.Add(new SqlParameter("@typ", machineLans.roadtyp));
                command2.Parameters.Add(new SqlParameter("@qty", machineLans.roadqty));
                command2.Parameters.Add(new SqlParameter("@proid", product));
                command2.Parameters.Add(new SqlParameter("@pdname", product1.pdname));
                command2.Parameters.Add(new SqlParameter("@content1", product1.content1));
                command2.Parameters.Add(new SqlParameter("@pic", product1.pic));
                command2.Parameters.Add(new SqlParameter("@proqty", proqty));
                command2.Parameters.Add(new SqlParameter("@mode", mode));
                command2.Parameters.Add(new SqlParameter("@act", machineLans.act_mode));
                command2.Parameters.Add(new SqlParameter("@uid", vmuid));
                command2.Parameters.Add(new SqlParameter("@unit", product1.unit_price));
                command2.Parameters.Add(new SqlParameter("@cost", product1.cost_price));
                command2.Parameters.Add(new SqlParameter("@upData", date1.ToString("yyyy/MM/dd")));
                command2.Parameters.Add(new SqlParameter("@upTime", date1.ToString("HH:mm:ss")));
                command2.Parameters.Add(new SqlParameter("@data", date1.ToString("yyyyMMdd")));
                command2.Parameters.Add(new SqlParameter("@life", product1.shelf_life_day));
                command2.Parameters.Add(new SqlParameter("expiry", date1.AddDays(Convert.ToInt32(product1.shelf_life_day)).ToString("yyyy/MM/dd")));
                command2.Parameters.Add(new SqlParameter("@vmprid",vmprid));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool groupProduct(int id, int product)
        {
            SqlConnection sql = new SqlConnection(sqlconn);

            Mainproduct product1 = new Mainproduct();

            product1 = this.GetProductinfo2(product);

            DateTime date1 = DateTime.Now;

            string commandStr = "";
                
            commandStr = "update vm_pro_qty set pro_id=@proid,pdname=@pdname,content1=@content1,pic=@pic,unit_price=@unit,cost_price=@cost,up_data=@upData,up_time=@upTime,up_repdate=@data,shelf_life_day=@life,expiry_date=@expiry where vmprid=@vmprid";

            try
            {
                SqlCommand command2 = new SqlCommand(commandStr);
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@proid", product));
                command2.Parameters.Add(new SqlParameter("@pdname", product1.pdname));
                command2.Parameters.Add(new SqlParameter("@content1", product1.content1));
                command2.Parameters.Add(new SqlParameter("@pic", product1.pic));
                command2.Parameters.Add(new SqlParameter("@unit", product1.unit_price));
                command2.Parameters.Add(new SqlParameter("@cost", product1.cost_price));
                command2.Parameters.Add(new SqlParameter("@upData", date1.ToString("yyyy/MM/dd")));
                command2.Parameters.Add(new SqlParameter("@upTime", date1.ToString("HH:mm:ss")));
                command2.Parameters.Add(new SqlParameter("@data", date1.ToString("yyyyMMdd")));
                command2.Parameters.Add(new SqlParameter("@life", product1.shelf_life_day));
                command2.Parameters.Add(new SqlParameter("expiry", date1.AddDays(Convert.ToInt32(product1.shelf_life_day)).ToString("yyyy/MM/dd")));
                command2.Parameters.Add(new SqlParameter("@vmprid", id));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        public Product GetMainproductinfo(int id,int type)
        {
            Product products = new Product();
            SqlConnection sql = new SqlConnection(sqlconn);
            string str = "";
            if (type == 1)
            {
                str = "select road_num,layer_num,roadid,pro_id,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where vmprid = @id";
            }
            else
            {
                str = "select road_layer,roadid,pro_id,roadqty,up_data,shelf_life_day,expiry_date,proqty from vm_pro_qty where vmprid = @id";
            }

            SqlCommand command1 = new SqlCommand(str);
            command1.Parameters.Add(new SqlParameter("@id",id));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    if(type==1)
                    {
                        products.roadNum = reader1.GetString(reader1.GetOrdinal("road_num"));
                        products.layerNum = reader1.GetInt32(reader1.GetOrdinal("layer_num"));
                        products.roadId = reader1.GetString(reader1.GetOrdinal("roadid"));
                        products.proId = reader1.GetInt32(reader1.GetOrdinal("pro_id"));
                        products.proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                    }
                    else
                    {
                        products.roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer"));
                        products.roadId = reader1.GetString(reader1.GetOrdinal("roadid"));
                        products.proId = reader1.GetInt32(reader1.GetOrdinal("pro_id"));
                        products.proQty = reader1.GetInt32(reader1.GetOrdinal("proqty"));
                    }
                }
            }
            sql.Close();

            return products;
        }

        public bool deleteProduct(int id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command1 = new SqlCommand(@"update product set isUse=0 where id=@id");
                command1.Parameters.Add(new SqlParameter("@id", id));
                command1.Connection = sql1;
                sql1.Open();
                command1.ExecuteNonQuery();
                sql1.Close();

                /*SqlCommand command2 = new SqlCommand(@"delete product where id=@id");
                command2.Parameters.Add(new SqlParameter("@id", id));
                command2.Connection = sql2;
                sql2.Open();
                command2.ExecuteNonQuery();
                sql2.Close();*/
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool deleteProductLocal(int id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command1 = new SqlCommand(@"update product set isUse=0 where id=@id");
                command1.Parameters.Add(new SqlParameter("@id", id));
                command1.Connection = sql2;
                sql2.Open();
                command1.ExecuteNonQuery();
                sql2.Close();

                /*SqlCommand command2 = new SqlCommand(@"delete product where id=@id");
                command2.Parameters.Add(new SqlParameter("@id", id));
                command2.Connection = sql2;
                sql2.Open();
                command2.ExecuteNonQuery();
                sql2.Close();*/
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Fillup(int uid,string date1,string time1,string date2,int number)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command1 = new SqlCommand(@"update vm_pro_qty setup_data=@upData,up_time=@upTime,expiry_date=@expiry,shelf_life_day=@number where vm_uid=@uid");
                command1.Parameters.Add(new SqlParameter("@number",number));
                command1.Parameters.Add(new SqlParameter("@upData",date1));
                command1.Parameters.Add(new SqlParameter("@upTime",time1));
                command1.Parameters.Add(new SqlParameter("@expiry",date2));
                command1.Parameters.Add(new SqlParameter("@uid", uid));
                command1.Connection = sql2;
                sql2.Open();
                command1.ExecuteNonQuery();
                sql2.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool cloudToLocal(string vm_num,int uid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);
            //DataTable dt = getsrc(getCloudData(vm_num,uid));//取來源
          
            try {
                   //BulkCopy(dt);//寫入
                /*SqlCommand command2 = new SqlCommand(@"insert into product (pro_id,big_sort_id,pdname,amount,vm_num,pic,unit_price,cost_price,bonus,point,content1,vm_uid,vm_tenant_id,shelf_life_day)" +
                    " values(@proid,@sort,@pdname,@amount,@vmnum,@pic,@unit,@cost,@bonus,@point,@content,@uid,@tenant,@life)");
                foreach(Mainproduct mainproduct in mainproducts)
                {
                    command2.Parameters.Add(new SqlParameter("@proid", mainproduct.pro_id));
                    command2.Parameters.Add(new SqlParameter("@sort", mainproduct.big_sort_id));
                    command2.Parameters.Add(new SqlParameter("@pdname", mainproduct.pdname));
                    command2.Parameters.Add(new SqlParameter("@amount", mainproduct.amount));
                    command2.Parameters.Add(new SqlParameter("@vmnum", vm_num));
                    command2.Parameters.Add(new SqlParameter("@pic",mainproduct.pic));
                    command2.Parameters.Add(new SqlParameter("@unit",mainproduct.unit_price));
                    command2.Parameters.Add(new SqlParameter("@cost",mainproduct.cost_price));
                    command2.Parameters.Add(new SqlParameter("@bonus",'0'));
                    command2.Parameters.Add(new SqlParameter("@point", '0'));
                    command2.Parameters.Add(new SqlParameter("@content", mainproduct.content1));
                    command2.Parameters.Add(new SqlParameter("@uid", uid));
                    command2.Parameters.Add(new SqlParameter("@tenant", '0'));
                    command2.Parameters.Add(new SqlParameter("@life",mainproduct.shelf_life_day));
                    command2.Connection = sql2;
                    sql2.Open();
                    command2.ExecuteNonQuery();
                    sql2.Close();*/
                //}
               
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool getClouddata(string vmnum,int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);

            Mainproduct mainproductInfo = new Mainproduct();
            List<Mainproduct> mainproducts = new List<Mainproduct>();
            SqlCommand command1 = new SqlCommand(@"select big_sort_id,pdname,shelf_life_day,cost_price,unit_price,amount,content1,pic,product.pro_id as Cproid,bonus,point,vm_tenant_id,machine_product.isUse as useStatus from product inner join machine_product on product.pro_id=machine_product.pro_id where product.pro_id in (select pro_id from machine_product where vm_uid=@id)");
            command1.Parameters.Add(new SqlParameter("@id",vmuid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Mainproduct mainproduct = new Mainproduct()
                    {
                        pdname = reader1.GetString(reader1.GetOrdinal("pdname")),
                        unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        content1 = reader1.IsDBNull(6) ? "暫無簡介" : reader1.GetString(reader1.GetOrdinal("content1")),
                        cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        big_sort_id = reader1.IsDBNull(0) ? 0 : reader1.GetInt32(reader1.GetOrdinal("big_sort_id")),
                        amount = reader1.GetString(reader1.GetOrdinal("amount")),
                        shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day")),
                        pro_id = reader1.GetInt32(reader1.GetOrdinal("Cproid")),
                        bonus = reader1.GetString(reader1.GetOrdinal("bonus")),
                        point = reader1.GetString(reader1.GetOrdinal("point")),
                        vm_tenant_id = reader1.GetString(reader1.GetOrdinal("vm_tenant_id")),
                        isUse=reader1.GetInt32(reader1.GetOrdinal("useStatus"))
                    };

                    if (checkLocal(mainproduct.pro_id))
                    {
                        if (mainproduct.isUse == 1)
                        {
                            updateData(mainproduct,mainproduct.pro_id);
                            //continue;
                        }
                        else
                        {
                            deleteData(mainproduct.pro_id);
                            //continue;
                        }
                    }
                    else
                    {
                        if (insertData(mainproduct, vmnum, vmuid))
                        {
                            //continue;
                        }
                    }
                }
            }
            sql.Close();

            return true;
        }

        public bool updateData(Mainproduct mainproduct,int proid)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            try
            {
                SqlCommand command3 = new SqlCommand(@"update product set big_sort_id=@sort,pdname=@pdname,amount=@amount," +
                           "pic=@pic,unit_price=@unit,cost_price=@cost,bonus=@bonus,@point=@point,content1=@content," +
                           "vm_tenant_id=@tenant,shelf_life_day=@life where pro_id=@proid");
                command3.Parameters.Add(new SqlParameter("@proid", mainproduct.pro_id));
                command3.Parameters.Add(new SqlParameter("@sort", mainproduct.big_sort_id));
                command3.Parameters.Add(new SqlParameter("@pdname", mainproduct.pdname));
                command3.Parameters.Add(new SqlParameter("@amount", mainproduct.amount));
                //command3.Parameters.Add(new SqlParameter("@vmnum", vmnum));
                command3.Parameters.Add(new SqlParameter("@pic", mainproduct.pic));
                command3.Parameters.Add(new SqlParameter("@unit", mainproduct.unit_price));
                command3.Parameters.Add(new SqlParameter("@cost", mainproduct.cost_price));
                command3.Parameters.Add(new SqlParameter("@bonus", '0'));
                command3.Parameters.Add(new SqlParameter("@point", '0'));
                command3.Parameters.Add(new SqlParameter("@content", mainproduct.content1));
                //command3.Parameters.Add(new SqlParameter("@uid", uid));
                command3.Parameters.Add(new SqlParameter("@tenant", '0'));
                command3.Parameters.Add(new SqlParameter("@life", mainproduct.shelf_life_day));
                command3.Connection = sql2;
                sql2.Open();
                command3.ExecuteNonQuery();
                sql2.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool deleteData(int proid)
        {
            try
            {
                SqlConnection sql3 = new SqlConnection(sqlconn);
                SqlCommand command3 = new SqlCommand(@"delete product where pro_id=@proid");
                command3.Parameters.Add(new SqlParameter("@proid", proid));
                command3.Connection = sql3;
                sql3.Open();
                command3.ExecuteNonQuery();
                sql3.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool checkLocal(int proid)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            bool check = false;
            SqlCommand command1 = new SqlCommand(@"select * from product where pro_id=@id");
            command1.Parameters.Add(new SqlParameter("@id",proid));
            command1.Connection = sql2;
            sql2.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }            

        public bool insertData(Mainproduct mainproduct,string vmnum,int uid)
        {
            try {
                SqlConnection sql5 = new SqlConnection(sqlconn);
                SqlCommand command2 = new SqlCommand(@"insert into product(pro_id,big_sort_id,pdname,amount,vm_num,pic,unit_price,cost_price,bonus,point,content1,vm_uid,vm_tenant_id,shelf_life_day)"+
                    " values(@proid,@sort,@pdname,@amount,@vmnum,@pic,@unit,@cost,@bonus,@point,@content,@uid,@tenant,@life)");
                command2.Parameters.Add(new SqlParameter("@proid", mainproduct.pro_id));
                command2.Parameters.Add(new SqlParameter("@sort", mainproduct.big_sort_id));
                command2.Parameters.Add(new SqlParameter("@pdname", mainproduct.pdname));
                command2.Parameters.Add(new SqlParameter("@amount", mainproduct.amount));
                command2.Parameters.Add(new SqlParameter("@vmnum", vmnum));
                command2.Parameters.Add(new SqlParameter("@pic", mainproduct.pic));
                command2.Parameters.Add(new SqlParameter("@unit", mainproduct.unit_price));
                command2.Parameters.Add(new SqlParameter("@cost", mainproduct.cost_price));
                command2.Parameters.Add(new SqlParameter("@bonus", '0'));
                command2.Parameters.Add(new SqlParameter("@point", '0'));
                command2.Parameters.Add(new SqlParameter("@content", mainproduct.content1));
                command2.Parameters.Add(new SqlParameter("@uid", uid));
                command2.Parameters.Add(new SqlParameter("@tenant", '0'));
                command2.Parameters.Add(new SqlParameter("@life", mainproduct.shelf_life_day));
                command2.Connection = sql5;
                sql5.Open();
                command2.ExecuteNonQuery();
                sql5.Close();

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        //雲端貨道下載
        public bool getCloudRoadinfo(string vmnum, int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);

            //Mainproduct mainproductInfo = new Mainproduct();
            List<Product> products = new List<Product>();
            SqlCommand command1 = new SqlCommand(@"select road_layer,roadid,layer_num,road_num,road_code,roadtyp,roadqty,pro_id,pdname,content1,pic,proqty,vm_mode,act_mode,vm_uid,vm_tenant_id,"+
                       "unit_price,cost_price,bonus,point,sp_act,road_act,up_data,up_time,up_repdate,Shelf_life_day,expiry_date from vm_pro_qty where vm_num=@num and vm_uid=@uid");
            command1.Parameters.Add(new SqlParameter("@num", vmnum));
            command1.Parameters.Add(new SqlParameter("@uid",vmuid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    if (reader1.IsDBNull(2))
                    {
                        Product product = new Product()
                        {
                            roadLayer=reader1.GetInt32(reader1.GetOrdinal("road_layer")),
                            roadId=reader1.GetString(reader1.GetOrdinal("roadid")),
                            roadCode=reader1.GetString(reader1.GetOrdinal("road_code")),
                            roadtype=reader1.GetString(reader1.GetOrdinal("roadtyp")),
                            roadRty=reader1.GetString(reader1.GetOrdinal("roadqty")),
                            proId=reader1.GetInt32(reader1.GetOrdinal("pro_id")),
                            pdName=reader1.GetString(reader1.GetOrdinal("pdname")),
                            content=reader1.GetString(reader1.GetOrdinal("content1")),
                            pic=reader1.GetString(reader1.GetOrdinal("pic")),
                            proQty=reader1.GetInt32(reader1.GetOrdinal("proqty")),
                            vmMode=reader1.GetString(reader1.GetOrdinal("vm_mode")),
                            actMode=reader1.GetString(reader1.GetOrdinal("act_mode")),
                            vmUid=reader1.GetInt32(reader1.GetOrdinal("vm_uid")),
                            vmTenantId=reader1.GetString(reader1.GetOrdinal("vm_tenant_id")),
                            unitPrice=reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                            costPrice=reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                            bonus=reader1.GetString(reader1.GetOrdinal("bonus")),
                            point=reader1.GetString(reader1.GetOrdinal("point")),
                            spAct=reader1.GetInt32(reader1.GetOrdinal("sp_act")),
                            roadAct=reader1.GetInt32(reader1.GetOrdinal("road_act")),
                            upData=reader1.GetString(reader1.GetOrdinal("up_data")),
                            upTime=reader1.GetString(reader1.GetOrdinal("up_time")),
                            upRepdate=reader1.GetString(reader1.GetOrdinal("up_repdate")),
                            shelfLifeDay=reader1.GetString(reader1.GetOrdinal("Shelf_life_day")),
                            expiryDate=reader1.GetString(reader1.GetOrdinal("expiry_date"))
                        };

                        products.Add(product);
                    }
                    else
                    {
                        Product product = new Product()
                        {
                            roadLayer = reader1.GetInt32(reader1.GetOrdinal("road_layer")),
                            roadId = reader1.GetString(reader1.GetOrdinal("roadid")),
                            layerNum = reader1.GetInt32(reader1.GetOrdinal("layer_num")),
                            roadNum = reader1.GetString(reader1.GetOrdinal("road_num")),
                            roadCode=reader1.GetString(reader1.GetOrdinal("road_code")),
                            roadRty = reader1.GetString(reader1.GetOrdinal("roadqty")),
                            proId = reader1.GetInt32(reader1.GetOrdinal("pro_id")),
                            pdName = reader1.GetString(reader1.GetOrdinal("pdname")),
                            content = reader1.GetString(reader1.GetOrdinal("content1")),
                            pic = reader1.GetString(reader1.GetOrdinal("pic")),
                            proQty = reader1.GetInt32(reader1.GetOrdinal("proqty")),
                            vmMode = reader1.GetString(reader1.GetOrdinal("vm_mode")),
                            actMode = reader1.GetString(reader1.GetOrdinal("act_mode")),
                            vmUid = reader1.GetInt32(reader1.GetOrdinal("vm_uid")),
                            vmTenantId = reader1.GetString(reader1.GetOrdinal("vm_tenant_id")),
                            unitPrice = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                            costPrice = reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                            bonus = reader1.GetString(reader1.GetOrdinal("bonus")),
                            point = reader1.GetString(reader1.GetOrdinal("point")),
                            spAct = reader1.GetInt32(reader1.GetOrdinal("sp_act")),
                            roadAct = reader1.GetInt32(reader1.GetOrdinal("road_act")),
                            upData = reader1.GetString(reader1.GetOrdinal("up_data")),
                            upTime = reader1.GetString(reader1.GetOrdinal("up_time")),
                            upRepdate = reader1.GetString(reader1.GetOrdinal("up_repdate")),
                            shelfLifeDay = reader1.GetString(reader1.GetOrdinal("Shelf_life_day")),
                            expiryDate = reader1.GetString(reader1.GetOrdinal("expiry_date"))
                        };

                        products.Add(product);
                    }
                }
            }
            sql.Close();

            foreach(Product product1 in products)
            {
                if (checkLocalRoad(product1.roadId))
                {
                    updateRoadData(product1, product1.roadId);

                }
                else
                {
                    if (insertRoadData(product1, vmnum, vmuid))
                    {
                        //continue;
                    }
                }
            }

            return true;
        }

        public bool updateRoadData(Product product, string roadid)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            string commandStr="";
            try
            {
                if (Convert.ToString(product.layerNum) == "0")
                {
                    commandStr += "update vm_pro_qty set road_layer=@layer,roadid=@rid,road_code=@rcode,roadtyp=@rtype,roadqty=@rqty,pro_id=@proid,pdname=@pdname,content1=@content,pic=@pic,";
                    commandStr += "proqty=@pqty,vm_mode=@vmode,act_mode=@act,vm_uid=@uid,vm_tenant_id=@tenant,unit_price=@unit,cost_price=@cost,bonus=@bonus,point=@point,sp_act=@sp,road_act=@ract,";
                    commandStr += "up_data =@upd,up_time=@upt,up_repdate=@upr,Shelf_life_day=@life,expiry_date=@expiry where roadid=@rid";
                }
                else
                {
                    commandStr += "update vm_pro_qty set layer_num=@layernum,roadid=@rid,road_num=@roadnum,road_code=@rcode,roadtyp=@rtype,roadqty=@rqty,pro_id=@proid,pdname=@pdname,content1=@content,pic=@pic,";
                    commandStr += "proqty=@pqty,vm_mode=@vmode,act_mode=@act,vm_uid=@uid,vm_tenant_id=@tenant,unit_price=@unit,cost_price=@cost,bonus=@bonus,point=@point,sp_act=@sp,road_act=@ract,";
                    commandStr += "up_data =@upd,up_time=@upt,up_repdate=@upr,Shelf_life_day=@life,expiry_date=@expiry where roadid=@rid";
                }
                SqlCommand command3 = new SqlCommand(commandStr);
                command3.Parameters.Add(new SqlParameter("@layer",product.roadLayer));
                command3.Parameters.Add(new SqlParameter("@rid", product.roadId));
                if (Convert.ToString(product.layerNum) != "0")
                {
                    command3.Parameters.Add(new SqlParameter("@layernum", product.layerNum));
                    command3.Parameters.Add(new SqlParameter("@roadnum", product.roadNum));
                }
                command3.Parameters.Add(new SqlParameter("@rcode", product.roadCode));
                command3.Parameters.Add(new SqlParameter("@rtype", product.roadtype));
                command3.Parameters.Add(new SqlParameter("@rqty", product.roadRty));
                command3.Parameters.Add(new SqlParameter("@proid", product.proId));
                command3.Parameters.Add(new SqlParameter("@pdname", product.pdName));
                command3.Parameters.Add(new SqlParameter("@pic", product.pic));
                command3.Parameters.Add(new SqlParameter("@pqty", product.proQty));
                command3.Parameters.Add(new SqlParameter("@vmode", product.vmMode));
                command3.Parameters.Add(new SqlParameter("@act", product.actMode));
                command3.Parameters.Add(new SqlParameter("@uid", product.vmUid));
                command3.Parameters.Add(new SqlParameter("@tenant", product.vmTenantId));
                command3.Parameters.Add(new SqlParameter("@unit", product.unitPrice));
                command3.Parameters.Add(new SqlParameter("@cost", product.costPrice));
                command3.Parameters.Add(new SqlParameter("@bonus", product.bonus));
                command3.Parameters.Add(new SqlParameter("@point",product.point));
                command3.Parameters.Add(new SqlParameter("@sp", product.spAct));
                command3.Parameters.Add(new SqlParameter("@ract", product.roadAct));
                command3.Parameters.Add(new SqlParameter("@upd",product.upData));
                command3.Parameters.Add(new SqlParameter("@upt",product.upTime));
                command3.Parameters.Add(new SqlParameter("@upr",product.upRepdate));
                command3.Parameters.Add(new SqlParameter("@content", product.content));
                command3.Parameters.Add(new SqlParameter("@life", product.shelfLifeDay));
                command3.Parameters.Add(new SqlParameter("@expiry",product.expiryDate));
                command3.Connection = sql2;
                sql2.Open();
                command3.ExecuteNonQuery();
                sql2.Close();

                if (product.roadAct == 0)
                {
                    SqlCommand command11 = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id");
                    command11.Parameters.Add(new SqlParameter("@act", "0"));
                    command11.Parameters.Add(new SqlParameter("@act2", "2"));
                    command11.Parameters.Add(new SqlParameter("@id", roadid));
                    command11.Connection = sql2;
                    sql2.Open();
                    command11.ExecuteNonQuery();
                    sql2.Close();
                }
                else if(product.roadAct == 1)
                {
                    SqlCommand command11 = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id");
                    command11.Parameters.Add(new SqlParameter("@act", "1"));
                    command11.Parameters.Add(new SqlParameter("@act2", "1"));
                    command11.Parameters.Add(new SqlParameter("@id", roadid));
                    command11.Connection = sql2;
                    sql2.Open();
                    command11.ExecuteNonQuery();
                    sql2.Close();

                }
                else
                {
                    return false;
                }


                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool checkLocalRoad(string roadid)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            bool check = false;
            SqlCommand command1 = new SqlCommand(@"select * from vm_pro_qty where roadid=@id");
            command1.Parameters.Add(new SqlParameter("@id", roadid));
            command1.Connection = sql2;
            sql2.Open();
            SqlDataReader reader2 = command1.ExecuteReader();
            if (reader2.HasRows)
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }

        public bool insertRoadData(Product product, string vmnum, int uid)
        {
            SqlConnection sql5 = new SqlConnection(sqlconn);
            string commandStr = "";
            try
            {
                if (Convert.ToInt32(product.layerNum) == 0)
                {
                    //commandStr += "update vm_pro_qty set road_layer=@layer,roadid=@rid,road_code=@rcode,roadtyp=@rtype,roadqty=@rqty,pro_id=@proid,pdname=@pdname,content1=@content,pic=@pic,";
                    //commandStr += "proqty=@pqty,vm_mode=@vmode,act_mode=@act,vm_uid=@uid,vm_tenant_id=@tenant,unit_price=@unit,cost_price=@cost,bonus=@bonus,point=@point,sp_act=@sp,@road_act=@ract,";
                    //commandStr += "up_data=@upd,up_time=@upt,up_repdate=@upr,Shelf_life_day=@life,expiry_date=@expiry";
                    commandStr += "insert into vm_pro_qty (road_layer,roadid,road_code,roadtyp,roadqty,pro_id,pdname,content1,pic,proqty,vm_mode,act_mode,vm_uid,vm_tenant_id,unit_price,cost_price,bonus,point,";
                    commandStr += "sp_act,road_act,up_data,up_time,up_repdate,Shelf_life_day,expiry_date) values(@layer,@rid,@rcode,@rtype,@rqty,@proid,@pdname,@content,@pic,@pqty,@vmode,@act,@uid,@tenant,";
                    commandStr += "@unit,@cost,@bonus,@point,@sp,@ract,@upd,@upt,@upr,@life,@expiry)";
                }
                else
                {
                    //commandStr += "update vm_pro_qty set layer_num=@layernum,roadid=@rid,road_num=@roadnum,road_code=@rcode,roadtyp=@rtype,roadqty=@rqty,pro_id=@proid,pdname=@pdname,content1=@content,pic=@pic,";
                    //commandStr += "proqty=@pqty,vm_mode=@vmode,act_mode=@act,vm_uid=@uid,vm_tenant_id=@tenant,unit_price=@unit,cost_price=@cost,bonus=@bonus,point=@point,sp_act=@sp,@road_act=@ract,";
                    //commandStr += "up_data =@upd,up_time=@upt,up_repdate=@upr,Shelf_life_day=@life,expiry_date=@expiry";
                    commandStr += "insert into vm_pro_qty (layer_num,road_num,roadid,road_code,roadtyp,roadqty,pro_id,pdname,content1,pic,proqty,vm_mode,act_mode,vm_uid,vm_tenant_id,unit_price,cost_price,bonus,point,";
                    commandStr += "sp_act,road_act,up_data,up_time,up_repdate,Shelf_life_day,expiry_date) values(@layernum,@roadnum,@rid,@rcode,@rtype,@rqty,@proid,@pdname,@content,@pic,@pqty,@vmode,@act,@uid,@tenant,";
                    commandStr += "@unit,@cost,@bonus,@point,@sp,@ract,@upd,@upt,@upr,@life,@expiry)";
                }
                SqlCommand command5 = new SqlCommand(commandStr);
                command5.Parameters.Add(new SqlParameter("@layer", product.roadLayer));
                command5.Parameters.Add(new SqlParameter("@rid", product.roadId));
                if (Convert.ToInt32(product.layerNum) != 0)
                { 
                    command5.Parameters.Add(new SqlParameter("@layernum", product.layerNum));
                    command5.Parameters.Add(new SqlParameter("@roadnum", product.roadNum));
                }
                command5.Parameters.Add(new SqlParameter("@rcode", product.roadCode));
                command5.Parameters.Add(new SqlParameter("@rtype", product.roadtype));
                command5.Parameters.Add(new SqlParameter("@rqty", product.roadRty));
                command5.Parameters.Add(new SqlParameter("@proid", product.proId));
                command5.Parameters.Add(new SqlParameter("@pdname", product.pdName));
                command5.Parameters.Add(new SqlParameter("@pic", product.pic));
                command5.Parameters.Add(new SqlParameter("@pqty", product.proQty));
                command5.Parameters.Add(new SqlParameter("@vmode", product.vmMode));
                command5.Parameters.Add(new SqlParameter("@act", product.actMode));
                command5.Parameters.Add(new SqlParameter("@uid", product.vmUid));
                command5.Parameters.Add(new SqlParameter("@tenant", product.vmTenantId));
                command5.Parameters.Add(new SqlParameter("@unit", product.unitPrice));
                command5.Parameters.Add(new SqlParameter("@cost", product.costPrice));
                command5.Parameters.Add(new SqlParameter("@bonus", product.bonus));
                command5.Parameters.Add(new SqlParameter("@point", product.point));
                command5.Parameters.Add(new SqlParameter("@sp", product.spAct));
                command5.Parameters.Add(new SqlParameter("@ract", product.roadAct));
                command5.Parameters.Add(new SqlParameter("@upd", product.upData));
                command5.Parameters.Add(new SqlParameter("@upt", product.upTime));
                command5.Parameters.Add(new SqlParameter("@upr", product.upRepdate));
                command5.Parameters.Add(new SqlParameter("@content", product.content));
                command5.Parameters.Add(new SqlParameter("@life", product.shelfLifeDay));
                command5.Parameters.Add(new SqlParameter("@expiry", product.expiryDate));
                command5.Connection = sql5;
                sql5.Open();
                command5.ExecuteNonQuery();
                sql5.Close();

                if (product.roadAct == 0)
                {
                    SqlCommand command11 = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id");
                    command11.Parameters.Add(new SqlParameter("@act", "0"));
                    command11.Parameters.Add(new SqlParameter("@act2", "2"));
                    command11.Parameters.Add(new SqlParameter("@id", product.roadId));
                    command11.Connection = sql5;
                    sql5.Open();
                    command11.ExecuteNonQuery();
                    sql5.Close();
                }
                else if (product.roadAct == 1)
                {
                    SqlCommand command11 = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id");
                    command11.Parameters.Add(new SqlParameter("@act", "1"));
                    command11.Parameters.Add(new SqlParameter("@act2", "1"));
                    command11.Parameters.Add(new SqlParameter("@id", product.roadId));
                    command11.Connection = sql5;
                    sql5.Open();
                    command11.ExecuteNonQuery();
                    sql5.Close();

                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /*public bool checkLocal(int proid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);
            bool check = false;

            Mainproduct mainproductInfo = new Mainproduct();
            List<Mainproduct> mainproducts = new List<Mainproduct>();
            SqlCommand command1 = new SqlCommand(@"select * from product where pro_id=@id");
            command1.Parameters.Add(new SqlParameter("@id", proid));
            command1.Connection = sql2;
            sql2.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            sql2.Close();

            return check;
        }

        public List<Mainproduct> getCloudData(string vmnum,int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            SqlConnection sql2 = new SqlConnection(sqlconn);

            Mainproduct mainproductInfo = new Mainproduct();
            List<Mainproduct> mainproducts = new List<Mainproduct>();
            SqlCommand command1 = new SqlCommand(@"select big_sort_id,pdname,shelf_life_day,cost_price,unit_price,amount,content1,pic,pro_id,bonus,point,vm_tebant_id from product where pro_id in (select pro_id from machine_product where vm_uid=@id and isUse=1)");
            command1.Parameters.Add(new SqlParameter("@id", vmuid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Mainproduct mainproduct = new Mainproduct()
                    {
                        pdname = reader1.GetString(reader1.GetOrdinal("pdname")),
                        unit_price = reader1.GetInt32(reader1.GetOrdinal("unit_price")),
                        content1 = reader1.IsDBNull(6) ? "暫無簡介" : reader1.GetString(reader1.GetOrdinal("content1")),
                        cost_price = reader1.GetInt32(reader1.GetOrdinal("cost_price")),
                        pic = reader1.GetString(reader1.GetOrdinal("pic")),
                        big_sort_id = reader1.IsDBNull(0) ? 0 : reader1.GetInt32(reader1.GetOrdinal("big_sort_id")),
                        amount = reader1.GetString(reader1.GetOrdinal("amount")),
                        shelf_life_day = reader1.GetString(reader1.GetOrdinal("shelf_life_day")),
                        pro_id = reader1.GetInt32(reader1.GetOrdinal("pro_id")),
                        bonus = reader1.GetString(reader1.GetOrdinal("bonus")),
                        point = reader1.GetString(reader1.GetOrdinal("point")),
                        vm_tenant_id=reader1.GetString(reader1.GetOrdinal("vm_tenant_id"))
                    };
                    mainproducts.Add(mainproduct);
                }
            }
            sql.Close();

            return mainproducts;
        }

        public DataTable getsrc(List<Mainproduct> list)
        {
            var tb = new DataTable(typeof(Mainproduct).Name);

            PropertyInfo[] props = typeof(Mainproduct).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }

            foreach (Mainproduct item in list)
            {
                var values = new object[props.Length];

                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }

            return tb;
        }

        /// <summary>
        /// Determine of specified type is nullable
        /// </summary>
        public static bool IsNullable(Type t)
        {
            return !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Return underlying type if type is Nullable otherwise return the type
        /// </summary>
        public static Type GetCoreType(Type t)
        {
            if (t != null && IsNullable(t))
            {
                if (!t.IsValueType)
                {
                    return t;
                }
                else
                {
                    return Nullable.GetUnderlyingType(t);
                }
            }
            else
            {
                return t;
            }
        }

        private void BulkCopy(System.Data.DataTable dt)
        {
            //  整批轉入
            string sConn = sqlconn ;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                using (SqlBulkCopy sqlBC = new SqlBulkCopy(conn))
                {
                    //設定一個批次量寫入多少筆資料
                    sqlBC.BatchSize = 1000;

                    //設定逾時的秒數
                    sqlBC.BulkCopyTimeout = 60;

                    //設定要寫入的資料庫
                    sqlBC.DestinationTableName = "product";

                    //對應資料行

                    /*command2.Parameters.Add(new SqlParameter("@proid", mainproduct.pro_id));
                    command2.Parameters.Add(new SqlParameter("@sort", mainproduct.big_sort_id));
                    command2.Parameters.Add(new SqlParameter("@pdname", mainproduct.pdname));
                    command2.Parameters.Add(new SqlParameter("@amount", mainproduct.amount));
                    command2.Parameters.Add(new SqlParameter("@vmnum", vm_num));
                    command2.Parameters.Add(new SqlParameter("@pic", mainproduct.pic));
                    command2.Parameters.Add(new SqlParameter("@unit", mainproduct.unit_price));
                    command2.Parameters.Add(new SqlParameter("@cost", mainproduct.cost_price));
                    command2.Parameters.Add(new SqlParameter("@bonus", '0'));
                    command2.Parameters.Add(new SqlParameter("@point", '0'));
                    command2.Parameters.Add(new SqlParameter("@content", mainproduct.content1));
                    command2.Parameters.Add(new SqlParameter("@uid", uid));
                    command2.Parameters.Add(new SqlParameter("@tenant", '0'));
                    command2.Parameters.Add(new SqlParameter("@life", mainproduct.shelf_life_day));*/
        /*sqlBC.ColumnMappings.Add("pro_id", "pro_id");
        sqlBC.ColumnMappings.Add("big_sort_id", "big_sort_id");
        sqlBC.ColumnMappings.Add("pdname", "pdname");
        sqlBC.ColumnMappings.Add("amount","amount");
        sqlBC.ColumnMappings.Add("vm_num","vm_num");
        sqlBC.ColumnMappings.Add("pic","pic");
        sqlBC.ColumnMappings.Add("unit_price","unit_price");
        sqlBC.ColumnMappings.Add("cost_price", "cost_price");
        sqlBC.ColumnMappings.Add("bonus", "bonus");
        sqlBC.ColumnMappings.Add("point", "point");
        sqlBC.ColumnMappings.Add("content1", "content1");
        sqlBC.ColumnMappings.Add("vm_uid", "vm_uid");
        sqlBC.ColumnMappings.Add("vm_tenant_id", "vm_tenant_id");
        sqlBC.ColumnMappings.Add("shelf_life_day", "shelf_life_day");


        //開始寫入
        sqlBC.WriteToServer(dt);
    }
}
}*/
    }




    public class Product
    {

        public int vmprid { get; set; }
        public string vmNum { get; set; }
        public int roadLayer { get; set; }
        public string roadId { get; set; }
        public int layerNum { get; set; }
        public string roadNum { get; set; }
        public string roadCode { get; set; }
        public string roadtype { get; set; }
        public string roadRty { get; set; }
        public int proId { get; set; }
        public string pdName { get; set; }
        public object content { get; set; }
        public string pic { get; set; }
        public int proQty { get; set; }
        public string vmMode { get; set; }
        public string actMode { get; set; }
        public int vmUid { get; set; }
        public string vmTenantId { get; set; }
        public int unitPrice { get; set; }
        public int costPrice { get; set; }
        public string bonus { get; set; }
        public string point { get; set; }
        public int spAct { get; set; }
        public int roadAct { get; set; }
        public string upData { get; set; }
        public string upTime { get; set; }
        public string upRepdate { get; set; }
        public string shelfLifeDay { get; set; }
        public string expiryDate { get; set; }
    }

    public class Mainproduct
    {
        public string sortName { get; set; }
        public int Pcount { get; set; }
        public int id { get; set; }
        public int pro_id { get; set; }
        public int big_sort_id { get; set; }
        public string pdname { get; set; }
        public string amount { get; set; }
        public string vm_num { get; set; }
        public string pic { get; set; }
        public int unit_price { get; set; }
        public int cost_price { get; set; }
        public string bonus { get; set; }
        public string point { get; set; }
        public string url { get; set; }
        public string content1 { get; set; }
        public int pr_visible { get; set; }
        public int pr_bonusChange { get; set; }
        public int vm_uid { get; set; }
        public string vm_tenant_id { get; set; }
        public string shelf_life_day { get; set; }
        public int isUse { get; set; }
    }
}