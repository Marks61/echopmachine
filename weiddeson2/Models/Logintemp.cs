﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class LogintempRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);

        public Logintemp login(string cm_id,string name)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            Logintemp logintemp = new Logintemp();
            try
            {
                SqlCommand command2 = new SqlCommand(@"update Logintemp set cm_id=@id,name=@name,login_time='"+DateTime.Now.AddMinutes(45).ToString("yyyy-MM-dd HH:mm:ss")+"' where id=1");
                command2.Parameters.Add(new SqlParameter("@id",cm_id));
                command2.Parameters.Add(new SqlParameter("@name", name));
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                
                SqlCommand command1 = new SqlCommand(@"select cm_id,name from Logintemp where id=1");
                command1.Connection = sql;
                sql.Open();
                SqlDataReader reader = command1.ExecuteReader();
                if (reader.HasRows)
                {
                    while(reader.Read())
                    {
                        logintemp.cm_id = reader.GetString(reader.GetOrdinal("cm_id"));
                        logintemp.name = reader.GetString(reader.GetOrdinal("name"));
                    }
                }
                sql.Close();
                return logintemp;
            }
            catch (Exception e)
            {
                return new Logintemp()
                {
                };
            }
            

        }

        public Logintemp getLogin()
        {
            DateTime date1 = new DateTime();
            date1 = DateTime.Now;
            DateTime date2 = new DateTime();
            Logintemp logintemp = new Logintemp();
            SqlConnection sql = new SqlConnection(sqlconn5);

            SqlCommand command1 = new SqlCommand(@"select login_time from Logintemp where id=1");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    date2 = reader.IsDBNull(0)? Convert.ToDateTime("0001-01-01 00:00:00") : reader.GetDateTime(reader.GetOrdinal("login_time"));
                }
            }
            sql.Close();

            if(date1 > date2)
            {
                SqlCommand command2 = new SqlCommand(@"update Logintemp set cm_id=null,name=null,login_time=null where id=1");
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();

                return logintemp;
            }
            else
            {
                SqlCommand command2 = new SqlCommand(@"update Logintemp set login_time='" + DateTime.Now.AddMinutes(45).ToString("yyyy-MM-dd HH:mm:ss") + "' where id=1");
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();

                SqlCommand command3 = new SqlCommand(@"select cm_id,name from Logintemp where id=1");
                command3.Connection = sql;
                sql.Open();
                SqlDataReader reader2 = command3.ExecuteReader();
                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        logintemp.cm_id = reader2.GetString(reader2.GetOrdinal("cm_id"));
                        logintemp.name = reader2.GetString(reader2.GetOrdinal("name"));
                    }
                }
                sql.Close();
                return logintemp;
            }
        }

        public bool logout()
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            
            try
            {
                SqlCommand command2 = new SqlCommand(@"update Logintemp set cm_id=null,name=null,login_time=null where id=1");
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }


    public class Logintemp
    {
        public string cm_id { get; set; }
        public string name { get; set; }
        public DateTime login { get; set; }
    }
}