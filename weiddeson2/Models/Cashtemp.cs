﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{

    public class CashtempRepository
    {
        privateRsa privateRsa = new privateRsa();
        //private static readonly string sqlconn = System.Configuration.ConfigurationManager.AppSettings["sqlconn2"];
        private static readonly string sqlconn2 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);

        public bool resetCashtemp()
        {
            SqlConnection sql = new SqlConnection(sqlconn2);
            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0");
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return true;
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool cashtempStart(int salse)
        {
            SqlConnection sql = new SqlConnection(sqlconn2);
            try
            {
                //SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set sales_price=" + Convert.ToString(salse) + ",is_receive=1 where cid=1");
                SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set sales_price=" + Convert.ToString(salse) + ",is_receive=1");
                command2.Connection = sql;
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Cashtemp getCashin()
        {
            int salse_price = 0;
            string cashIn = "";
            Cashtemp cashtemp1 = new Cashtemp();
            SqlConnection sql = new SqlConnection(sqlconn2);
            //SqlCommand command1 = new SqlCommand("select cash_in,sales_price from dbo.cash_temp where cid=1");
            SqlCommand command1 = new SqlCommand("select cash_in,sales_price,is_receive,is_refund from dbo.cash_temp");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    cashIn= reader1.GetString(reader1.GetOrdinal("cash_in"));
                    if (cashIn == "0" )
                    {
                        cashtemp1.coollision = true;
                        break;
                    }
                    else
                    {
                        cashtemp1.coollision = false;
                        salse_price = Convert.ToInt32(reader1.GetString(reader1.GetOrdinal("sales_price")));
                        cashtemp1.cashIn = reader1.GetString(reader1.GetOrdinal("cash_in"));
                        cashtemp1.nonCash = salse_price - Convert.ToInt32(cashtemp1.cashIn);
                        cashtemp1.salsePrice = Convert.ToString(salse_price);
                        cashtemp1.cashOut = updateCashout(Convert.ToInt32(cashtemp1.cashIn) - salse_price);
                        cashtemp1.is_receive = reader1.GetInt32(reader1.GetOrdinal("is_receive"));
                        cashtemp1.is_refund = reader1.GetInt32(reader1.GetOrdinal("is_refund"));
                    }
                    if (cashtemp1.nonCash > 0)
                    {
                        //cashtemp1.buySuccess = false;
                        cashtemp1.buySuccess = false;
                    }
                    else
                    {
                        cashtemp1.buySuccess = true;
                        cashtemp1.nonCash = 0;
                    }
                }
            }
            sql.Close();

            return cashtemp1;
        }

        public string updateCashout(int cashOut)
        {
            if(cashOut<0)
            {
                cashOut = 0;
            }

            SqlConnection sql = new SqlConnection(sqlconn2);
            //SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set cash_out=" + cashOut + " where cid=1");
            SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set cash_out=" + cashOut);
            command2.Connection = sql;
            sql.Open();
            command2.ExecuteNonQuery();
            sql.Close();
            return Convert.ToString(cashOut);
        }

        public int returnGivecash(int buycasher)
        {
            int givecash = 0;
            int salse_price = buycasher;
            SqlConnection sql = new SqlConnection(sqlconn2);
            //SqlCommand command1 = new SqlCommand("select cash_in from dbo.cash_temp where cid=1");
            SqlCommand command1 = new SqlCommand("select cash_in from dbo.cash_temp");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    givecash =((Convert.ToInt32(reader1.GetString(reader1.GetOrdinal("cash_in")))-salse_price));
                }
            }
            sql.Close();

            return givecash;
        }

        public int returnCashout()
        {
            int cashOut = 0;
            SqlConnection sql = new SqlConnection(sqlconn2);
            //SqlCommand command1 = new SqlCommand("select cash_out from dbo.cash_temp where cid=1");
            SqlCommand command1 = new SqlCommand("select cash_out from dbo.cash_temp");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    cashOut = Convert.ToInt32(reader1.GetString(reader1.GetOrdinal("cash_out")));
                }
            }
            sql.Close();

            return cashOut;
        }

        public bool Cashout(int cashIn)
        {
            //int cashIn = 0;
            SqlConnection sql2 = new SqlConnection(sqlconn2);
            //SqlCommand command3 = new SqlCommand("select cash_in from dbo.cash_temp where cid=1");
            /*SqlCommand command3 = new SqlCommand("select cash_in from dbo.cash_temp");
            command3.Connection = sql2;
            sql2.Open();
            SqlDataReader reader1 = command3.ExecuteReader();
            if(reader1.HasRows)
            {
                while(reader1.Read())
                {
                    cashIn = Convert.ToInt32(reader1.GetString(reader1.GetOrdinal("cash_in")));
                    break;
                }
            }
            sql2.Close();*/

            try
            {
                //SqlCommand command4 = new SqlCommand(@"update dbo.cash_temp set cash_out=@cashout,cash_in=0 where cid=1");
                SqlCommand command4 = new SqlCommand(@"update dbo.cash_temp set cash_out=@cashout,cash_in=0");
                command4.Connection = sql2;
                command4.Parameters.Add(new SqlParameter("@cashout",cashIn));
                sql2.Open();
                command4.ExecuteNonQuery();
                sql2.Close();
            }
            catch (Exception e)
            {
                return false;
            }

            
            if(returnCashout() != 0)
            {
                SqlConnection sql = new SqlConnection(sqlconn2);
                try
                {
                    //SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set is_refund=1 where cid=1");
                    SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set is_refund=1");
                    command2.Connection = sql;
                    sql.Open();
                    command2.ExecuteNonQuery();
                    sql.Close();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }

            }
            else
            {
                return true;
            }
        }

        public bool Cashout2()
        {
            string cashIn = "0";
            if (returnCashout() != 0)
            {
                SqlConnection sql = new SqlConnection(sqlconn2);
                SqlCommand command3 = new SqlCommand(@"select cash_in from cash_temp");
                command3.Connection = sql;
                sql.Open();
                SqlDataReader reader = command3.ExecuteReader();
                while(reader.Read())
                {
                    cashIn = reader.GetString(reader.GetOrdinal("cash_in"));
                }
                sql.Close();
                try
                {
                    //SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set is_refund=1 where cid=1");
                    SqlCommand command2 = new SqlCommand(@"update dbo.cash_temp set cash_in=@cashin,is_refund=1");
                    command2.Parameters.Add(new SqlParameter("@cashin",cashIn));
                    command2.Connection = sql;
                    sql.Open();
                    command2.ExecuteNonQuery();
                    sql.Close();
                    SqlCommand command4 = new SqlCommand(@"update dbo.cash_temp set cash_in=@cashin");
                    command4.Parameters.Add(new SqlParameter("@cashin", 0));
                    command4.Connection = sql;
                    sql.Open();
                    command4.ExecuteNonQuery();
                    sql.Close();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }

            }
            else
            {
                return true;
            }
        }

        public Cashtemp getCash()
        {
            Cashtemp cashtemp1 = new Cashtemp();
            SqlConnection sql = new SqlConnection(sqlconn2);
            //SqlCommand command1 = new SqlCommand("select cash_in,cash_out from dbo.cash_temp where cid=1");
            SqlCommand command1 = new SqlCommand("select cash_in,cash_out from dbo.cash_temp");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    cashtemp1.cashIn = reader1.GetString(reader1.GetOrdinal("cash_in"));
                    cashtemp1.cashOut = reader1.GetString(reader1.GetOrdinal("cash_out"));
                }
            }
            sql.Close();

            return cashtemp1;
        }
    }

    public class Cashtemp
    {
        public string cashIn { get; set; }
        public string salsePrice { get; set; }
        public int is_receive { get; set; }
        public int is_refund { get; set; }
        public string cashOut { get; set; }
        public int nonCash { get; set; }
        public bool buySuccess { get; set; }
        public bool coollision { get; set; }
    }
}