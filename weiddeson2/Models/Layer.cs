﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class LayerRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);

        public List<Layer> frontedGetLayers()
        {
            List<Layer> layers = new List<Layer>();
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand("select * from vmcode or 1=1");
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader1 = command1.ExecuteReader();
            if(reader1.HasRows)
            {
                while(reader1.Read()){
                    Layer layer = new Layer()
                    {
                        vmid = reader1.GetInt32(reader1.GetOrdinal("vmid")),
                        city = reader1.GetString(reader1.GetOrdinal("country")),
                        zip = reader1.GetString(reader1.GetOrdinal("zip")),
                        address = reader1.GetString(reader1.GetOrdinal("address"))
                    };

                    layers.Add(layer);
                }
            }
            sql.Close();

            return layers;
        }


    }
    public class Layer
    {
        public int vmid { get; set; }
        public string vmNum { get; set; }
        public string vmName { get; set; }
        public string vmUid { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string address { get; set; }
        public int zt { get; set; }
        public string type { get; set; }
    }
}