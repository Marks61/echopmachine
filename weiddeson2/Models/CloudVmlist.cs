﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class CloudVmlistRepository
    {
        privateRsa privateRsa = new privateRsa();
        //private readonly string sqlconn = System.Configuration.ConfigurationManager.AppSettings["sqlconn"];
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);

        public List<CloudVmlist> cloudVms(string st)
        {
            SqlConnection sql = new SqlConnection(sqlconn5);
            List<CloudVmlist> cloudVmlists = new List<CloudVmlist>();
            SqlCommand command = new SqlCommand(@"select * from vm_list where st_id=@st");
            command.Connection = sql;
            command.Parameters.Add(new SqlParameter("@st",st));
            sql.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CloudVmlist cloudVmlist = new CloudVmlist()
                    {
                        vm_id = reader.GetString(reader.GetOrdinal("vm_id")),
                        st_id = reader.GetString(reader.GetOrdinal("st_id")),
                        vm_num = reader.GetString(reader.GetOrdinal("vm_num")),
                        evm_shop_name = reader.GetString(reader.GetOrdinal("evm_shop_name")),
                        bscode = reader.GetString(reader.GetOrdinal("bscode")),
                        vm_grid = reader.GetString(reader.GetOrdinal("vm_grid")),
                        vm_mode_id = reader.GetInt32(reader.GetOrdinal("vm_mode_id")),
                        vm_mode = reader.GetString(reader.GetOrdinal("vm_mode")),
                        vm_mode_num = reader.GetString(reader.GetOrdinal("vm_mode_num")),
                        p_size = reader.GetString(reader.GetOrdinal("p_size")),
                        ac_power = reader.GetString(reader.GetOrdinal("ac_power")),
                        is_sp = reader.GetInt32(reader.GetOrdinal("is_sp")),
                        is_lifts = reader.GetInt32(reader.GetOrdinal("is_lifts")),
                        area = reader.GetString(reader.GetOrdinal("area")),
                        address = reader.GetString(reader.GetOrdinal("address")),
                        act_mode = reader.IsDBNull(15)? "":reader.GetString(reader.GetOrdinal("act_mode")),
                        road_act_mode = reader.GetInt32(reader.GetOrdinal("road_act_mode")),
                        online = reader.GetString(reader.GetOrdinal("online")),
                        vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid")),
                        is_updata = reader.GetInt32(reader.GetOrdinal("is_updata")),
                        prezt = reader.GetInt32(reader.GetOrdinal("prezt")),
                        bunusgamezt = reader.GetInt32(reader.GetOrdinal("bunusgamezt")),
                        vm_group_id = reader.IsDBNull(22) ? 0 : reader.GetInt32(reader.GetOrdinal("vm_group_id")),
                        vm_group_name = reader.IsDBNull(23) ? "" : reader.GetString(reader.GetOrdinal("vm_group_name"))
                    };
                    cloudVmlists.Add(cloudVmlist);
                }
            }
            sql.Close();
            return cloudVmlists;
        }

        public bool insert(string taxid, string country, string city, int zip, string address, string location, string name)
        {
            string sqlcommandStr = "";
            SqlConnection sql = new SqlConnection(sqlconn5);
            try
            {
                sqlcommandStr += "insert into store_location (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "st_id," +
                    "cm_id," +
                    "name," +
                    "country," +
                    "city," +
                    "zip," +
                    "address," +
                    "location," +
                    "isUse)";
                sqlcommandStr += " values('" + Convert.ToString(Guid.NewGuid()) + "'," +
                    "@cmid," +
                    "@name" + "," +
                    "@country," +
                    "@city," +
                    "@zip," +
                    "@address," +
                    "@location" +
                    ",1)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);
                command1.Parameters.Add(new SqlParameter("@cmid", taxid));
                command1.Parameters.Add(new SqlParameter("@name", name));
                command1.Parameters.Add(new SqlParameter("@country", country));
                command1.Parameters.Add(new SqlParameter("@city", city));
                command1.Parameters.Add(new SqlParameter("@zip", zip));
                command1.Parameters.Add(new SqlParameter("@address", address));
                command1.Parameters.Add(new SqlParameter("@location", location));
                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

    public class CloudVmlist
    {
        public string vm_id { get; set; }
        public string st_id { get; set; }
        public string vm_num { get; set; }
        public string evm_shop_name { get; set; }
        public string bscode { get; set; }
        public string vm_grid { get; set; }
        public int vm_mode_id { get; set; }
        public string vm_mode { get; set; }
        public string vm_mode_num { get; set; }
        public string p_size { get; set; }
        public string ac_power { get; set; }
        public int is_sp { get; set; }
        public int is_lifts { get; set; }
        public string area { get; set; }
        public string address { get; set; }
        public string act_mode { get; set; }
        public int road_act_mode { get; set; }
        public string online { get; set; }
        public int vm_uid { get; set; }
        public int is_updata { get; set; }
        public int prezt { get; set; }
        public int bunusgamezt { get; set; }
        public int vm_group_id { get; set; }
        public string vm_group_name { get; set; }
    }
}