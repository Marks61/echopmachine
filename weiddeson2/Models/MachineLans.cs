﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{

    public class MachineLasRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);

        public int getMachineType(int vmuid)
        {
            int type = 0;
            SqlConnection sql1 = new SqlConnection(sqlconn);

            SqlCommand command1 = new SqlCommand(@"select MACHINETYPE from vm_setp_list where vm_uid=@uid");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    type = reader.GetInt32(reader.GetOrdinal("MACHINETYPE"));
                    break;
                }
            }
            sql1.Close();

            return type;
        }

        public List<MachineLans> machineLayer(int vmuid,string vmnum)
        {
            int type = 0;
            SqlConnection sql1 = new SqlConnection(sqlconn);
            List<MachineLans> machineLayer = new List<MachineLans>();
            string commandStr = "";

            if (this.getMachineType(vmuid) == 1)
            { 
                commandStr = "select layer_num from vmroad where vm_num=@num group by layer_num order by layer_num asc";
            }
            else
            {               
                commandStr = "select road_layer from vmroad where vm_num=@num group by road_layer order by road_layer asc"; 
            }

            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@num", vmnum));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (this.getMachineType(vmuid) == 1)
                    {
                       MachineLans machineLans = new MachineLans()
                        {
                           
                            layer_num = reader.GetInt32(reader.GetOrdinal("layer_num"))
                        };
                        machineLayer.Add(machineLans);
                    }
                    else
                    { 
                        MachineLans machineLans = new MachineLans()
                        {
                            road_layer = reader.GetInt32(reader.GetOrdinal("road_layer"))
                        };
                        machineLayer.Add(machineLans);
                    }
                }
            }
            sql1.Close();

            return machineLayer;
        }

        public List<MachineLans> machineLans(int vmuid,string vmnum,string roadLayer)
        {
            int type = 0;
            SqlConnection sql1 = new SqlConnection(sqlconn);
            List<MachineLans> machineLans1 = new List<MachineLans>();
            string commandStr = "";

            if (this.getMachineType(vmuid) == 1)
            {                
                if(roadLayer == "")
                {
                    commandStr = "select roadid,layer_num,road_num,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num order by road_code asc";
                }
                else
                {
                    commandStr = "select roadid,layer_num,road_num,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num and road_layer=@roadLayer order by road_code asc";
                }

            }
            else
            {
                if (roadLayer == "")
                {
                    commandStr = "select road_layer,roadid,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num order by road_code asc";
                }
                else
                {
                    commandStr = "select road_layer,roadid,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num and road_layer=@roadLayer order by road_code asc";
                }

            }

            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@num", vmnum));
            command1.Parameters.Add(new SqlParameter("@roadLayer",roadLayer));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if(this.getMachineType(vmuid) == 1)
                    {
                        MachineLans machineLans = new MachineLans()
                        {
                            road_num = reader.GetString(reader.GetOrdinal("road_num")),
                            layer_num = reader.GetInt32(reader.GetOrdinal("layer_num")),
                            road_code = reader.GetString(reader.GetOrdinal("road_code")),
                            roadqty = reader.GetString(reader.GetOrdinal("roadqty")),
                            act_mode = reader.GetInt32(reader.GetOrdinal("act_mode")),
                            vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid")),
                            road_act = reader.GetInt32(reader.GetOrdinal("road_act")),
                            roadtyp = reader.GetString(reader.GetOrdinal("roadtyp")),
                            roadid = reader.GetString(reader.GetOrdinal("roadid"))
                        };
                        machineLans1.Add(machineLans);
                    }
                    else
                    {
                        MachineLans machineLans = new MachineLans()
                        {
                            road_layer = reader.GetInt32(reader.GetOrdinal("road_layer")),
                            roadid = reader.GetString(reader.GetOrdinal("roadid")),
                            road_code = reader.GetString(reader.GetOrdinal("road_code")),
                            roadqty=reader.GetString(reader.GetOrdinal("roadqty")),
                            act_mode=reader.GetInt32(reader.GetOrdinal("act_mode")),
                            vm_uid=reader.GetInt32(reader.GetOrdinal("vm_uid")),
                            road_act=reader.GetInt32(reader.GetOrdinal("road_act")),
                            roadtyp = reader.GetString(reader.GetOrdinal("roadtyp"))
                        };
                        machineLans1.Add(machineLans);

                    }
                }
            }
            sql1.Close();

            return machineLans1;
        }

        public string Qtyminus(string roadid,int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            string qty = "";
            SqlCommand command1 = new SqlCommand(@"select roadqty from vmroad where vm_uid=@uid and roadid=@id");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@id",roadid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qty = reader.GetString(reader.GetOrdinal("roadqty"));
                    break;
                }
            }
            sql.Close();

            qty = Convert.ToString(Convert.ToInt32(qty)-1);

            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vmroad set roadqty=@qty where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@id", roadid));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Parameters.Add(new SqlParameter("@qty",qty));
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return qty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return qty;
            }
        }

        public string Qtyplus(string roadid, int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            string qty = "";
            SqlCommand command1 = new SqlCommand(@"select roadqty from vmroad where vm_uid=@uid and roadid=@id");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@id", roadid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qty = reader.GetString(reader.GetOrdinal("roadqty"));
                    break;
                }
            }
            sql.Close();

            qty = Convert.ToString(Convert.ToInt32(qty) + 1);

            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vmroad set roadqty=@qty where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@id", roadid));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Parameters.Add(new SqlParameter("@qty", qty));
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return qty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return qty;
            }
        }

        public string ProductQtymiuns(string roadid, int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            string qty = "";
            SqlCommand command1 = new SqlCommand(@"select proqty from vm_pro_qty where vm_uid=@uid and roadid=@id");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@id", roadid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qty = Convert.ToString(reader.GetInt32(reader.GetOrdinal("proqty")));
                    break;
                }
            }
            sql.Close();

            qty = Convert.ToString(Convert.ToInt32(qty) - 1);

            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vm_pro_qty set proqty=@qty where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@id", roadid));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Parameters.Add(new SqlParameter("@qty", qty));
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return qty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return qty;
            }
        }

        public string ProductQtyplus(string roadid, int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            string qty = "";
            SqlCommand command1 = new SqlCommand(@"select proqty from vm_pro_qty where vm_uid=@uid and roadid=@id");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@id", roadid));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qty = Convert.ToString(reader.GetInt32(reader.GetOrdinal("proqty")));
                    break;
                }
            }
            sql.Close();

            qty = Convert.ToString(Convert.ToInt32(qty) + 1);

            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vm_pro_qty set proqty=@qty where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@id", roadid));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Parameters.Add(new SqlParameter("@qty", qty));
                command.Connection = sql;
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                return qty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return qty;
            }
        }

        public bool Roadoff(string roadid,int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@act", "0"));
                command.Parameters.Add(new SqlParameter("@act2","2"));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Connection = sql;
                command.Parameters.Add(new SqlParameter("@id", roadid));
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command2 = new SqlCommand(@"update dbo.vm_pro_qty set road_act=@act,roadtyp=@act2 where roadid=@id and vm_uid=@uid");
                command2.Parameters.Add(new SqlParameter("@act", "0"));
                command2.Parameters.Add(new SqlParameter("@act2", "2"));
                command2.Parameters.Add(new SqlParameter("@uid", vmuid));
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@id", roadid));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool Roadon(string roadid, int vmuid)
        {
            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                //SqlCommand command = new SqlCommand(@"update dbo.cash_temp set cash_in=0,sales_price=0,is_receive=0,is_refund=0,cash_out=0 where cid=1");
                SqlCommand command = new SqlCommand(@"update dbo.vmroad set road_act=@act,roadtyp=@act2 where roadid=@id and vm_uid=@uid");
                command.Parameters.Add(new SqlParameter("@act", "1"));
                command.Parameters.Add(new SqlParameter("@act2", "1"));
                command.Parameters.Add(new SqlParameter("@uid", vmuid));
                command.Connection = sql;
                command.Parameters.Add(new SqlParameter("@id", roadid));
                sql.Open();
                command.ExecuteNonQuery();
                sql.Close();
                SqlCommand command2 = new SqlCommand(@"update dbo.vm_pro_qty set road_act=@act,roadtyp=@act2 where roadid=@id and vm_uid=@uid");
                command2.Parameters.Add(new SqlParameter("@act", "1"));
                command2.Parameters.Add(new SqlParameter("@act2", "1"));
                command2.Parameters.Add(new SqlParameter("@uid", vmuid));
                command2.Connection = sql;
                command2.Parameters.Add(new SqlParameter("@id", roadid));
                sql.Open();
                command2.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public MachineLans GetmachineLans(int vmuid, string vmnum, string id)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            string commandStr = "";
            MachineLans machineLans = new MachineLans();

            if (this.getMachineType(vmuid) == 1)
            {
                commandStr = "select roadid,layer_num,road_num,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num and roadid=@id";
            }
            else
            {
                commandStr = "select road_layer,roadid,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_num=@num and roadid=@id";
            }

            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@num", vmnum));
            command1.Parameters.Add(new SqlParameter("@id", id));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (this.getMachineType(vmuid) == 1)
                    {
                        machineLans.road_num = reader.GetString(reader.GetOrdinal("road_num"));
                        machineLans.layer_num = reader.GetInt32(reader.GetOrdinal("layer_num"));
                        machineLans.road_code = reader.GetString(reader.GetOrdinal("road_code"));
                        machineLans.roadqty = reader.GetString(reader.GetOrdinal("roadqty"));
                        machineLans.act_mode = reader.GetInt32(reader.GetOrdinal("act_mode"));
                        machineLans.vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                        machineLans.road_act = reader.GetInt32(reader.GetOrdinal("road_act"));
                        machineLans.roadtyp = reader.GetString(reader.GetOrdinal("roadtyp"));
                        machineLans.roadid = reader.GetString(reader.GetOrdinal("roadid"));
                    }
                    else
                    {
                        machineLans.road_layer = reader.GetInt32(reader.GetOrdinal("road_layer"));
                        machineLans.roadid = reader.GetString(reader.GetOrdinal("roadid"));
                        machineLans.road_code = reader.GetString(reader.GetOrdinal("road_code"));
                        machineLans.roadqty = reader.GetString(reader.GetOrdinal("roadqty"));
                        machineLans.act_mode = reader.GetInt32(reader.GetOrdinal("act_mode"));
                        machineLans.vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                        machineLans.road_act = reader.GetInt32(reader.GetOrdinal("road_act"));
                        machineLans.roadtyp = reader.GetString(reader.GetOrdinal("roadtyp"));
                    }
                }
            }
            sql1.Close();

            return machineLans;
        }

        public int getLayerinfo(int type, int uid, string vmnum, string roadid)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            string commandStr = "";
            int layer = 0;
            MachineLans machineLans = new MachineLans();

            if (type == 1)
            {
                commandStr = "select layer_num from vmroad where vm_num=@num and roadid=@id";
            }
            else
            {
                commandStr = "select road_layer from vmroad where vm_num=@num and roadid=@id";
            }

            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@num", vmnum));
            command1.Parameters.Add(new SqlParameter("@id", roadid));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (type == 1)
                    {

                        layer = reader.GetInt32(reader.GetOrdinal("layer_num"));

                    }
                    else
                    {
                        layer = reader.GetInt32(reader.GetOrdinal("road_layer"));

                    }
                }
            }
            return layer;
        }

        public bool roadCheck(int id)
        {
            bool check = false;
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command = new SqlCommand(@"select pro_id from vm_pro_qty where roadid=@id");
            command.Parameters.Add(new SqlParameter("@id",id));
            command.Connection = sql;
            sql.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.GetInt32(reader.GetOrdinal("pro_id")) == 0)
                    {
                        check = false;
                    }
                    else
                    {
                        check = true;
                    }
                    break;
                }
            }
            
            sql.Close();
            return check;
        }

        public List<MachineLans> GetmachineLansmore(int vmuid,int roadLayer)
        {
            int type = 0;
            SqlConnection sql1 = new SqlConnection(sqlconn);
            List<MachineLans> machineLans1 = new List<MachineLans>();
            string commandStr = "";

            if (this.getMachineType(vmuid) == 1)
            {
                commandStr = "select roadid,layer_num,road_num,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_uid=@uid and layer_num=@roadLayer order by road_code asc";
            }
            else
            {
                commandStr = "select road_layer,roadid,road_code,roadqty,roadtyp,act_mode,vm_uid,road_act,roadtyp from vmroad where vm_uid=@uid and layer_num=@roadLayer order by road_code asc";
            }

            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@roadLayer", roadLayer));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (this.getMachineType(vmuid) == 1)
                    {
                        MachineLans machineLans = new MachineLans()
                        {
                            road_num = reader.GetString(reader.GetOrdinal("road_num")),
                            layer_num = reader.GetInt32(reader.GetOrdinal("layer_num")),
                            road_code = reader.GetString(reader.GetOrdinal("road_code")),
                            roadqty = reader.GetString(reader.GetOrdinal("roadqty")),
                            act_mode = reader.GetInt32(reader.GetOrdinal("act_mode")),
                            vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid")),
                            road_act = reader.GetInt32(reader.GetOrdinal("road_act")),
                            roadtyp = reader.GetString(reader.GetOrdinal("roadtyp")),
                            roadid = reader.GetString(reader.GetOrdinal("roadid"))
                        };
                        machineLans1.Add(machineLans);
                    }
                    else
                    {
                        MachineLans machineLans = new MachineLans()
                        {
                            road_layer = reader.GetInt32(reader.GetOrdinal("road_layer")),
                            roadid = reader.GetString(reader.GetOrdinal("roadid")),
                            road_code = reader.GetString(reader.GetOrdinal("road_code")),
                            roadqty = reader.GetString(reader.GetOrdinal("roadqty")),
                            act_mode = reader.GetInt32(reader.GetOrdinal("act_mode")),
                            vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid")),
                            road_act = reader.GetInt32(reader.GetOrdinal("road_act")),
                            roadtyp = reader.GetString(reader.GetOrdinal("roadtyp"))
                        };
                        machineLans1.Add(machineLans);

                    }
                }
            }
            sql1.Close();

            return machineLans1;
        }
    }

    public class MachineLans
    {
        public int rdid { get; set; }
        public string cm_num { get; set; }
        public int road_layer { get; set; }
        public string roadid { get; set; }
        public int layer_num { get; set; }
        public string road_num { get; set; }
        public string road_code { get; set; }
        public string roadqty { get; set; }
        public string roadtyp { get; set; }
        public string merge_qty { get; set; }
        public int act_mode { get; set; }
        public int vm_uid { get; set; }
        public string vm_tenant_id { get; set; }
        public int road_act { get; set; }
        public int MACHINETYPE { get; set; }
    }
}