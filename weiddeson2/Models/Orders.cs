﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Diagnostics.SymbolStore;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls.Expressions;
using weiddeson2.App_Code;

namespace weiddeson2.Models
{
    public class OrdersRepository
    {
        privateRsa privateRsa = new privateRsa();
        private readonly string sqlconn = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn"]);
        private readonly string sqlconn5 = privateRsa.Decryption(System.Configuration.ConfigurationManager.AppSettings["sqlconn5"]);

        public bool paymentAct(string Roadid,DateTime date1,string dingdan,object cash_in)
        {
            ProductRepository productRepo = new ProductRepository();
            CashtempRepository cashtempRepo = new CashtempRepository();
            Cashtemp cashtemp = new Cashtemp();
            Product product = new Product();
            string road = "";
            if (Convert.ToInt32(Roadid) <= 9)
            {
                road = "0" + Roadid;
            }
            else
            {
                road = Convert.ToString(Roadid);
            }

            product = productRepo.getAll(Convert.ToString(Roadid));
            cashtemp = cashtempRepo.getCash();
            string sqlcommandStr = "";
            string qrcode = "";
            string device = "";
            string exchgs = "";
            string pickup = "";
            string bz = "";
            string vm_uid2 = "";
            string vm_tenant_id = "";
            string card_num = "";
            string u_id = "";

            if (qrcode != "")
            {
              qrcode="'"+qrcode+"'";
            }
            else
            {
                qrcode = "NULL";
            }

            if (device != "")
            {
                device="'"+device+"";
            }
            else
            {
                device = "NULL";
            }

            if (exchgs != "")
            {
               exchgs="'"+exchgs+"'";
            }
            else
            {
                exchgs = "NULL";
            }

            if (pickup != "")
            {
                pickup="'"+pickup+"'";
            }
            else
            {
                pickup = "NULL";
            }

            if (bz != "")
            {
                bz="'"+bz+"'";
            }
            else
            {
                bz="NULL";
            }

            if(vm_uid2 != "")
            {
                vm_uid2 = "'" + vm_uid2 + ";";
            }else
            {
                vm_uid2 = "6";
            }

            if(vm_tenant_id != "")
            {
                vm_tenant_id = "'" + vm_tenant_id + "'";
            }else
            {
                vm_tenant_id = "NULL";
            }

            if(card_num != "")
            {
                card_num = "'" + card_num + "'";
            }
            else
            {
                card_num = "NULL";
            }

            if(u_id != "")
            {
                u_id = "'" + u_id + "'";
            }
            else
            {
                u_id = "NULL";
            }

       
            string date = date1.ToString("yyyy/MM/dd");
            /*string year = Convert.ToString(date1.Year);
            string month = Convert.ToString(date1.Month);
            string day = Convert.ToString(date1.Day);
            string hours = Convert.ToString(date1.Hour);
            string minis = Convert.ToString(date1.Minute);
            string seconds = Convert.ToString(date1.Second);*/
            object DBNULL = DBNull.Value;
            string times = date1.ToString("HH:mm:ss");

            
            string od_ropdate = date1.ToString("yyyyMMdd");
            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                sqlcommandStr += "insert into vending_machine_order (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "pdname," +
                    "qrcode," +
                    //"road_num," +
                    //"layer_num," +
                    "roadid," +
                   // "layer_num,"+
                    "road_num,"+
                    "receive," +
                    "device_no," +
                    "vm_num," +
                    "pro_id," +
                    "ordernum," +
                    "order_date," +
                    "order_time," +
                    "order_type," +
                    "incom_type," +
                    "pay_mode," +
                    "pay_type" +
                    ",site_type," +
                    "forder_date," +
                    "forder_time," +
                    "unit_price," +
                    "cost_price," +
                    "vm_uid," +
                    "vm_tenant_id," +
                    "card_num," +
                    "u_id," +
                    //"bonus," +
                    //"point," +
                    "exchgs_code," +
                    "pickup_code," +
                    "od_repdate," +
                    "bz," +
                    "cash_in," +
                    "cash_out)";
                sqlcommandStr += " values(";
                sqlcommandStr += "@pdname,"
                    +qrcode+","+
                    //",@roadid," +
                    //"@layernum," +
                    "@roadid2," +
                    "@roadid2," +
                    "0,"
                    + device+
                    ",@vm_num2" +
                    ",@pro_id," +
                    "@dingdan," +
                    "@order_date," +
                    "@order_time," +
                    "@order_type," +
                    "@incom_type," +
                    "@pay_mode," +
                    "@pay_type," +
                    "@site_type," +
                    "@forder_date,"+
                    "@forder_time," +
                    "@unit_price," +
                    "@cost_price," +
                    "@vmuid" +","+
                    vm_tenant_id +","+
                    card_num +","+
                    u_id +","+
                    //",@bonus," +
                    //"@point," +
                    exchgs+","
                    +pickup+",@od_repdate,"
                    +bz+"," +
                    "@cash_in," +
                    "@cash_out)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);


                command1.Parameters.Add(new SqlParameter("@pdname",product.pdName));
                //command1.Parameters.Add(new SqlParameter("@road_layer",product.roadLayer));
                //command1.Parameters.Add(new SqlParameter("@roadid",product.roadNum));
                //command1.Parameters.Add(new SqlParameter("@layernum",product.layerNum));
                command1.Parameters.Add(new SqlParameter("@roadid2",road));
                //command1.Parameters.Add(new SqlParameter("@receive", 0));
                command1.Parameters.Add(new SqlParameter("@vm_num2",product.vmNum));
                command1.Parameters.Add(new SqlParameter("@pro_id",product.proId));
                command1.Parameters.Add(new SqlParameter("@dingdan",dingdan));
                command1.Parameters.Add(new SqlParameter("@order_date", date));
                command1.Parameters.Add(new SqlParameter("@order_time",times));
                command1.Parameters.Add(new SqlParameter("@order_type",1));
                command1.Parameters.Add(new SqlParameter("@incom_type",1));
                command1.Parameters.Add(new SqlParameter("@pay_mode",1));
                command1.Parameters.Add(new SqlParameter("@pay_type","1"));
                command1.Parameters.Add(new SqlParameter("@site_type",1));
                command1.Parameters.Add(new SqlParameter("@forder_date", DBNULL));
                command1.Parameters.Add(new SqlParameter("@forder_time", DBNULL));
                command1.Parameters.Add(new SqlParameter("@unit_price", product.unitPrice));
                command1.Parameters.Add(new SqlParameter("@cost_price",product.costPrice));
                //command1.Parameters.Add(new SqlParameter("@bonus",product.bonus));
                //command1.Parameters.Add(new SqlParameter("@point",product.point));
                command1.Parameters.Add(new SqlParameter("@vmuid", product.vmUid));
                command1.Parameters.Add(new SqlParameter("@od_repdate",od_ropdate));
                command1.Parameters.Add(new SqlParameter("@cash_in",cash_in));
                command1.Parameters.Add(new SqlParameter("@cash_out",cashtemp.cashOut));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool LinepaymentAct(string Roadid, DateTime date1, string dingdan, object cash_in,string transid)
        {
            ProductRepository productRepo = new ProductRepository();
            CashtempRepository cashtempRepo = new CashtempRepository();
            Cashtemp cashtemp = new Cashtemp();
            Product product = new Product();
            string road = "";
            if (Convert.ToInt32(Roadid) <= 9)
            {
                road = "0" + Roadid;
            }
            else
            {
                road = Convert.ToString(Roadid);
            }

            product = productRepo.getAll(Convert.ToString(Roadid));
            cashtemp = cashtempRepo.getCash();
            string sqlcommandStr = "";
            string qrcode = "";
            string device = "";
            string exchgs = "";
            string pickup = "";
            string bz = "";
            string vm_uid2 = "";
            string vm_tenant_id = "";
            string card_num = "";
            string u_id = "";

            if (qrcode != "")
            {
                qrcode = "'" + qrcode + "'";
            }
            else
            {
                qrcode = "NULL";
            }

            if (device != "")
            {
                device = "'" + device + "";
            }
            else
            {
                device = "NULL";
            }

            if (exchgs != "")
            {
                exchgs = "'" + exchgs + "'";
            }
            else
            {
                exchgs = "NULL";
            }

            if (pickup != "")
            {
                pickup = "'" + pickup + "'";
            }
            else
            {
                pickup = "NULL";
            }

            if (bz != "")
            {
                bz = "'" + bz + "'";
            }
            else
            {
                bz = "NULL";
            }

            if (vm_uid2 != "")
            {
                vm_uid2 = "'" + vm_uid2 + ";";
            }
            else
            {
                vm_uid2 = "6";
            }

            if (vm_tenant_id != "")
            {
                vm_tenant_id = "'" + vm_tenant_id + "'";
            }
            else
            {
                vm_tenant_id = "NULL";
            }

            if (card_num != "")
            {
                card_num = "'" + card_num + "'";
            }
            else
            {
                card_num = "NULL";
            }

            if (u_id != "")
            {
                u_id = "'" + u_id + "'";
            }
            else
            {
                u_id = "NULL";
            }


            string date = date1.ToString("yyyy/MM/dd");
            /*string year = Convert.ToString(date1.Year);
            string month = Convert.ToString(date1.Month);
            string day = Convert.ToString(date1.Day);
            string hours = Convert.ToString(date1.Hour);
            string minis = Convert.ToString(date1.Minute);
            string seconds = Convert.ToString(date1.Second);*/
            object DBNULL = DBNull.Value;
            string times = date1.ToString("HH:mm:ss");


            string od_ropdate = date1.ToString("yyyyMMdd");
            SqlConnection sql = new SqlConnection(sqlconn);
            try
            {
                sqlcommandStr += "insert into vending_machine_order (";
                //sqlcommandStr += "insert into sales_order (";
                sqlcommandStr += "pdname," +
                    "qrcode," +
                    //"road_num," +
                    //"layer_num," +
                    "roadid," +
                    // "layer_num,"+
                    "road_num," +
                    "receive," +
                    "device_no," +
                    "vm_num," +
                    "pro_id," +
                    "ordernum," +
                    "order_date," +
                    "order_time," +
                    "order_type," +
                    "incom_type," +
                    "pay_mode," +
                    "pay_type" +
                    ",site_type," +
                    "forder_date," +
                    "forder_time," +
                    "unit_price," +
                    "cost_price," +
                    "vm_uid," +
                    "vm_tenant_id," +
                    "card_num," +
                    "u_id," +
                    //"bonus," +
                    //"point," +
                    "exchgs_code," +
                    "pickup_code," +
                    "od_repdate," +
                    "bz," +
                    "cash_in," +
                    "cash_out)";
                sqlcommandStr += " values(";
                sqlcommandStr += "@pdname,"
                    + qrcode + "," +
                    //",@roadid," +
                    //"@layernum," +
                    "@roadid2," +
                    "@roadid2," +
                    "0,"
                    + device +
                    ",@vm_num2" +
                    ",@pro_id," +
                    "@dingdan," +
                    "@order_date," +
                    "@order_time," +
                    "@order_type," +
                    "@incom_type," +
                    "@pay_mode," +
                    "@pay_type," +
                    "@site_type," +
                    "@forder_date," +
                    "@forder_time," +
                    "@unit_price," +
                    "@cost_price," +
                    "@vmuid" + "," +
                    vm_tenant_id + "," +
                    card_num + "," +
                    u_id + "," +
                    //",@bonus," +
                    //"@point," +
                    exchgs + ","
                    + pickup + ",@od_repdate,"
                    + bz + "," +
                    "@cash_in," +
                    "@cash_out)";
                SqlCommand command1 = new SqlCommand(sqlcommandStr);

                command1.Parameters.Add(new SqlParameter("@pdname", product.pdName));
                //command1.Parameters.Add(new SqlParameter("@road_layer",product.roadLayer));
                //command1.Parameters.Add(new SqlParameter("@roadid",product.roadNum));
                //command1.Parameters.Add(new SqlParameter("@layernum",product.layerNum));
                command1.Parameters.Add(new SqlParameter("@roadid2", road));
                //command1.Parameters.Add(new SqlParameter("@receive", 0));
                command1.Parameters.Add(new SqlParameter("@vm_num2", product.vmNum));
                command1.Parameters.Add(new SqlParameter("@pro_id", product.proId));
                command1.Parameters.Add(new SqlParameter("@dingdan", dingdan));
                command1.Parameters.Add(new SqlParameter("@order_date", date));
                command1.Parameters.Add(new SqlParameter("@order_time", times));
                command1.Parameters.Add(new SqlParameter("@order_type", 1));
                command1.Parameters.Add(new SqlParameter("@incom_type", 1));
                command1.Parameters.Add(new SqlParameter("@pay_mode", 1));
                command1.Parameters.Add(new SqlParameter("@pay_type", "5"));
                command1.Parameters.Add(new SqlParameter("@site_type", 1));
                command1.Parameters.Add(new SqlParameter("@forder_date", DBNULL));
                command1.Parameters.Add(new SqlParameter("@forder_time", DBNULL));
                command1.Parameters.Add(new SqlParameter("@unit_price", product.unitPrice));
                command1.Parameters.Add(new SqlParameter("@cost_price", product.costPrice));
                command1.Parameters.Add(new SqlParameter("@vmuid",product.vmUid));
                //command1.Parameters.Add(new SqlParameter("@bonus",product.bonus));
                //command1.Parameters.Add(new SqlParameter("@point",product.point));
                command1.Parameters.Add(new SqlParameter("@od_repdate", od_ropdate));
                command1.Parameters.Add(new SqlParameter("@cash_in", cash_in));
                command1.Parameters.Add(new SqlParameter("@cash_out", cashtemp.cashOut));

                command1.Connection = sql;
                sql.Open();
                command1.ExecuteNonQuery();
                sql.Close();

               /* //remote
                SqlConnection sql2 = new SqlConnection(sqlconn5);
                SqlCommand command2 = new SqlCommand("insert into [linepaytran] (trumid,code,amount,order_num) values(@transid,'Linepay',@amount,@order)");
                command2.Parameters.Add(new SqlParameter("@transid",transid));
                command2.Parameters.Add(new SqlParameter("@amount",cash_in));
                command2.Parameters.Add(new SqlParameter("@order",dingdan));
                command2.Connection = sql2;
                sql2.Open();
                command2.ExecuteNonQuery();
                sql2.Close();*/
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Salsecomplete(string dingdan,DateTime date1)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn);
            string date = date1.ToString("yyyy/MM/dd");
            /*string year = Convert.ToString(date1.Year);
            string month = Convert.ToString(date1.Month);
            string day = Convert.ToString(date1.Day);
            string hours = Convert.ToString(date1.Hour);
            string minis = Convert.ToString(date1.Minute);
            string seconds = Convert.ToString(date1.Second);*/

            string times = date1.ToString("HH:mm:ss");
            try
            {
                SqlCommand command2 = new SqlCommand(@"update vending_machine_order set forder_date=@forder_date,forder_time=@forder_time where ordernum=@dingdan");
                //SqlCommand command2 = new SqlCommand(@"update sales_order set forder_date=@forder_date,forder_time=@forder_time where ordernum=@dingdan");
                command2.Connection = sql2;
                command2.Parameters.Add(new SqlParameter("@dingdan", dingdan));
                command2.Parameters.Add(new SqlParameter("@forder_date",date));
                command2.Parameters.Add(new SqlParameter("@forder_time",times));
                sql2.Open();
                command2.ExecuteNonQuery();
                sql2.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public TodaySalse TodaySalse(int vmuid,string date1)
        {
            SqlConnection sql1 = new SqlConnection(sqlconn);
            TodaySalse todaySalse = new TodaySalse();

            SqlCommand command1 = new SqlCommand(@"select count(od_id) as todayPay,sum(unit_price) as sales from vending_machine_order where vm_uid=@uid and order_date=@date and pay_type=1 and receive=1");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@date",date1));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    todaySalse.TodayPaycount = reader.GetInt32(reader.GetOrdinal("todayPay"));
                    todaySalse.TodayPaysalse = reader.IsDBNull(1)? 0:reader.GetInt32(reader.GetOrdinal("sales"));
                    break;
                }
            }
            sql1.Close();

            SqlCommand command2 = new SqlCommand(@"select count(od_id) as todayPay,sum(unit_price) as sales from vending_machine_order where vm_uid=@uid and order_date=@date and pay_type=7 and receive=1");
            command2.Parameters.Add(new SqlParameter("@uid", vmuid));
            command2.Parameters.Add(new SqlParameter("@date", date1));
            command2.Connection = sql1;
            sql1.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    todaySalse.TodayExchangecount = reader2.GetInt32(reader2.GetOrdinal("todayPay"));
                    todaySalse.TodayExchangesalse = reader2.IsDBNull(1) ? 0 : reader2.GetInt32(reader.GetOrdinal("sales"));
                    break;
                }
            }
            sql1.Close();

            SqlCommand command3 = new SqlCommand(@"select sum(unit_price) as sales from vending_machine_order where vm_uid=@uid and receive=1");
            command3.Parameters.Add(new SqlParameter("@uid", vmuid));
            command3.Connection = sql1;
            sql1.Open();
            SqlDataReader reader3 = command3.ExecuteReader();
            if (reader3.HasRows)
            {
                while (reader3.Read())
                {
                    todaySalse.salse = reader3.IsDBNull(0) ? 0 : reader3.GetInt32(reader3.GetOrdinal("sales"));
                    break;
                }
            }
            sql1.Close();

            return todaySalse;
        }

        public List<salseProtal> MonSalse(int vmuid,string type)
        {
            DateTime date2 = DateTime.Now;
            string date = date2.ToString("yyyy/MM");

            List<salseProtal> salseProtals = new List<salseProtal>();
            SqlConnection sql1 = new SqlConnection(sqlconn);
           

            SqlCommand command1 = new SqlCommand(@"select pdname,unit_price from vending_machine_order where order_date like @date and pay_type=@type and vm_uid=@uid and receive=1");
            command1.Parameters.Add(new SqlParameter("@uid", vmuid));
            command1.Parameters.Add(new SqlParameter("@date", date+"%"));
            command1.Parameters.Add(new SqlParameter("@type", type));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                { 
                    salseProtal todaySalse = new salseProtal() { 
                        pdname = reader.GetString(reader.GetOrdinal("pdname")),
                        unit_price = reader.GetInt32(reader.GetOrdinal("unit_price"))
                    };

                    salseProtals.Add(todaySalse);
                }
            }
            sql1.Close();

            return salseProtals;
        }

        public List<salseOrder> getSalselist(string start,string end,int uid,string type)
        {
            List<salseOrder> salse = new List<salseOrder>();
            SqlConnection sql1 = new SqlConnection(sqlconn5);
            string commandStr = "";
            string startStr= start == "" ? DateTime.Now.ToString("yyyy/MM/dd") : start;
            
            if (end == "")
            {
                if (type == "")
                {
                    commandStr= "select ordernum,pdname,cash_in,unit_price,cash_out,order_date,order_time,pay_type,receive from vending_machine_order where order_date=@start and vm_uid=@uid";
                }
                else
                {
                    commandStr = (@"select ordernum,pdname,cash_in,unit_price,cash_out,order_date,order_time,pay_type,receive from vending_machine_order where order_date=@start and pay_type=@type and vm_uid=@uid");
                }
            }
            else
            {
                if (type == "")
                {
                    commandStr = (@"select ordernum,pdname,cash_in,unit_price,cash_out,order_date,order_time,pay_type,receive from vending_machine_order where order_date between @start and @end and vm_uid=@uid");
                }
                else
                {
                    commandStr = (@"select ordernum,pdname,cash_in,unit_price,cash_out,order_date,order_time,pay_type,receive from vending_machine_order where order_date between @start and @end and pay_type=@type and vm_uid=@uid");
                }

            }
            SqlCommand command1 = new SqlCommand(commandStr);
            command1.Parameters.Add(new SqlParameter("@uid", uid));
            command1.Parameters.Add(new SqlParameter("@start", startStr));
            command1.Parameters.Add(new SqlParameter("@end", end));
            command1.Parameters.Add(new SqlParameter("@type", type));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    salseOrder Salse = new salseOrder()
                    {
                        ordernum = reader.GetString(reader.GetOrdinal("ordernum")),
                        cash_in = reader.GetString(reader.GetOrdinal("cash_in")),
                        cash_out = reader.GetString(reader.GetOrdinal("cash_out")),
                        pdname = reader.GetString(reader.GetOrdinal("pdname")),
                        unit_price = reader.GetInt32(reader.GetOrdinal("unit_price")),
                        pay_type = paytype(reader.GetString(reader.GetOrdinal("pay_type"))),
                        status = reader.GetString(reader.GetOrdinal("receive")) == "0" ? "交易中" : "完成",
                        datetime=reader.GetString(reader.GetOrdinal("order_date"))+" "+reader.GetString(reader.GetOrdinal("order_time"))
                    };

                    salse.Add(Salse);
                }
            }
            sql1.Close();

            return salse;
        }

        public string paytype(string type)
        {
            string typeStr = "";

            SqlConnection sql1 = new SqlConnection(sqlconn);
            string commandStr = "";

            SqlCommand command1 = new SqlCommand(@"select pay_type_name from pay_type_list where pay_type_id=@type");
            command1.Parameters.Add(new SqlParameter("@type", type));
            command1.Connection = sql1;
            sql1.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    typeStr = reader.GetString(reader.GetOrdinal("pay_type_name"));
                    break;
                }
            }
            sql1.Close();

            return typeStr;
        }

        public List<Orders> LocaltoCloud(string vmcode, string date1)
        {
            bool replace = false;
            List<Orders> orders = new List<Orders>();
            SqlConnection sql = new SqlConnection(sqlconn);
            SqlCommand command1 = new SqlCommand(@"select * from vending_machine_order where vm_uid=@code and forder_date=@date");
            command1.Parameters.Add(new SqlParameter("@code", vmcode));
            command1.Parameters.Add(new SqlParameter("@date", date1));
            command1.Connection = sql;
            sql.Open();
            SqlDataReader reader = command1.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Orders orders1 = new Orders();

                    orders1.od_id = reader.GetInt32(reader.GetOrdinal("od_id"));
                    orders1.pdname = reader.GetString(reader.GetOrdinal("pdname"));
                    if (!reader.IsDBNull(2))
                    {
                        orders1.qrcode = reader.GetString(reader.GetOrdinal("qrcode"));
                    }
                    if (!reader.IsDBNull(3))
                    {
                        orders1.road_layer = reader.GetInt32(reader.GetOrdinal("road_layer"));
                    }
                    orders1.roadid = reader.GetString(reader.GetOrdinal("roadid"));
                    if (!reader.IsDBNull(5))
                    {
                        orders1.layer_num = reader.GetInt32(reader.GetOrdinal("layer_num"));
                    }
                    orders1.road_num = reader.GetString(reader.GetOrdinal("road_num"));
                    if (!reader.IsDBNull(7))
                    {
                        orders1.road_code = reader.GetString(reader.GetOrdinal("road_code"));
                    }
                    orders1.receive = reader.GetString(reader.GetOrdinal("receive"));
                    if (!reader.IsDBNull(9))
                    {
                        orders1.device_no = reader.GetString(reader.GetOrdinal("device_no"));
                    }
                    orders1.vm_num = reader.GetString(reader.GetOrdinal("vm_num"));
                    orders1.pro_id = reader.GetString(reader.GetOrdinal("pro_id"));
                    orders1.ordernum = reader.GetString(reader.GetOrdinal("ordernum"));
                    orders1.order_date = reader.GetString(reader.GetOrdinal("order_date"));
                    orders1.order_time = reader.GetString(reader.GetOrdinal("order_time"));
                    orders1.order_type = reader.GetString(reader.GetOrdinal("order_type"));
                    orders1.incom_type = reader.GetString(reader.GetOrdinal("incom_type"));
                    orders1.pay_mode = reader.GetString(reader.GetOrdinal("pay_mode"));
                    orders1.pay_type = reader.GetString(reader.GetOrdinal("pay_type"));
                    orders1.site_type = reader.GetString(reader.GetOrdinal("site_type"));
                    orders1.forder_date = reader.GetString(reader.GetOrdinal("forder_date"));
                    orders1.forder_time = reader.GetString(reader.GetOrdinal("forder_time"));
                    orders1.unit_price = reader.GetInt32(reader.GetOrdinal("unit_price"));
                    orders1.cost_price = reader.GetInt32(reader.GetOrdinal("cost_price"));
                    if (!reader.IsDBNull(24))
                    {
                        orders1.iqty = reader.GetInt32(reader.GetOrdinal("iqty"));
                    }     
                    orders1.vm_uid = reader.GetInt32(reader.GetOrdinal("vm_uid"));
                    if (!reader.IsDBNull(26))
                    {
                        orders1.vm_tenant_id = reader.GetInt32(reader.GetOrdinal("vm_tenant_id"));
                    }
                    if (!reader.IsDBNull(27))
                    {
                        orders1.card_num = reader.GetString(reader.GetOrdinal("card_num"));
                    }
                    if (!reader.IsDBNull(28))
                    {
                        orders1.u_id = reader.GetString(reader.GetOrdinal("u_id"));
                    }
                    orders1.bonus = reader.IsDBNull(29) ? "0" : reader.GetString(reader.GetOrdinal("bonus"));
                    orders1.point = reader.IsDBNull(30) ? "0" : reader.GetString(reader.GetOrdinal("point"));
                    if (!reader.IsDBNull(31))
                    {
                        orders1.exchgs_code = reader.GetString(reader.GetOrdinal("exchgs_code"));
                    }
                    if (!reader.IsDBNull(32))
                    {
                        orders1.pickup_code = reader.GetString(reader.GetOrdinal("pickup_code"));
                    }
                    orders1.od_repdate = reader.GetString(reader.GetOrdinal("od_repdate"));
                    if (!reader.IsDBNull(34))
                    {
                        orders1.bz = reader.GetString(reader.GetOrdinal("bz"));
                    }
                    orders1.is_update = reader.GetInt32(reader.GetOrdinal("is_updata"));
                    orders1.cash_in = reader.GetString(reader.GetOrdinal("cash_in"));
                    orders1.cash_out = reader.GetString(reader.GetOrdinal("cash_out"));

                    if (checkOrders(orders1.ordernum))
                    {
                        orders.Add(orders1);
                    }
                    else
                    {
                        replace = false;
                    }
                }
            }
            else
            {
                replace = false;
            }
            sql.Close();
            return orders;
        }

        public bool checkOrders(string ordernum)
        {
            SqlConnection sql2 = new SqlConnection(sqlconn5);
            SqlCommand command2 = new SqlCommand(@"select * from vending_machine_order where ordernum=@num");
            command2.Parameters.Add(new SqlParameter("@num", ordernum));
            command2.Connection = sql2;
            sql2.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                sql2.Close();
                return false;
            }
            else
            {
                sql2.Close();
                return true;
            }
            
        }

        public bool returnCloud(Orders orders)
        {
            SqlConnection sql3 = new SqlConnection(sqlconn5);
            try
            {
                SqlCommand command3 = new SqlCommand(@"insert into vending_machine_order (pdname,roadid,road_num,receive,vm_num,pro_id,ordernum,order_date,order_time,order_type,incom_type
                                    ,pay_mode,pay_type,site_type,forder_date,forder_time,unit_price,cost_price,iqty,vm_uid,od_repdate,is_updata,cash_in,cash_out) 
                                    values(@name,@roadid,@road,@receive,@vmnum,@proid,@order,@odate,@otime,@otype,@incom,@paymode,@paytype,@site,@fdate,@ftime,@unit,@cost,1,@uid,@od,
                                    @isupdata,@cashin,@cashout)");
                command3.Parameters.Add(new SqlParameter("@name",orders.pdname));
                command3.Parameters.Add(new SqlParameter("roadid",orders.roadid));
                command3.Parameters.Add(new SqlParameter("@road",orders.road_num));
                command3.Parameters.Add(new SqlParameter("@receive",orders.receive));
                command3.Parameters.Add(new SqlParameter("@vmnum",orders.vm_num));
                command3.Parameters.Add(new SqlParameter("@proid",orders.pro_id));
                command3.Parameters.Add(new SqlParameter("@order",orders.ordernum));
                command3.Parameters.Add(new SqlParameter("@odate",orders.order_date));
                command3.Parameters.Add(new SqlParameter("@otime",orders.order_time));
                command3.Parameters.Add(new SqlParameter("@otype",orders.order_type));
                command3.Parameters.Add(new SqlParameter("@incom", orders.incom_type));
                command3.Parameters.Add(new SqlParameter("@paymode",orders.pay_mode));
                command3.Parameters.Add(new SqlParameter("@paytype", orders.pay_type));
                command3.Parameters.Add(new SqlParameter("@site",orders.site_type));
                command3.Parameters.Add(new SqlParameter("@fdate",orders.forder_date));
                command3.Parameters.Add(new SqlParameter("@ftime",orders.forder_time));
                command3.Parameters.Add(new SqlParameter("@unit",orders.unit_price));
                command3.Parameters.Add(new SqlParameter("@cost",orders.cost_price));
                command3.Parameters.Add(new SqlParameter("@uid",orders.vm_uid));
                command3.Parameters.Add(new SqlParameter("@od",orders.od_repdate));
                command3.Parameters.Add(new SqlParameter("@isupdata",orders.is_update));
                command3.Parameters.Add(new SqlParameter("@cashin",orders.cash_in));
                command3.Parameters.Add(new SqlParameter("@cashout",orders.cash_out));
                command3.Connection = sql3;
                sql3.Open();
                command3.ExecuteNonQuery();
                sql3.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }

    public class Orders
    {
        public int od_id { get; set; }
        public string pdname { get; set; }
        public string qrcode { get; set; }
        public int road_layer { get; set; }
        public string roadid { get; set; }
        public int layer_num { get; set; }
        public string road_num { get; set; }
        public string road_code { get; set; }
        public string receive { get; set; }
        public string device_no { get; set; }
        public string vm_num { get; set; }
        public string pro_id { get; set; }
        public string ordernum { get; set; }
        public string order_date { get; set; }
        public string order_time { get; set; }
        public string order_type { get; set; }
        public string incom_type { get; set; }
        public string pay_mode { get; set; }
        public string pay_type { get; set; }
        public string site_type { get; set; }
        public string forder_date { get; set; }
        public string forder_time { get; set; }
        public int unit_price { get; set; }
        public int cost_price { get; set; }
        public int iqty { get; set; }
        public int vm_uid { get; set; }
        public int vm_tenant_id { get; set; }
        public string card_num { get; set; }
        public string u_id { get; set; }
        public string bonus { get; set; }
        public string point { get; set; }
        public string exchgs_code { get; set; }
        public string pickup_code { get; set; }
        public string od_repdate { get; set; }
        public string bz { get; set; }
        public int is_update { get; set; }
        public string cash_in { get; set; }
        public string cash_out { get; set; }
    }

    public class TodaySalse
    {
        public int TodayPaycount { get; set; }
        public int TodayPaysalse { get; set; }
        public int TodayExchangecount { get; set; }
        public int TodayExchangesalse { get; set; }

        public int salse { get; set; }
    }

    public class salseProtal
    {
        public string pdname { get; set; }
        public int unit_price { get; set; }
    }

    public class salseOrder
    {
        public string ordernum { get; set; }
        public string pdname { get; set; }
        public string cash_in { get; set; }
        public int unit_price { get; set; }
        public string cash_out { get; set; }

        public string pay_type { get; set; }
        public string status { get; set; }
        public string datetime { get; set; }
    }
}