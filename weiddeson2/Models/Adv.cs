﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace weiddeson2.Models
{
    public class Adv
    {
        public int adid { get; set; }
        public string adname { get; set; }
        public string adinfo { get; set; }
        public string adfile { get; set; }
        public object adUrl { get; set; }
        public int adtype { get; set; }
        public int advaddress { get; set; }
        public string btime { get; set; }
        public string ltime { get; set; }
        public string addtime { get; set; }
        public int vm_uid { get; set; }
        public int zt { get; set; }
    }
}